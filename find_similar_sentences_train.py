# -*- coding: utf-8 -*-
import feat_generator
import numpy
import json
from sklearn.feature_selection import VarianceThreshold
from nltk import Tree
from nltk.draw.tree import TreeView
import call_stanford_parser_server_api
import collections
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.externals import joblib
import sys



if __name__ == "__main__":

	input_text = [
		{"text": "The final exams were unbelievably difficult.", "entities": {"subject": "exams", "adj": ["final", "difficult"], "verb": "were", "adverb": "unbelievably", "object": "difficult"}},
		{"text": "This pie is very delicious.", "entities": {"subject": "pie", "verb": "is", "adverb": "very", "adj": "delicious", "object": "delicious"}},
		{"text": "A person smarter than me needs to figure this out.", "entities": {"subject": "person", "adverb": "smarter", "object": "figure", "verb": "needs"}},
		{"text": "Everyone was extremely delighted when the winner was announced.", "entities": {"subject": "Everyone", "object": "winner", "verb": "delighted", "adverb": "extremely", "moment": "announced"}},
		{"text": "The new outfit was very pricey but really beautiful.", "entities": {"subject": "outfit", "verb": "was", "adj": ["new", "pricey", "beautiful"], "adverb": ["very", "really"]}},
		{"text": "The cost of the car is way too high.", "entities": {"subject": "cost", "verb": "is", "adverb": ["way", "too"], "adj": "high"}},
		{"text": "Upset students staged a rally.", "entities": {"subject": "students", "adj": "Upset", "verb": "staged", "object": "rally"}},
		{"text": "That huge complex has quite small but cheap apartments.", "entities": {"subject": "complex", "adj": "huge", "verb": "has", "obj_adj": ["small", "cheap"], "adverb": "quite", "object": "apartments"}},
		{"text": "Her beautiful eyes were very mesmerizing to the young man.", "entities": {"subject": "eyes", "verb": "mesmerizing", "adverb": "very", "object": "man", "adj": "beautiful"}},
		{"text": "The highly emotive actor gave a wonderful performance.", "entities": {"subject": "actor", "verb": "gave", "adverb": "highly", "adj": "emotive", "obj_adj": "wonderful", "object": "performance"}},
		{"text": "The extremely tired kitten fell asleep by her food dish.", "entities": {"subject": "kitten", "verb": "fell", "adverb": "extremely", "adj": "tired", "object": "asleep"}},
		{"text": "Eating out is usually not very healthy.", "entities": {"subject": "Eating", "verb": "is", "adverb": ["usually", "not", "very"], "adj": "healthy", "object": "healthy"}},
		{"text": "I was fairly bored with her.", "entities": {"subject": "I", "verb": "bored", "adverb": "fairly", "object": "her"}},
		{"text": "The original tapestry was beautifully stitched by rugged hands.", "entities": {"subject": "tapestry", "verb": "stitched", "adverb": "beautifully", "adj": "original", "obj_adj": "rugged", "object": "hands"}},
		{"text": "James writes good poems effectively.", "entities": {"subject": "James", "verb": "writes", "adverb": "effectively", "obj_adj": "good", "object": "poems"}},
		{"text": "The overly enthusiastic fans painted their bodies with the team's colors.", "entities": {"subject": "fans", "verb": "painted", "adverb": "overly", "adj": "enthusiastic", "object": "bodies"}},
		{"text": "Ronaldo is the best football player in the world", "entities": {"subject": "Ronaldo", "verb": "is", "obj_adj": "best", "object": "player"}},
		{"text": "He can speak Chinese fluently.", "entities": {"subject": "He", "verb": "speak", "adverb": "fluently", "object": "Chinese"}},
		{"text": "Ram is my best friend.", "entities": {"subject": "Ram", "verb": "is", "adj": "best", "object": "friend"}},
		{"text": "Gopal loves to watch scary movies in theatres.", "entities": {"subject": "Gopal", "verb": "loves", "obj_adj": "scary", "object": ["watch", "movies"]}},
		{"text": "The only person to dance in the office is Mary.", "entities": {"subject": "Mary", "verb": "dance", "obj_adj": "only", "object": "person"}},
		{"text": "Martha loves spicy chinese food.", "entities": {"subject": "Martha", "verb": "loves", "obj_adj": ["spicy", "chinese"], "object": "food"}},
		{"text": "In 2000, the insolvent company was led by well-knowned entrepreneur Larry", "entities": {"subject": "Larry", "verb": "led", "adj": "well-knowned", "obj_adj": "insolvent", "object": "company", "moment": "2000"}},
		{"text": "Stephen was born after 300 years apparently.", "entities": {"subject": "Stephen", "verb": "born", "adverb": "apparently", "obj_adj": "300", "object": "years"}},
		{"text": "An average human loses about 200 hairs per day obscurely.", "entities": {"subject": "human", "verb": "loses", "adverb": "obscurely", "adj": ["average", "human"], "obj_adj": "200", "object": "hairs"}},
		{"text": "Thomas knows her very well.", "entities": {"subject": "Thomas", "verb": "knows", "adverb": ["very", "well"], "object": "her"}},
		{"text": "The histrionic actor smiled at the waiting audience vivaciously.", "entities": {"subject": "actor", "verb": "smiled", "adverb": "vivaciously", "adj": "histrionic", "obj_adj": "waiting", "object": "audience"}},
		{"text": "The skilled Veterinarian took care of the sick animals empathically.", "entities": {"subject": "Veterinarian", "verb": "took", "adverb": "empathically", "adj": "skilled", "object": "care"}},
		{"text": "The tired kids slept at dark night soundlessly.", "entities": {"subject": "kids", "verb": "slept", "adverb": "soundlessly", "adj": "tired", "obj_adj": "dark", "object": "night", "moment": "night"}},
		{"text": "He silently left the office in the evening.", "entities": {"subject": "He", "verb": "left", "adverb": "silently", "moment": "evening", "object": "office"}},
		{"text": "Jefferson was elected as honorable president in 1908 widely.", "entities": {"subject": "Jefferson", "verb": "elected", "moment": "1908", "obj_adj": "honorable", "object": "president"}},
		{"text": "The well-acquainted philosopher went to the mighty United States on Halloween secretly.", "entities": {"subject": "philosopher", "verb": "went", "adj": "well-acquainted", "obj_adj": "mighty", "object": "States", "adverb": "secretly"}},
		{"text": "The mighty King visits the astonishing palace every Monday secretly.", "entities": {"subject": "King", "verb": "visits", "adverb": "secretly", "adj": "mighty", "obj_adj": "astonishing", "object": "palace", "moment": "Monday"}},
		{"text": "The unstoppable rain ceased at night.", "entities": {"subject": "rain", "verb": "ceased", "adj": "unstoppable", "object": "night", "moment": "night"}},
		{"text": "He goes to school everyday.", "entities": {"subject": "He", "verb": "goes", "object": "school", "moment": "everyday"}},
		{"text": "Recession hit the nation in 2009.", "entities": {"subject": "Recession", "verb": "hit", "object": "nation", "moment": "2009"}},
		{"text": "It was raining heavily yesterday.", "entities": {"subject": "It", "verb": "raining", "adverb": "heavily", "object": "yesterday", "moment": "yesterday"}},
		{"text": "Bryan Adams will perform on 8th July apparently.", "entities": {"subject": "Adams", "verb": "perform", "adverb": "apparently", "object": ["8th", "July"], "moment": ["8th", "July"]}},
		{"text": "Convocation is scheduled on 26th of this month.", "entities": {"subject": "Convocation", "verb": "scheduled", "object": ["26th", "month"], "moment": ["26th", "month"]}},
		{"text": "He flunked in the yesterday's exam.", "entities": {"subject": "He", "verb": "flunked", "obj_adj": "yesterday", "object": "exam", "moment": "yesterday"}},
		{"text": "Silly John was texting her in the night endlessly.", "entities": {"subject": "John", "verb": "texting", "adverb": "endlessly", "adj": "Silly", "object": "her", "moment": "night"}},
		{"text": "He is leaving the city tomorrow.", "entities": {"subject": "He", "verb": "leaving", "object": "city", "moment": "tomorrow"}},
		{"text": "He is leaving the city on 1st July.", "entities": {"subject": "He", "verb": "leaving", "object": "city", "moment": ["1st", "July"]}},
		{"text": "I usually go to bed at around 11 o'clock.", "entities": {"subject": "I", "verb": "go", "adverb": "usually", "object": "bed", "moment": "11"}},
		{"text": "In 1920, NASA moved its headquarters to California.", "entities": {"subject": "NASA", "verb": "moved", "object": "headquarters", "moment": "1920"}},
		{"text": "The film is releasing on 13th October.", "entities": {"subject": "film", "verb": "releasing", "object": "13th", "moment": ["13th", "October"]}},
		{"text": "He was born on 28th July, 1933", "entities": {"subject": "He", "verb": "born", "object": "28th", "moment": ["28th", "July", "1933"]}},
		{"text": "The train arrived at the station at 3 am.", "entities": {"subject": "train", "verb": "arrived", "object": "station", "moment": ["3", "am"]}},
		{"text": "Laura has appeared on the television.", "entities": {"subject": "Laura", "verb": "appeared", "object": "television"}},
		{"text": "My old friends met the histrionic actor. ", "entities": {"subject": "friends", "verb": "met", "adj": "old", "obj_adj": "histrionic", "object": "actor"}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},


	]



	X_train_all = []

	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])

		feats = feat_generator.get_feats(stanford_output)

		feats = feats.flatten()

		X_train_all.append(feats)

	print "X_train_all", len(X_train_all), ",", len(X_train_all[0])

	numpy.save('train_feats.npy', {"feats": X_train_all, "input_text": input_text})
