# -*- coding: utf-8 -*-
import feat_generator
import numpy
import call_stanford_parser_server_api
from sklearn.externals import joblib


def predict_adjectives(all_stanford_output, pred_entities_score_stats, input_text, pred_entities):
	all_subject_adjs = []
	all_object_adjs = []
	for index, stanford_output in enumerate(all_stanford_output):
		subject_label = feat_generator.get_answer_label(stanford_output, pred_entities_score_stats["subject"][index][0][0])
		subject_adjs = feat_generator.get_adjectives(subject_label["deps_with_words"])
		print "actual subject_adjs", [input_text[index]["entities"]["adj"] if "adj" in input_text[index]["entities"] else None]
		print "predict subject_adjs", subject_adjs

		all_subject_adjs.append(subject_adjs)
		print "\n##############\n"

		object_label = feat_generator.get_answer_label(stanford_output, pred_entities_score_stats["object"][index][0][0])
		object_adjs = feat_generator.get_adjectives(object_label["deps_with_words"])
		print "actual object_adjs", [input_text[index]["entities"]["obj_adj"] if "obj_adj" in input_text[index]["entities"] else None]
		print "predict object_adjs", object_adjs
		
		all_object_adjs.append(object_adjs)
		print "\n##############\n"

	pred_entities["sub_adj"] = all_subject_adjs
	pred_entities["obj_adj"] = all_object_adjs
	return pred_entities



def y_label2words(preds, y_train_tree, all_stanford_output, input_text, entity, stanford_output_train_entity):
	preds_word = []

	pred_stanf_output = []
	pred_score_stats = []

	for index, pred in enumerate(preds):
		# if pred:
		pred_answer_label = y_train_tree[pred]
		stanford_output_pred = stanford_output_train_entity[pred]

		pred_stanf_output.append(stanford_output_pred["sentences"][0]["parse"])
	
		stanford_output_test = all_stanford_output[index]

		pred_word, score_stats = feat_generator.get_test_answer_label_distance(stanford_output_test, pred_answer_label, stanford_output_pred)
		pred_score_stats.append(score_stats)

		pred_word_pos = feat_generator.get_token_attr(stanford_output_test, pred_word)["pos"]

		if entity == "adverb":
			if pred_word_pos[:2] == "RB":
				preds_word.append(pred_word)
			else:
				preds_word.append(None)
		elif pred_word_pos[:2] != ".":
			preds_word.append(pred_word)
		else:
			preds_word.append(None)


	actual_word = [i["entities"][entity] if entity in i["entities"] else None for i in input_text]
	print "\nactual_word", actual_word

	print "preds_word  ", preds_word

	if preds_word:
		accuracy = sum([1 for index in xrange(0, len(preds_word)) if actual_word[index] == preds_word[index]])/float(len(preds_word))
		print "accuracy", accuracy

	print "\n\n", "pred_stanf_output\n", pred_stanf_output

	return preds_word, pred_score_stats




if __name__ == "__main__":

	input_text = [
		# {"text": "My old father shoots a red car accurately.", "entities": {"subject": "father", "adj": "old", "verb": "shoots", "adverb": "accurately", "object": "car", "obj_adj": "red"}},
		# {"text": "The final exams were unbelievably difficult.", "entities": {"subject": "exams", "adj": ["final", "difficult"], "verb": "were", "adverb": "unbelievably", "object": "difficult"}},
		# {"text": "This pie is very delicious.", "entities": {"subject": "pie", "verb": "is", "adverb": "very", "adj": "delicious", "object": "delicious"}},
		# {"text": "A person smarter than me needs to figure this out.", "entities": {"subject": "person", "adverb": "smarter", "object": "figure", "verb": "needs"}},
		# {"text": "The fat boy went under the big bridge secretly at night.", "entities": {"subject": "boy", "verb": "went", "adverb": "secretly", "adj": "fat", "obj_adj": "big", "object": "bridge", "prep_ph": "night"}},
		# {"text": "Under the Rowlatt Act, John was elected as the president.", "entities": {"subject": "John", "verb": "elected", "object": "president", "prep_ph": "Act"}},
		{"text": "The tedious problems were scaring John.", "entities": {"subject": "problems", "adj": "tedious", "verb": "scaring", "object": "John"}},
		# {"text": "One in every 4 Americans has appeared on television.", "entities": {"subject": "Americans", "verb": "appeared", "object": "television"}},
		# {"text": "A skunk's smell can be detected by a human.", "entities": {"subject": "smell", "verb": "detected", "object": "human"}},
		# {"text": "The mother of Michael Nesmith invented whiteout.", "entities": {"subject": "mother", "verb": "invented", "", "object": "whiteout"}},
		# {"text": "Isaac Asimov is the only author to have a book in every Dewey-decimal category.", "entities": {"subject": "Asimov", "verb": "is", "adj": "only", "object": "book"}},
		# {"text": "Barbie's full name is 'Babara Millicent Roberts.'", "entities": {"subject": "name", "verb": "is", "object": "Babara"}},
		# {"text": "Shakespeare invented the word 'assassination' and 'bump'.", "entities": {"subject": "Shakespeare", "verb": "invented", "object": "word"}},
		# {"text": "Tina Turner's real name is Annie Mae Bullock.", "entities": {"subject": "name", "verb": "is", "object": "Annie"}},
		# {"text": "Beethoven dipped his head in cold water before he composed.", "entities": {"subject": "Beethoven", "verb": "dipped", "object": "head", "prep_ph": "composed"}},
		# {"text": "President John F Kennedy could read 4 newspapers in 20 minutes.", "entities": {"subject": "Kennedy", "verb": "read", "object": "newspapers", "prep_ph": "20"}},
		# {"text": "John runs.", "entities": {"subject": "John", "verb": "runs"}},
		# # {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},
		# # {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},

		# {"text": "Barbie's full name is 'Babara Millicent Roberts.'", "entities": {"subject": "name", "verb": "is", "object": "Babara"}},

		
		
		
		
		# Adolf Hitler's mother seriously considered having an abortion but was talked out of it by her doctor.
		# The shortest British monarch was Charles I, who was 4 feet 9 inches.
		
		
		
		# Bob Dylan's real name is Robert Zimmerman.
		# Sigmund Freud had a morbid fear of ferns.
		# Anne Boleyn, Queen Elizabeth I's mother, had six fingers on one hand.
		# Orville Wright was involved in the first aircraft accident. His passenger, a Frenchman, was killed.
		# The sound of E.T. walking was made by someone squishing her hands in jelly.
		# Cher's last name was "Sarkissian." She changed it because no one could pronounce it.
		# Sugar was first added to chewing gum in 1869 by a dentist, William Semple.
		# Paper was invented early in the second century by Chinese eunuch.
		# Sir Isaac Newton was only 23 years old when he discovered the law of universal gravitation.
		# Hannibal had only one eye after getting a disease while attacking Rome.
		# A blue whales heart only beats nine times per minute.
		# A cat uses its whiskers to determine if a space is too small to squeeze through.
		# A chameleon's tongue is twice the length of its body.
		# A crocodiles tongue is attached to the roof of its mouth.
		# Rodent's teeth never stop growing.
		# A shark can detect one part of blood in 100 million parts of water.
		# The penguin is the only bird that can swim but can't fly.
		# The cheetah is the only cat that can't retract its claws.
		# A lion's roar can be heard from five miles away.
		# Emus and kangaroos can't walk backwards.
		# Cats have over 100 vocal sounds; dogs only have 10.
		# A mole can dig a tunnel 300 feet (91 m) long in just one night.
		# Insects outnumber humans 100,000,000 to one.
		# Sharkskin has tiny tooth-like scales all over.
		# Chameleons can move their eyes in two directions at the same time.
		# Koalas never drink water. They get fluids from the eucalyptus leaves they eat.
		# A cow gives nearly 200,000 glasses of milk in her lifetime.
		# When sharks take a bite, their eyes roll back and their teeth jut out.
		# Camels chew in a figure 8 pattern.
		# Proportional to their size, cats have the largest eyes of all mammals.
		# Sailfish can leap out of the water and into the air at a speed of 50 miles (81 km) per hour.
		# The catfish has the most taste buds of all animals, having over 27,000 of them.
		# A skunk's smell can be detected by a human a mile away.
		# A lion in the wild usually makes no more than 20 kills a year.
		# In space, astronauts cannot cry, because there is no gravity, so the tears can't flow.
		# The state of Florida is bigger than England.
		# One in every 4 Americans has appeared on television.
		# The average American/Canadian will eat about 11.9 pounds of cereal per year!







	]



	X_test_all = []
	all_stanford_output = []

	y_train_tree_all = numpy.load('y_train_tree_all.npy')
	y_train_tree_all = y_train_tree_all.tolist()

	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		all_stanford_output.append(stanford_output)

		feats = feat_generator.get_feats(stanford_output)

		feats = feats.flatten()

		X_test_all.append(feats)

	pred_entities = {}
	pred_entities_score_stats = {}

	for entity, y_train_tree in y_train_tree_all.iteritems():

		if entity in ["adj", "obj_adj"]:
			continue

		print "\n     Testing :", entity

		model = joblib.load('model_' + entity + '.pkl')

		stanford_output_train_entity = numpy.load('stanford_output_train_' + entity + '.npy')

		preds = model.predict(X_test_all)
		print "predict", preds.tolist()

		preds_word, pred_score_stats = y_label2words(preds, y_train_tree, all_stanford_output, input_text, entity, stanford_output_train_entity)
		pred_entities_score_stats[entity] = pred_score_stats

		pred_entities[entity] = preds_word

		print "\n##############\n"
		# exit(0)

	# print "\npred_entities_score_stats\n\n", pred_entities_score_stats
	# exit(0)

	# print "pred_entities", pred_entities

	pred_entities = predict_adjectives(all_stanford_output, pred_entities_score_stats, input_text, pred_entities)

	print "pred_entities", pred_entities





