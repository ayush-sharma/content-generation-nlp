# -*- coding: utf-8 -*-
import feat_generator
import numpy
import call_stanford_parser_server_api
import generate_sentences
import time
from operator import mul
import nltk.data
import json
import pdb


def generate_questions(input_text, sample):

	tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
	sentences = tokenizer.tokenize(input_text)
	input_text = [{"text": i} for i in sentences]
	print "input_text", json.dumps(input_text, indent=4)

	pred_entities = get_predicted_entities(input_text)
	inp = raw_input('paused')

	entity_mappings = get_entity_mappings(pred_entities)
	print "entity_mappings", entity_mappings, "\n"

	subject_verb_object_combo = get_subject_verb_object_combo(entity_mappings)
	print "len subject_verb_object_combo", len(subject_verb_object_combo)

	required_entities = get_required_entities(sample["pred_entities"])
	print "required_entities", required_entities

	satisfied_combos = []

	for each_combo in subject_verb_object_combo:
		for required_entity in required_entities:
			main_entity = required_entity.split('_')
			if main_entity[0] == "sub":
				if main_entity[1] in entity_mappings["subjects"][each_combo["subject"]]:					
					for value in entity_mappings["subjects"][each_combo["subject"]][main_entity[1]]:
						if value:
							each_combo.setdefault(required_entity, [])
							each_combo[required_entity].append(value)
			elif main_entity[0] == "obj":
				if main_entity[1] in entity_mappings["objects"][each_combo["object"]]:					
					for value in entity_mappings["objects"][each_combo["object"]][main_entity[1]]:
						if value:
							each_combo.setdefault(required_entity, [])
							each_combo[required_entity].append(value)
			else:
				if main_entity[1] in entity_mappings["verbs"][each_combo["verb"]]:					
					for value in entity_mappings["verbs"][each_combo["verb"]][main_entity[1]]:
						if value:
							each_combo.setdefault(required_entity, [])
							each_combo[required_entity].append(value)

		if check_all_entity_satisfied(required_entities, each_combo):
			satisfied_combos.append(each_combo)

	text_gen_combos = satisfied_combos

	len_text_gen_combos = len(text_gen_combos)
	print "len of combos", len_text_gen_combos
	inp = raw_input('paused')
	start_time = time.time()



	# {'obj_det': [u'the'], 'verb': u'is', 'verb_prep': [u'on'], 'object': u'school', 'subject': u'We'}


	corpus_dict = get_nearby_words(input_text)
	questions_generated = []
	# for index, combo in enumerate(text_gen_combos):
	for index in xrange(0, 100):
		combo = text_gen_combos[index]

		question = generate_sentences.parsedUnderstandingSimilar(combo, sample)
		score, score_stats = get_combo_distance(combo, corpus_dict)
		question["score"] = score
		question["score_stats"] = score_stats
		questions_generated.append(question)
		print "count", index, " / ", len_text_gen_combos

	# print "\nquestions_generated", questions_generated

	print "\nTIME TAKEN:", time.time() - start_time

	sorted_questions_generated = sorted(questions_generated, key=lambda k: k['score']) 

	print "\nsorted_questions_generated", sorted_questions_generated

	return sorted_questions_generated[:20]



def get_predicted_entities(input_text):
	# input_text = [


	# 	{"text": "The male seahorse carries the eggs until they hatch instead of the female."},
	# 	{"text": "Negative emotions such as anxiety and depression can weaken your immune system."},
	# 	{"text": "Stephen Hawking was born exactly 300 years after Galileo died."},
	# 	{"text": "Mercury is the only planet whose orbit is coplanar with its equator."},
	# 	{"text": "Some moths never eat anything as adults because they don't have mouths. "},
	# 	{"text": "Stewardesses is the longest word typed with only the left hand."},
	# 	{"text": "An average human loses about 200 head hairs per day."},
	# 	{"text": "Mexico City sinks about 10 inches a year."},
	# 	{"text": "It's impossible to sneeze with your eyes open."},
	# 	{"text": "In France, a five year old child can buy an alcoholic drink in a bar."},
	# 	{"text": "The glue on Israeli postage is certified kosher."},
	# 	{"text": "On average, 100 people choke to death on ball-point pens every year."},
	# 	{"text": "Thirty-five percent of the people who use personal ads for dating are already married."},
	# 	{"text": "The electric chair was invented by a dentist."},
	# 	{"text": "The top butterfly flight speed is 12 miles per hour. "},
	# 	{"text": "A Boeing 747s wingspan is longer than the Wright brother's first flight."},



	# 	# {"text": "John goes to the school everyday."},
	# 	# {"text": "John made new friends."},
	# 	# {"text": "John was preparing for the exams well before."},
	# 	# {"text": "Martha was driving the car very fast."},
	# 	# {"text": "Martha did her homework yesterday."},
	# 	# {"text": "Mary is sleeping on the couch."},
	# 	# {"text": "They were going to a party."},
	# 	# {"text": "They plucked a rose from the garden."},
	# 	# {"text": "We did not reach the station on time."},
	# 	# {"text": "We got a call from the headquarters."},
	# 	# {"text": "The police caught the thief at his home."},
	# 	# {"text": "I wanted a red car on my birthday."},
	# 	# {"text": "Martha made a new project."},

	# 	# {"text": "The britishers captured the beautiful city."},
	# 	# {"text": "John went to the school."},
	# 	# {"text": "The beautiful city was founded by the britishers apparently."},
	# 	# {"text": "The small city had many illiterate people apparently."},
	# 	# {"text": "The people were mainly farmers apparently."},
	# 	# {"text": "The big green farms were located near the sea coast apparently."},
	# 	# {"text": "The farms had a horse stable too apparently."},
	# 	# {"text": "The people praised Lord Krishna apparently."},
	# 	# {"text": "The britishers also praised the Lord similarly."},
	# 	# {"text": "The britishers manage their coasts very well."},
	# 	# {"text": "Britishers work hard to make their city better than other cities."},
	# 	# {"text": "Britishers work hard to make their city."},
	# 	# {"text": "Britishers invented farming techniques which revolutionized cities."},
	# ]



	# input_text = "Bud is a bird. He wakes up early to catch a worm. Bud is very hungry. Will is a worm. He moves around in the dirt. Will sees Bud flying in the sky. Will moves near a tree. Bud flies around the tree. Will moves to the garden. He still sees Bud flying in the sky. I must find some place safe to hide. Will looks around and disappears as Bud swoops down. Bud eats some bird seeds that he finds on the ground. Bud did not see Will.Some fireflies glow to warn other animals that they don't taste good. Frogs, bats, and birds do not like to eat animals that glow. The glow helps keep fireflies safe.Ben and his dad went fishing. Ben took the pole. Ben liked the boat ride. Ben put a worm on the hook. He put his line in the water. He felt the pole pull. Was it a fish? He reeled it in. He did not get a fish. Ben got an old can."

	X_test_all = []
	all_test_stanford_output = []

	XY_train = numpy.load('XY_train.npy').tolist()

	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		all_test_stanford_output.append(stanford_output)

		feats = feat_generator.get_feats(stanford_output)

		feats = feats.flatten()

		X_test_all.append(feats)

	pred_entities = [{} for i in range(0, len(input_text))]

	for entity, XY in XY_train.iteritems():
		if entity not in ["adj", "obj_adj", "prep_ph"]:

			print "\n     Testing :", entity

			for index, X_test in enumerate(X_test_all):
				matches = feat_generator.get_similar_sentences(X_test, XY)

				pred_answer_tree = XY["y"][matches[0][2]]
				stanford_output_test = all_test_stanford_output[index]
				stanford_output_pred = XY["stanford_output"][matches[0][2]]

				pred_word = feat_generator.get_main_entities(pred_answer_tree, stanford_output_test, input_text[index], entity, stanford_output_pred)
				pred_entities[index][entity] = pred_word

	print "init pred_entities", pred_entities

	for index, text in enumerate(input_text):
		test_stanford_output = all_test_stanford_output[index]
		pred_entities[index] = feat_generator.predict_next_entities(test_stanford_output, pred_entities[index], text)

	print "\npred_entities\n", pred_entities, "\n"

	return pred_entities



def get_words_distance(word1, word2, corpus_dict):
	if word1 in corpus_dict:
		if word2 in corpus_dict[word1]:
			return corpus_dict[word1][word2]
	return corpus_dict["__MAX__"]

def get_combo_distance(combo, corpus_dict):

	entity_score = {}
	verb_object_prep_det_score = []
	
	subject_verb_score = get_words_distance(combo["subject"], combo["verb"], corpus_dict)
	verb_object_score = get_words_distance(combo["verb"], combo["object"], corpus_dict)

	entity_score["subject_verb_score"] = subject_verb_score
	entity_score["verb_object_score"] = verb_object_score
	verb_object_prep_det_score.append(verb_object_score)

	if "verb_prep" in combo:
		verb_prep_score = 0
		object_prep_score = 0
		for prep in combo["verb_prep"]:
			verb_prep_score += get_words_distance(combo["verb"], prep, corpus_dict)
			object_prep_score += get_words_distance(combo["object"], prep, corpus_dict)
		verb_prep_score = verb_prep_score/float(len(combo["verb_prep"]))
		object_prep_score = object_prep_score/float(len(combo["verb_prep"]))

		entity_score["verb_prep_score"] = verb_prep_score
		entity_score["object_prep_score"] = object_prep_score
		verb_object_prep_det_score += [verb_prep_score, object_prep_score]

	if "sub_adj" in combo:
		subject_adjective_score = 0
		for adj in combo["sub_adj"]:
			subject_adjective_score += get_words_distance(combo["subject"], adj, corpus_dict)
		subject_adjective_score = subject_adjective_score/float(len(combo["sub_adj"]))

		entity_score["subject_adjective_score"] = subject_adjective_score

	if "obj_adj" in combo:
		object_adjective_score = 0
		for adj in combo["obj_adj"]:
			object_adjective_score += get_words_distance(combo["object"], adj, corpus_dict)
		object_adjective_score = object_adjective_score/float(len(combo["obj_adj"]))

		entity_score["object_adjective_score"] = object_adjective_score

	if "sub_det" in combo:
		subject_det_score = 0
		for det in combo["sub_det"]:
			subject_det_score += get_words_distance(combo["subject"], det, corpus_dict)
		subject_det_score = subject_det_score/float(len(combo["sub_det"]))

		entity_score["subject_det_score"] = subject_det_score

	if "obj_det" in combo:
		object_det_score = 0
		for det in combo["obj_det"]:
			object_det_score += get_words_distance(combo["object"], det, corpus_dict)
		object_det_score = object_det_score/float(len(combo["obj_det"]))

		entity_score["object_det_score"] = object_det_score
		verb_object_prep_det_score.append(object_det_score)

	if "adverb" in combo:
		verb_adverb_score = get_words_distance(combo["verb"], combo["adverb"], corpus_dict)
		entity_score["verb_adverb_score"] = verb_adverb_score

	# entity_score["verb_object_prep_det_score"] = pow(reduce(mul, verb_object_prep_det_score, 1), 1/len(verb_object_prep_det_score))
	# entity_score["verb_object_prep_det_score"] = sum(verb_object_prep_det_score)/float(len(verb_object_prep_det_score))
	entity_score["verb_object_prep_det_score"] = sum(verb_object_prep_det_score)

	score = sum([value for value in entity_score.values()])

	return score, entity_score



def get_nearby_words(input_text):
	corpus_dict = {}
	max_distance = 0

	for text in input_text:
		text = text["text"].lower()
		text = ''.join(e for e in text if e.isalnum() or e==" ")
		text_split = text.split()
		for word_index, word in enumerate(text_split):
			corpus_dict.setdefault(word, {})
			for compare_word_index, compare_word in enumerate(text_split):
				if word == compare_word:
					continue
				distance = abs(word_index - compare_word_index)

				if max_distance < distance:
					max_distance = distance + 1

				if compare_word in corpus_dict[word]:
					if corpus_dict[word][compare_word] > distance:
						corpus_dict[word][compare_word] = distance
				else:
					corpus_dict[word][compare_word] = distance

				# corpus_dict[word].setdefault(compare_word, [])
				# corpus_dict[word][compare_word].append(distance)

	corpus_dict["__MAX__"] = max_distance
	return corpus_dict



def check_all_entity_satisfied(required_entities, each_combo):
	for i in required_entities:
		if i not in each_combo:
			return False
	return True



def get_required_entities(sample):
	required_entities = []
	for entity in sample:
		if entity in ["subject", "object", "verb"]:
			continue
		if sample[entity]:
			required_entities.append(entity)
	return required_entities

def get_subject_verb_object_combo(entity_mappings):
	subject_verb_object_combo = []
	for subject in entity_mappings["subjects"]:
		for verb in entity_mappings["verbs"]:
			for object in entity_mappings["objects"]:
				if subject.lower() != object.lower():
					subject_verb_object_combo.append({"subject": subject, "verb": verb, "object": object})
	return subject_verb_object_combo


def get_entity_mappings(pred_entities):
	entity_mappings = {"subjects": {}, "verbs": {}, "objects": {}}
	for sentence in pred_entities:

		entity_mappings["subjects"].setdefault(sentence["subject"], {})
		entity_mappings["subjects"][sentence["subject"]].setdefault("adj", [])
		# entity_mappings["subjects"][sentence["subject"]].setdefault("prep", [])
		entity_mappings["subjects"][sentence["subject"]].setdefault("det", [])
		entity_mappings["subjects"][sentence["subject"]].setdefault("verb", [])

		entity_mappings["subjects"][sentence["subject"]]["adj"] += sentence["sub_adj"]
		# entity_mappings["subjects"][sentence["subject"]]["prep"].append(sentence["sub_prep"])
		entity_mappings["subjects"][sentence["subject"]]["det"].append(sentence["sub_det"])
		entity_mappings["subjects"][sentence["subject"]]["verb"].append(sentence["verb"])


		entity_mappings["objects"].setdefault(sentence["object"], {})
		entity_mappings["objects"][sentence["object"]].setdefault("adj", [])
		# entity_mappings["objects"][sentence["object"]].setdefault("prep", [])
		entity_mappings["objects"][sentence["object"]].setdefault("det", [])
		entity_mappings["objects"][sentence["object"]].setdefault("verb", [])

		entity_mappings["objects"][sentence["object"]]["adj"] += sentence["obj_adj"]
		# entity_mappings["objects"][sentence["object"]]["prep"].append(sentence["obj_prep"])
		entity_mappings["objects"][sentence["object"]]["det"].append(sentence["obj_det"])
		entity_mappings["objects"][sentence["object"]]["verb"].append(sentence["verb"])


		entity_mappings["verbs"].setdefault(sentence["verb"], {})
		entity_mappings["verbs"][sentence["verb"]].setdefault("adverb", [])
		entity_mappings["verbs"][sentence["verb"]].setdefault("prep", [])

		entity_mappings["verbs"][sentence["verb"]]["adverb"].append(sentence["adverb"])
		entity_mappings["verbs"][sentence["verb"]]["prep"].append(sentence["obj_prep"])

	# print "\nentity_mappings", entity_mappings
	return entity_mappings



if __name__ == "__main__":

	input_text = "Bud is a bird. He wakes up early to catch a worm. Bud is very hungry. Will is a worm. He moves around in the dirt. Will sees Bud flying in the sky. Will moves near a tree. Bud flies around the tree. Will moves to the garden. He still sees Bud flying in the sky. I must find some place safe to hide. Will looks around and disappears as Bud swoops down. Bud eats some bird seeds that he finds on the ground. Bud did not see Will.Some fireflies glow to warn other animals that they don't taste good. Frogs, bats, and birds do not like to eat animals that glow. The glow helps keep fireflies safe. Ben and his dad went fishing. Ben took the pole. Ben liked the boat ride. Ben put a worm on the hook. He put his line in the water. He felt the pole pull. Was it a fish? He reeled it in. He did not get a fish. Ben got an old can."
	# sample = {"pred_entities": {'obj_adj': None, 'adverb': None, 'object': u'school', 'sub_adj': None, 'obj_det': u'the', 'verb': u'went', 'sub_det': None, 'sub_prep': None, 'verb_prep': u'to', 'subject': u'John'}}
	# sample = {"ansSampleObject": {"adv": "", "objPrep": "", "objS": "", "objAdj": "", "subS": "John", "subDet": "", "objDet": "", "verb": "", "subAdj": "", "subPrep": ""}, "text": {"adv": "", "objPrep": "to", "objS": "school", "objAdj": "", "subS": "John", "subDet": "", "objDet": "the", "verb": "goes", "subAdj": "", "subPrep": ""}, "pred_entities": {"obj_adj": '', "adverb": '', "object": "school", "sub_adj": '', "obj_det": "the", "verb": "goes", "sub_det": '', "sub_prep": '', "obj_prep": "to", "subject": "John"}, "qsSampleObject": {"adv": "", "objPrep": "to", "objS": "school", "objAdj": "", "subS": "who", "subDet": "", "objDet": "the", "verb": "goes", "subAdj": "", "subPrep": ""}}
	sample = {
	  "ansSampleObject": {
	    "adv": "", 
	    "objAdj": "", 
	    "objDet": "", 
	    "objPrep": "", 
	    "objS": "", 
	    "subAdj": "", 
	    "subDet": "", 
	    "subPrep": "", 
	    "subS": "John", 
	    "verb": ""
	  }, 
	  "pred_entities": {
	    "adverb": None, 
	    "obj_adj": None, 
	    "obj_det": "the", 
	    "object": "school", 
	    "sub_adj": None, 
	    "sub_det": None, 
	    "sub_prep": None, 
	    "subject": "John", 
	    "verb": "goes", 
	    "verb_prep": "to"
	  }, 
	  "qsSampleObject": {
	    "adv": "", 
	    "inter": "WHO_SUBJECT", 
	    "objAdj": "", 
	    "objDet": "the", 
	    "objPrep": "to", 
	    "objS": "school", 
	    "subAdj": "", 
	    "subDet": "", 
	    "subPrep": "", 
	    "subS": "who", 
	    "verb": "goes"
	  }
	}

	questions_generated = generate_questions(input_text, sample)

	print "\n TOP 20:\n"
	print questions_generated







