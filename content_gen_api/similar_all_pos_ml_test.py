# -*- coding: utf-8 -*-
import feat_generator
import numpy
import call_stanford_parser_server_api
from sklearn.externals import joblib


if __name__ == "__main__":

	input_text = [

		# {"text": "Marisa gave me 78 on pizza.", "entities": {"by": "me", "merchant": "Marisa", "verb": "gave", "amount": "78", "purpose": "pizza"}},
		# {"text": "My old father shoots a red car accurately in night.", "entities": {"subject": "father", "adj": "old", "verb": "shoots", "adverb": "accurately", "object": "car", "obj_adj": "red", "prep_ph": "night"}},
		# {"text": "The final exams were unbelievably difficult.", "entities": {"subject": "exams", "adj": ["final", "difficult"], "verb": "were", "adverb": "unbelievably", "object": "difficult"}},
		# {"text": "This pie is very delicious.", "entities": {"subject": "pie", "verb": "is", "adverb": "very", "adj": "delicious", "object": "delicious"}},
		# {"text": "A person smarter than me needs to figure this out.", "entities": {"subject": "person", "adverb": "smarter", "object": "figure", "verb": "needs"}},
		# {"text": "The fat boy went under the big bridge secretly at night.", "entities": {"subject": "boy", "verb": "went", "adverb": "secretly", "adj": "fat", "obj_adj": "big", "object": "bridge", "prep_ph": "night"}},
		# {"text": "Under the Rowlatt Act, John was elected as the president.", "entities": {"subject": "John", "verb": "elected", "object": "president", "prep_ph": "Act"}},
		# {"text": "The tedious problems were scaring John.", "entities": {"subject": "problems", "adj": "tedious", "verb": "scaring", "object": "John"}},
		# {"text": "Ben put a worm on the hook.", "entities": {}},
		# {"text": "One in every 4 Americans has appeared on television.", "entities": {"subject": "Americans", "verb": "appeared", "object": "television"}},
		# {"text": "A skunk's smell can be detected by a human.", "entities": {"subject": "smell", "verb": "detected", "object": "human"}},
		# {"text": "The mother of Michael Nesmith invented whiteout.", "entities": {"subject": "mother", "verb": "invented", "", "object": "whiteout"}},
		# {"text": "Isaac Asimov is the only author to have a book in every Dewey-decimal category.", "entities": {"subject": "Asimov", "verb": "is", "adj": "only", "object": "book"}},
		# {"text": "Barbie's full name is 'Babara Millicent Roberts.'", "entities": {"subject": "name", "verb": "is", "object": "Babara"}},
		# {"text": "Shakespeare invented the word 'assassination' and 'bump'.", "entities": {"subject": "Shakespeare", "verb": "invented", "object": "word"}},
		# {"text": "Tina Turner's real name is Annie Mae Bullock.", "entities": {"subject": "name", "verb": "is", "object": "Annie"}},
		# {"text": "Beethoven dipped his head in cold water before he composed.", "entities": {"subject": "Beethoven", "verb": "dipped", "object": "head", "prep_ph": "composed"}},
		# {"text": "President John F Kennedy could read 4 newspapers in 20 minutes.", "entities": {"subject": "Kennedy", "verb": "read", "object": "newspapers", "prep_ph": "20"}},
		# {"text": "John runs.", "entities": {"subject": "John", "verb": "runs"}},
		# # {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},
		# # {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},
		# {"text": "Barbie's full name is 'Babara Millicent Roberts.'", "entities": {"subject": "name", "verb": "is", "object": "Babara"}},

		{"text": "Get me a best star hotel in San Diego.", "entities": {}},
		{"text": "4 star Trident hotel is what I want in Kanpur.", "entities": {}},
		{"text": "I need a 3.5 star hotel for 2 star people in Dadar.", "entities": {}},
		{"text": "I need a 3.5 star hotel.", "entities": {}},

	]



	X_test_all = []
	all_test_stanford_output = []

	XY_train = numpy.load('XY_train.npy').tolist()

	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		all_test_stanford_output.append(stanford_output)

		feats = feat_generator.get_feats(stanford_output)

		feats = feats.flatten()

		X_test_all.append(feats)

	pred_entities = [{} for i in range(0, len(input_text))]

	for index, X_test in enumerate(X_test_all):
		print "\nTest Sentence\n", input_text[index]["text"], "\n\n"
		for entity, XY in XY_train.iteritems():
			if entity not in ["adj", "obj_adj", "prep_ph"]:
				print "\n     Testing :", entity
		
				matches = feat_generator.get_similar_sentences(X_test, XY)
				print "matches", matches[:3]

				pred_answer_tree = XY["y"][matches[0][2]]
				stanford_output_test = all_test_stanford_output[index]
				stanford_output_pred = XY["stanford_output"][matches[0][2]]

				pred_word = feat_generator.get_main_entities(pred_answer_tree, stanford_output_test, input_text[index], entity, stanford_output_pred)
				pred_entities[index][entity] = pred_word

				# print "pred_word", pred_word, "\n#######\n"
				
		print "\n##################################\n"


	# for index, text in enumerate(input_text):
	# 	test_stanford_output = all_test_stanford_output[index]
	# 	pred_entities[index] = feat_generator.predict_next_entities(test_stanford_output, pred_entities[index], text)

	print "\npred_entities\n", pred_entities


