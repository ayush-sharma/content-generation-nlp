from subprocess import *
import json
import feat_generator
import numpy
import similar_text_gen
import nltk.data

main = [
 'subS',
 'subAdj',
 'subDet',
 'subPrep',
 'objS',
 'objAdj',
 'objDet',
 'objPrep',
 'verb',
 'adv',
 'inter'
 ]

key_mapping = {
    "objAdj": "obj_adj",
    "adv": "adverb",
    "objS": "object",
    "subAdj": "sub_adj",
    "objDet": "obj_det",
    "verb": "verb",
    "subDet": "sub_det",
    "subPrep": "sub_prep",
    "objPrep": "verb_prep",
    "subS": "subject",
    "inter": "inter"
}

key_mapping_opp = {
    "obj_adj": "objAdj",
    "adverb": "adv",
    "object": "objS",
    "sub_adj": "subAdj",
    "obj_det": "objDet",
    "verb": "verb",
    "sub_det": "subDet",
    "sub_prep": "subPrep",
    "verb_prep": "objPrep",
    "obj_prep": "objPrep",
    "subject": "subS",
    "inter": "inter"
}


def create_sample_entities(sample_input):
    # {"text": "John goes to school.", "question_tag": "who", "answer": "John"}
    text = sample_input["text"]
    question_tag = sample_input["question_tag"]
    answer = sample_input["answer"]

    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    sentences = tokenizer.tokenize(text)
    text = [{"text": i} for i in sentences]


    pred_entities = similar_text_gen.get_predicted_entities(text)[0]
    if "obj_prep" in pred_entities:
        pred_entities["verb_prep"] = pred_entities.pop("obj_prep")

    qsSampleObject = {}
    ansSampleObject = {}
    new_pred = {}

    for entity in pred_entities:
        new_pred[key_mapping_opp[entity]] = pred_entities[entity]
        if new_pred[key_mapping_opp[entity]]:
            if type(new_pred[key_mapping_opp[entity]]) == list:
                new_pred[key_mapping_opp[entity]] = new_pred[key_mapping_opp[entity]][0]
        else:
            new_pred[key_mapping_opp[entity]] = None
            pred_entities[entity] = None

        if new_pred[key_mapping_opp[entity]]:
            if new_pred[key_mapping_opp[entity]].lower() == answer.lower():
                qsSampleObject[key_mapping_opp[entity]] = question_tag
                ansSampleObject[key_mapping_opp[entity]] = answer
            else:
                qsSampleObject[key_mapping_opp[entity]] = new_pred[key_mapping_opp[entity]]
                ansSampleObject[key_mapping_opp[entity]] = ''
        else:
            qsSampleObject[key_mapping_opp[entity]] = ''
            ansSampleObject[key_mapping_opp[entity]] = ''

    if question_tag == "who":
        qsSampleObject["inter"] = "WHO_SUBJECT"

    return {"pred_entities": pred_entities, "qsSampleObject": qsSampleObject, "ansSampleObject": ansSampleObject}







def parsedUnderstandingSimilar(old_textObj, sample):

    new_textObj = {}
    for new_key in main:
        if key_mapping[new_key] in old_textObj:
            value = old_textObj[key_mapping[new_key]]
            if value:
                if type(value) == list:
                    new_textObj[new_key] = value[0]
                else:
                    new_textObj[new_key] = value
            else:
                new_textObj[new_key] = ''
        else:
            new_textObj[new_key] = ''

    textObj = new_textObj

    qsSampleObject = sample["qsSampleObject"]
    ansSampleObject = sample["ansSampleObject"]
    
    # qsSampleObject = {'adv': '', 'objPrep': 'to', 'objS': 'school', 'inter': 'WHAT_SUBJECT', 'objAdj': '', 'subS': 'who', 'subDet': '', 'objDet': 'the', 'verb': 'went', 'subAdj': '', 'subPrep': ''} 
    # ansSampleObject = {'adv': '', 'objPrep': '', 'objS': '', 'objAdj': '', 'subS': 'grandma', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}

    simQsObject= {}
    simAnsObject= {}
    text = ""
    qs = ""
    ans = ""
    for m in main:
        simQsObject[m]=""
        simAnsObject[m]=""
        if m in qsSampleObject.keys() and m !="inter":
            if qsSampleObject[m]!="":
                simQsObject[m]=textObj[m]
        elif m =="inter":
            simQsObject[m]=qsSampleObject[m]
        if m in ansSampleObject.keys() and m !="inter":
            if ansSampleObject[m]!="":
                if textObj[m] !="":
                    simAnsObject[m]=textObj[m]
                else: 
                    simAnsObject[m] = "NA"

    qsState = objectToStatement(json.dumps(simQsObject))
    # print "qsState", qsState
    qs = str(statementToLine(qsState[:-1])[0])
    print "qs", qs
    ansState = objectToStatement(json.dumps(simAnsObject))
    # print "ansState", ansState
    ans = str(statementToLine(ansState[:-1])[0])
    print "ans", ans
    textState = objectToStatement(json.dumps(textObj))
    # print "textState", textState
    text = str(statementToLine(textState[:-1])[0])
    print "text", text, "\n"

    return {"text": text, "qs": qs, "ans": ans}



def objectToStatement(obj):
    obj = json.loads(obj)
    text = ''
    for m in main:
        if m in obj.keys() and obj[m]!="" and m!="inter":
            text += obj[m].lower() + ',' + m + ','
        if m=="inter" and m in obj.keys():
            text += obj[m] + ',' + m + ','
    return text



def statementToLine(statement):
    print "statement", statement
    args = ['./test.jar', statement]
    process = Popen(['java', '-jar'] + list(args), stdout=PIPE, stderr=PIPE)
    ret = []
    while process.poll() is None:
        line = process.stdout.readline()
        if line != '' and line.endswith('\n'):
            ret.append(line[:-1])

    stdout, stderr = process.communicate()
    ret += stdout.split('\n')
    if stderr != '':
        ret += stderr.split('\n')
    ret.remove('')
    return ret

