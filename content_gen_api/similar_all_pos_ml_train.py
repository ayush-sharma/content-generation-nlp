# -*- coding: utf-8 -*-
import feat_generator
import numpy
import json
from sklearn.feature_selection import VarianceThreshold
from nltk import Tree
from nltk.draw.tree import TreeView
import call_stanford_parser_server_api
import collections
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.externals import joblib
import sys



if __name__ == "__main__":

	input_text = [
		# {"text": "The final exams were unbelievably difficult.", "entities": {"subject": "exams", "adj": ["final", "difficult"], "verb": "were", "adverb": "unbelievably", "object": "difficult"}},
		# {"text": "This pie is very delicious.", "entities": {"subject": "pie", "verb": "is", "adverb": "very", "adj": "delicious", "object": "delicious"}},
		# {"text": "A person smarter than me needs to figure this out.", "entities": {"subject": "person", "adverb": "smarter", "object": "figure", "verb": "needs"}},
		# {"text": "Everyone was extremely delighted when the winner was announced.", "entities": {"subject": "Everyone", "object": "winner", "verb": "delighted", "adverb": "extremely", "prep_ph": "announced"}},
		# {"text": "The new outfit was very pricey but really beautiful.", "entities": {"subject": "outfit", "verb": "was", "adj": ["new", "pricey", "beautiful"], "adverb": ["very", "really"]}},
		# {"text": "The cost of the car is way too high.", "entities": {"subject": "cost", "verb": "is", "adverb": ["way", "too"], "adj": "high"}},
		# {"text": "Upset students staged a rally.", "entities": {"subject": "students", "adj": "Upset", "verb": "staged", "object": "rally"}},
		# {"text": "That huge complex has quite small but cheap apartments.", "entities": {"subject": "complex", "adj": "huge", "verb": "has", "obj_adj": ["small", "cheap"], "adverb": "quite", "object": "apartments"}},
		# {"text": "Her beautiful eyes were very mesmerizing to the young man.", "entities": {"subject": "eyes", "verb": "mesmerizing", "adverb": "very", "object": "man", "adj": "beautiful"}},
		# {"text": "The highly emotive actor gave a wonderful performance.", "entities": {"subject": "actor", "verb": "gave", "adverb": "highly", "adj": "emotive", "obj_adj": "wonderful", "object": "performance"}},
		# {"text": "The extremely tired kitten fell asleep by her food dish.", "entities": {"subject": "kitten", "verb": "fell", "adverb": "extremely", "adj": "tired", "object": "asleep"}},
		# {"text": "Eating out is usually not very healthy.", "entities": {"subject": "Eating", "verb": "is", "adverb": ["usually", "not", "very"], "adj": "healthy", "object": "healthy"}},
		# {"text": "I was fairly bored with her.", "entities": {"subject": "I", "verb": "bored", "adverb": "fairly", "object": "her"}},
		# {"text": "The original tapestry was beautifully stitched by rugged hands.", "entities": {"subject": "tapestry", "verb": "stitched", "adverb": "beautifully", "adj": "original", "obj_adj": "rugged", "object": "hands"}},
		# {"text": "James writes good poems effectively.", "entities": {"subject": "James", "verb": "writes", "adverb": "effectively", "obj_adj": "good", "object": "poems"}},
		# {"text": "The overly enthusiastic fans painted their bodies with the team's colors.", "entities": {"subject": "fans", "verb": "painted", "adverb": "overly", "adj": "enthusiastic", "object": "bodies"}},
		# {"text": "Ronaldo is the best football player in the world", "entities": {"subject": "Ronaldo", "verb": "is", "obj_adj": "best", "object": "player"}},
		# {"text": "He can speak Chinese fluently.", "entities": {"subject": "He", "verb": "speak", "adverb": "fluently", "object": "Chinese"}},
		# {"text": "Ram is my best friend.", "entities": {"subject": "Ram", "verb": "is", "adj": "best", "object": "friend"}},
		# {"text": "Gopal loves to watch scary movies in theatres.", "entities": {"subject": "Gopal", "verb": "loves", "obj_adj": "scary", "object": ["watch", "movies"]}},
		# {"text": "The only person to dance in the office is Mary.", "entities": {"subject": "Mary", "verb": "dance", "obj_adj": "only", "object": "person"}},
		# {"text": "Martha loves spicy chinese food.", "entities": {"subject": "Martha", "verb": "loves", "obj_adj": ["spicy", "chinese"], "object": "food"}},
		# {"text": "In 2000, the insolvent company was led by well-knowned entrepreneur Larry", "entities": {"subject": "Larry", "verb": "led", "adj": "well-knowned", "obj_adj": "insolvent", "object": "company", "prep_ph": "2000"}},
		# {"text": "Stephen was born after 300 years apparently.", "entities": {"subject": "Stephen", "verb": "born", "adverb": "apparently", "obj_adj": "300", "object": "years"}},
		# {"text": "An average human loses about 200 hairs per day obscurely.", "entities": {"subject": "human", "verb": "loses", "adverb": "obscurely", "adj": ["average", "human"], "obj_adj": "200", "object": "hairs"}},
		# {"text": "Thomas knows her very well.", "entities": {"subject": "Thomas", "verb": "knows", "adverb": ["very", "well"], "object": "her"}},
		# {"text": "The histrionic actor smiled at the waiting audience vivaciously.", "entities": {"subject": "actor", "verb": "smiled", "adverb": "vivaciously", "adj": "histrionic", "obj_adj": "waiting", "object": "audience"}},
		# {"text": "The skilled Veterinarian took care of the sick animals empathically.", "entities": {"subject": "Veterinarian", "verb": "took", "adverb": "empathically", "adj": "skilled", "object": "care"}},
		# {"text": "The tired kids slept at dark night soundlessly.", "entities": {"subject": "kids", "verb": "slept", "adverb": "soundlessly", "adj": "tired", "obj_adj": "dark", "object": "night", "prep_ph": "night"}},
		# {"text": "He silently left the office in the evening.", "entities": {"subject": "He", "verb": "left", "adverb": "silently", "prep_ph": "evening", "object": "office"}},
		# {"text": "Jefferson was elected as honorable president in 1908 widely.", "entities": {"subject": "Jefferson", "verb": "elected", "prep_ph": "1908", "obj_adj": "honorable", "object": "president"}},
		# {"text": "The well-acquainted philosopher went to the mighty United States on Halloween secretly.", "entities": {"subject": "philosopher", "verb": "went", "adj": "well-acquainted", "obj_adj": "mighty", "object": "States", "adverb": "secretly"}},
		# {"text": "The mighty King visits the astonishing palace every Monday secretly.", "entities": {"subject": "King", "verb": "visits", "adverb": "secretly", "adj": "mighty", "obj_adj": "astonishing", "object": "palace", "prep_ph": "Monday"}},
		# {"text": "The unstoppable rain ceased at night.", "entities": {"subject": "rain", "verb": "ceased", "adj": "unstoppable", "object": "night", "prep_ph": "night"}},
		# {"text": "He goes to school everyday.", "entities": {"subject": "He", "verb": "goes", "object": "school", "prep_ph": "everyday"}},
		# {"text": "Recession hit the nation in 2009.", "entities": {"subject": "Recession", "verb": "hit", "object": "nation", "prep_ph": "2009"}},
		# {"text": "It was raining heavily yesterday.", "entities": {"subject": "It", "verb": "raining", "adverb": "heavily", "object": "yesterday", "prep_ph": "yesterday"}},
		# {"text": "Bryan Adams will perform on 8th July apparently.", "entities": {"subject": "Adams", "verb": "perform", "adverb": "apparently", "object": ["8th", "July"], "prep_ph": ["8th", "July"]}},
		# {"text": "Convocation is scheduled on 26th of this month.", "entities": {"subject": "Convocation", "verb": "scheduled", "object": ["26th", "month"], "prep_ph": ["26th", "month"]}},
		# {"text": "He flunked in the yesterday's exam.", "entities": {"subject": "He", "verb": "flunked", "obj_adj": "yesterday", "object": "exam", "prep_ph": "yesterday"}},
		# {"text": "Silly John was texting her in the night endlessly.", "entities": {"subject": "John", "verb": "texting", "adverb": "endlessly", "adj": "Silly", "object": "her", "prep_ph": "night"}},
		# {"text": "He is leaving the city tomorrow.", "entities": {"subject": "He", "verb": "leaving", "object": "city", "prep_ph": "tomorrow"}},
		# {"text": "He is leaving the city on 1st July.", "entities": {"subject": "He", "verb": "leaving", "object": "city", "prep_ph": ["1st", "July"]}},
		# {"text": "I usually go to bed at around 11 o'clock.", "entities": {"subject": "I", "verb": "go", "adverb": "usually", "object": "bed", "prep_ph": "11"}},
		# {"text": "In 1920, NASA moved its headquarters to California.", "entities": {"subject": "NASA", "verb": "moved", "object": "headquarters", "prep_ph": "1920"}},
		# {"text": "The film is releasing on 13th October.", "entities": {"subject": "film", "verb": "releasing", "object": "13th", "prep_ph": ["13th", "October"]}},
		# {"text": "He was born on 28th July, 1933", "entities": {"subject": "He", "verb": "born", "object": "28th", "prep_ph": ["28th", "July", "1933"]}},
		# {"text": "The train arrived at the station at 3 am.", "entities": {"subject": "train", "verb": "arrived", "object": "station", "prep_ph": ["3", "am"]}},
		# {"text": "Laura has appeared on the television.", "entities": {"subject": "Laura", "verb": "appeared", "object": "television"}},
		# {"text": "My old friends met the histrionic actor.", "entities": {"subject": "friends", "verb": "met", "adj": "old", "obj_adj": "histrionic", "object": "actor"}},
		# {"text": "The cat was hiding from the dog under the bed.", "entities": {"subject": "cat", "verb": "hiding", "object": "dog", "prep_ph": "bed"}},
		# {"text": "Under the presidential rule, Kejriwal was removed from the post.", "entities": {"subject": "Kejriwal", "verb": "removed", "object": "post", "prep_ph": "rule"}},
		# {"text": "In the king's reign, the theives were scared of stealing.", "entities": {"subject": "theives", "verb": "scared", "object": "stealing", "prep_ph": "reign"}},
		# {"text": "The successful Americans had been working on supercomputers since 1980.", "entities": {"subject": "Americans", "adj": "successful", "verb": "working", "object": "supercomputers", "prep_ph": "1980"}},
		# # {"text": "John went to the auditorium.", "entities": {}}

		# {"text": "Joey put on his mask.", "entities": {"subject": "Joey", "verb": "put", "object": "mask"}},
		# {"text": "He flapped his cape in front of the mirror.", "entities": {"subject": "He", "verb": "flapped", "object": "cape"}},
		# {"text": "Joey took Mindy upstairs to his room.", "entities": {"subject": "Joey", "verb": "took", "object": "Mindy"}},
		# {"text": "He dug through his closets.", "entities": {"subject": "He", "verb": "dug", "object": "closets"}},
		# {"text": "Joey found his baby blanket.", "entities": {"subject": "Joey", "verb": "found", "object": "blanket"}},
		# {"text": "He put it around Mindy’s shoulders.", "entities": {"subject": "He", "verb": "put", "object": "shoulders"}},

		# {"text": "One sunny April day, Maisy rode her bike past the toy store window.", "entities": {"subject": "Maisy", "verb": "rode", "object": "bike"}},
		# {"text": "She hit the breaks.", "entities": {"subject": "She", "verb": "hit", "object": "breaks"}},
		# {"text": "She hopped off her bike.", "entities": {"subject": "She", "verb": "hopped", "object": "bike"}},
		# {"text": "Maisy twisted her head.", "entities": {"subject": "Maisy", "verb": "twisted", "object": "head"}},
		# {"text": "She clinked nickels into her bank.", "entities": {"subject": "She", "verb": "clinked", "object": "nickels"}},
		# {"text": "Ben put a worm on the hook.", "entities": {"subject": "Ben", "verb": "put", "object": "worm"}},


 
 


		# {"text": "I spent 500 on shoes in bata.", "entities": {"by": "I", "verb": "spent", "purpose": "shoes", "merchant":"bata", "amount":"500"}},
		# {"text": "I gave 10 to Sohan for pizza.", "entities": {"by": "I", "verb": "gave", "purpose": "pizza", "merchant":"Sohan", "amount":"10"}},
		# {"text": "Ankit spent 100 at starbucks for coffee.", "entities": {"by": "Ankit", "verb": "spent", "purpose": "coffee", "merchant":"starbucks", "amount":"100"}},
		# {"text": "I spent 60 at gala for shoes.", "entities": {"by": "I", "verb": "spent", "purpose": "shoes", "merchant":"gala", "amount":"60"}},
		# {"text": "I spent 48 at bata for flipflops.", "entities": {"by": "I", "verb": "spent", "purpose": "flipflops", "merchant":"bata", "amount":"48"}},
		# {"text": "I spent 48 at bata for flipflops.", "entities": {"by": "I", "verb": "spent", "purpose": "flipflops", "merchant":"bata", "amount":"48"}},
		# {"text": "Gopal spent 100 on dosa at Prapti.", "entities": {"by": "Gopal", "verb": "spent", "purpose": "dosa", "merchant":"Prapti", "amount":"100"}},
		# {"text": "Gopal paid Rahul 2000 for drinks.", "entities": {"by": "Gopal", "verb": "paid", "purpose": "drinks", "merchant":"Rahul", "amount":"2000"}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},

		# {"text": "Book me a 4 star hotel for three adults and 2 children in San Francisco within 1 mile of Fisherman's Wharf.", "entities": {"rating": "4", "location": ["San", "Francisco"]}},
		# {"text": "I want to book a 2 star rated hotel in Los Angeles for May 3.", "entities": {"rating": "2", "location": ["Los", "Angeles"]}},
		{"text": "Book 5 star hotel in Mumbai.", "entities": {"rating": "5", "location": "Mumbai"}},
		{"text": "Book highest star hotel in Mumbai.", "entities": {"rating": "highest", "location": "Mumbai"}}

	]



	X_train_all = []
	y_train_tree_all = {}
	
	XY_train = {}

	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		feats = feat_generator.get_feats(stanford_output)

		for entity in text["entities"]:
			XY_train.setdefault(entity, {})
			XY_train[entity].setdefault("feats", [])
			XY_train[entity].setdefault("y", [])
			XY_train[entity].setdefault("input_text", [])
			XY_train[entity].setdefault("stanford_output", [])

			y_train_tree_all.setdefault(entity, [])

			if type(text["entities"][entity]) == list:
				for multi_entity in text["entities"][entity]:
					y_label_entity, y_train_tree_all[entity] = feat_generator.get_train_answer_label(stanford_output, multi_entity, y_train_tree_all[entity])
					
					XY_train[entity]["feats"].append(feats.flatten())
					XY_train[entity]["y"].append(y_train_tree_all[entity][y_label_entity])
					XY_train[entity]["input_text"].append(text)
					XY_train[entity]["stanford_output"].append(stanford_output)
			else:
				y_label_entity, y_train_tree_all[entity] = feat_generator.get_train_answer_label(stanford_output, text["entities"][entity], y_train_tree_all[entity])

				XY_train[entity]["feats"].append(feats.flatten())
				XY_train[entity]["y"].append(y_train_tree_all[entity][y_label_entity])
				XY_train[entity]["input_text"].append(text)
				XY_train[entity]["stanford_output"].append(stanford_output)

	# import pdb;pdb.set_trace()

	numpy.save('XY_train.npy', XY_train)




