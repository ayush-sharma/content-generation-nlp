# -*- coding: utf-8 -*-
from pycorenlp import StanfordCoreNLP
import json

nlp = StanfordCoreNLP('http://localhost:9000')

def get_output(text):
	return nlp.annotate(text, properties={
		# 'annotators': 'tokenize,ssplit,pos,depparse,parse, lemma, ner, natlog, openie, dcoref',
		# 'annotators': 'tokenize, ssplit, pos, ner, parse, dcoref',
		'annotators': 'tokenize, ssplit, pos, ner, parse',
		'outputFormat': 'json'
	})


if __name__ == "__main__":

	# out = get_output("The atom is a basic unit of matter, it consists of a dense central nucleus surrounded by a cloud of negatively charged electrons.")
	# out = get_output("The atoms are a basic unit of matter, they consists of a dense central nucleus surrounded by a cloud of negatively charged electrons.")
	out = get_output("John runs very fast.")
	# out = get_output("Raunak just met a boy. He is so young that he did not have a name yet. His feet were very small.")
	print out
