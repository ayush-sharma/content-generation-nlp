# -*- coding: utf-8 -*-
import feat_generator
import numpy
import call_stanford_parser_server_api


def get_text_gent_combos(input_text):
	X_test_all = []
	all_test_stanford_output = []

	XY_train = numpy.load('XY_train.npy').tolist()

	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		all_test_stanford_output.append(stanford_output)

		feats = feat_generator.get_feats(stanford_output)

		feats = feats.flatten()

		X_test_all.append(feats)

	pred_entities = [{} for i in range(0, len(input_text))]

	for entity, XY in XY_train.iteritems():
		if entity not in ["adj", "obj_adj", "prep_ph"]:

			print "\n     Testing :", entity

			for index, X_test in enumerate(X_test_all):
				matches = feat_generator.get_similar_sentences(X_test, XY)

				pred_answer_tree = XY["y"][matches[0][2]]
				stanford_output_test = all_test_stanford_output[index]
				stanford_output_pred = XY["stanford_output"][matches[0][2]]

				pred_word = feat_generator.get_main_entities(pred_answer_tree, stanford_output_test, input_text[index], entity, stanford_output_pred)
				pred_entities[index][entity] = pred_word

	for index, text in enumerate(input_text):
		test_stanford_output = all_test_stanford_output[index]
		pred_entities[index] = feat_generator.predict_next_entities(test_stanford_output, pred_entities[index], text)

	print "\npred_entities\n", pred_entities, "\n"

	# pred_entities = [{'obj_adj': [u'beautiful'], 'adverb': None, 'object': u'city', 'sub_adj': [], 'obj_det': u'the', 'verb': u'captured', 'sub_det': u'The', 'sub_prep': None, 'obj_prep': None, 'subject': u'britishers'}, {'obj_adj': [], 'adverb': None, 'object': u'school', 'sub_adj': [], 'obj_det': u'the', 'verb': u'went', 'sub_det': None, 'sub_prep': None, 'obj_prep': u'to', 'subject': u'John'}]

	entity_mappings = get_entity_mappings(pred_entities)
	# print "entity_mappings", entity_mappings, "\n"

	subject_verb_object_combo = get_subject_verb_object_combo(entity_mappings)


	sample = {'obj_adj': ['prestigious'], 'adverb': None, 'object': u'school', 'sub_adj': ['Smart'], 'obj_det': u'the', 'verb': u'went', 'sub_det': None, 'sub_prep': None, 'verb_prep': u'to', 'subject': u'John'}

	required_entities = get_required_entities(sample)

	for each_combo in subject_verb_object_combo:
		for required_entity in required_entities:
			main_entity = required_entity.split('_')
			if main_entity[0] == "sub":
				if main_entity[1] in entity_mappings["subjects"][each_combo["subject"]]:
					each_combo.setdefault(required_entity, [])
					for value in entity_mappings["subjects"][each_combo["subject"]][main_entity[1]]:
						if value:
							each_combo[required_entity].append(value)
			elif main_entity[0] == "obj":
				if main_entity[1] in entity_mappings["objects"][each_combo["object"]]:
					each_combo.setdefault(required_entity, [])
					for value in entity_mappings["objects"][each_combo["object"]][main_entity[1]]:
						if value:
							each_combo[required_entity].append(value)
			else:
				if main_entity[1] in entity_mappings["verbs"][each_combo["verb"]]:
					each_combo.setdefault(required_entity, [])
					for value in entity_mappings["verbs"][each_combo["verb"]][main_entity[1]]:
						if value:
							each_combo[required_entity].append(value)

	subject_verb_object_combo = subject_verb_object_combo


def get_required_entities(sample):
	required_entities = []
	for entity in sample:
		if entity in ["subject", "object", "verb"]:
			continue
		if sample[entity]:
			required_entities.append(entity)
	return required_entities

def get_subject_verb_object_combo(entity_mappings):
	subject_verb_object_combo = []
	for subject in entity_mappings["subjects"]:
		for verb in entity_mappings["verbs"]:
			for object in entity_mappings["objects"]:
				subject_verb_object_combo.append({"subject": subject, "verb": verb, "object": object})
	return subject_verb_object_combo


def get_entity_mappings(pred_entities):
	entity_mappings = {"subjects": {}, "verbs": {}, "objects": {}}
	for sentence in pred_entities:

		entity_mappings["subjects"].setdefault(sentence["subject"], {})
		entity_mappings["subjects"][sentence["subject"]].setdefault("adj", [])
		# entity_mappings["subjects"][sentence["subject"]].setdefault("prep", [])
		entity_mappings["subjects"][sentence["subject"]].setdefault("det", [])
		entity_mappings["subjects"][sentence["subject"]].setdefault("verb", [])

		entity_mappings["subjects"][sentence["subject"]]["adj"] += sentence["sub_adj"]
		# entity_mappings["subjects"][sentence["subject"]]["prep"].append(sentence["sub_prep"])
		entity_mappings["subjects"][sentence["subject"]]["det"].append(sentence["sub_det"])
		entity_mappings["subjects"][sentence["subject"]]["verb"].append(sentence["verb"])


		entity_mappings["objects"].setdefault(sentence["object"], {})
		entity_mappings["objects"][sentence["object"]].setdefault("adj", [])
		# entity_mappings["objects"][sentence["object"]].setdefault("prep", [])
		entity_mappings["objects"][sentence["object"]].setdefault("det", [])
		entity_mappings["objects"][sentence["object"]].setdefault("verb", [])

		entity_mappings["objects"][sentence["object"]]["adj"] += sentence["obj_adj"]
		# entity_mappings["objects"][sentence["object"]]["prep"].append(sentence["obj_prep"])
		entity_mappings["objects"][sentence["object"]]["det"].append(sentence["obj_det"])
		entity_mappings["objects"][sentence["object"]]["verb"].append(sentence["verb"])


		entity_mappings["verbs"].setdefault(sentence["verb"], {})
		entity_mappings["verbs"][sentence["verb"]].setdefault("adverb", [])
		entity_mappings["verbs"][sentence["verb"]].setdefault("prep", [])

		entity_mappings["verbs"][sentence["verb"]]["adverb"].append(sentence["adverb"])
		entity_mappings["verbs"][sentence["verb"]]["prep"].append(sentence["obj_prep"])

	# print "\nentity_mappings", entity_mappings
	return entity_mappings



if __name__ == "__main__":

	input_text = [
		{"text": "The britishers captured the beautiful city."},
		{"text": "John went to the school."}
		# "The beautiful city was founded by the britishers apparently.", "The small city had many illiterate people apparently.", "The people were mainly farmers apparently.", "The big green farms were located near the sea coast apparently.", "The farms had a horse stable too apparently.", "The people praised Lord Krishna apparently.", "The britishers also praised the Lord similarly.", "The britishers manage their coasts very well.", "Britishers work hard to make their city better than other cities.", "Britishers work hard to make their city.", "Britishers invented farming techniques which revolutionized cities."
	]

	print get_text_gent_combos(input_text)


	









