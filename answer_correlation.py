import feat_generator
import call_stanford_parser_server_api
import numpy
from pycorenlp import StanfordCoreNLP
import json
import operator
from collections import Counter
from nltk import Tree



def euclidean_distance(list1, list2):
	sum_of_squares = sum([pow(list1[index] - list2[index], 2) for index in xrange(0, len(list1))])
	return 1 / (1 + sum_of_squares)


def predict_depr(X_train, X_test, y_train):
	# print "shape", X_test
	all_preds = []
	all_pred_stats = []
	for ind, per_word_test in enumerate(X_test):
		max_score = 0
		best_match_index = 0
		pred_stats = []
		for index, per_train in enumerate(X_train):
			score = euclidean_distance(per_word_test, per_train)
			if max_score < score:
				max_score = score
				best_match_index = index
			pred_stats.append((score, y_train[index], index))
		# print "pred_stats", pred_stats
		# exit(0)
		all_preds.append(y_train[best_match_index])
		all_pred_stats.append(pred_stats)

	return all_preds, all_pred_stats



def predict(X_train, X_test, y_train, y_test_word):
	all_scores = []
	for index_test, per_test in enumerate(X_test):
		total_score = 0
		for index_train, per_train in enumerate(X_train):
			score = euclidean_distance(per_test, per_train)
			total_score += score
		all_scores.append((total_score, y_test_word[index_test]))
	
	all_scores.sort(key=operator.itemgetter(0), reverse=True)
	print "all_scores", all_scores
	all_scores_sorted = [i[1] for i in all_scores]
	print "all_scores_sorted", all_scores_sorted
	return all_scores_sorted[0]




def predict_by_multi_score(X_train, X_test, y_train, y_test_word):

	all_preds = []
	for index_train, per_train in enumerate(X_train):
		all_scores = []
		for index_test, per_test in enumerate(X_test):
			score = euclidean_distance(per_test, per_train)
			all_scores.append((score, y_test_word[index_test]))
		all_scores.sort(key=operator.itemgetter(0), reverse=True)
		all_preds.append(all_scores[0][1])

	most_common,num_most_common = Counter(all_preds).most_common(1)[0] # 4, 6 times
	print "common", most_common,num_most_common
	return most_common


def predict_by_multi_max_score(X_train, X_test, y_train, y_test_word):

	all_preds = []
	for index_train, per_train in enumerate(X_train):
		all_scores = []
		max_score = 0
		best_pred = y_test_word[0]
		for index_test, per_test in enumerate(X_test):
			score = euclidean_distance(per_test, per_train)
			if max_score < score:
				max_score = score
				best_pred = y_test_word[index_test]
		all_preds.append(best_pred)

	print "all_preds", all_preds

	most_common,num_most_common = Counter(all_preds).most_common(1)[0] # 4, 6 times
	print "common", most_common,num_most_common
	return most_common



def predict_by_weighted_avg(X_train, X_test, y_train, y_test_word):

	all_preds = []
	for index_train, per_train in enumerate(X_train):
		all_scores = []
		max_score = 0
		best_pred = y_test_word[0]
		for index_test, per_test in enumerate(X_test):
			score = euclidean_distance(per_test, per_train)
			if max_score < score:
				max_score = score
				best_pred = y_test_word[index_test]
		all_preds.append((best_pred, max_score))

	print "all_preds", all_preds

	word_freq = {}
	for pred in all_preds:
		word_freq.setdefault(pred[0], {"sum_score": 0, "weighted_score": 0, "freq": 0})
		word_freq[pred[0]]["sum_score"] += pred[1]
		word_freq[pred[0]]["freq"] += 1
		word_freq[pred[0]]["weighted_score"] = word_freq[pred[0]]["sum_score"]/ float(word_freq[pred[0]]["freq"])

	print "word_freq", json.dumps(word_freq, indent=4, sort_keys=True)

	sorted_word_freq = sorted(word_freq, key=lambda x: (word_freq[x]['weighted_score']), reverse=True)
	print "sorted_word_freq", sorted_word_freq

	return sorted_word_freq[0]




def predict_by_deep_weighted_avg(X_train, X_test, y_train, y_test_word):

	all_preds = []
	for index_train, per_train in enumerate(X_train):
		for index_test, per_test in enumerate(X_test):
			score = euclidean_distance(per_test, per_train)
			all_preds.append((y_test_word[index_test], score))
			print "paused", y_test_word[index_test], score, index_train, index_test
			raw_input()

	print "all_preds", all_preds

	word_freq = {}
	for pred in all_preds:
		word_freq.setdefault(pred[0], {"sum_score": 0, "weighted_score": 0, "freq": 0})
		word_freq[pred[0]]["sum_score"] += pred[1]
		word_freq[pred[0]]["freq"] += 1
		word_freq[pred[0]]["weighted_score"] = word_freq[pred[0]]["sum_score"]/ float(word_freq[pred[0]]["freq"])

	print "word_freq", json.dumps(word_freq, indent=4, sort_keys=True)

	sorted_word_freq = sorted(word_freq, key=lambda x: (word_freq[x]['weighted_score']), reverse=True)
	print "sorted_word_freq", sorted_word_freq

	return sorted_word_freq[0]






def find_pos_token_index(pos_list, match_token):
	for ind, val in enumerate(pos_list):
		if val[0:2] == match_token[0:2]:
			# print "match_token", match_token, ind
			return ind, val



if __name__ == "__main__":

	X_train = numpy.load('X_train.npy')
	y_train = numpy.load('y_train.npy')
	feats_select = numpy.load('feats_select.npy')

	# test_text = [{"text": "The pancreas produces Insulin.", "subject": "pancreas", "answer": "Insulin", "verb": "produces"},
	# 			{"text": "Cats have over 100 vocal sounds.", "subject": "Cats", "answer": "sounds", "verb": "have"},
	# 			{"text": "It is believed that Leonardo Da Vinci invented the scissors.", "subject": "It", "answer": "scissors", "verb": "believed"},
	# 			{"text": "Mexico City sinks about 10 inches a year.", "subject": "City", "answer": "year", "verb": "sinks"},

	# 			{"text": "Negative emotions such as anxiety and depression can weaken your immune system.", "subject": "emotions", "answer": "system"},
	# 			# {"text":"Stephen Hawking was born exactly 300 years after Galileo died.", "subject": "Hawking", "answer": "exactly"},
	# 			{"text": "Mercury is the only planet whose orbit is coplanar with its equator.", "subject": "Mercury", "answer": "coplanar"},
	# 			{"text": "Some moths never eat anything as adults because they don't have mouths. ", "subject": "moths", "answer": "mouths"},
	# 			{"text": "'Stewardesses' is the longest word typed with only the left hand.", "subject": "Stewardesses", "answer": "hand"},
	# 			{"text": "An average human loses about 200 head hairs per day.", "subject": "human", "answer": "hairs"},
	# 			{"text": "It is impossible to sneeze with your eyes open.", "subject": "It", "answer": "eyes"},
	# 			{"text": "In France, a five year old child can buy an alcoholic drink in a bar.", "subject": "child", "answer": "drink"},
	# 			{"text": "During the chariot scene in 'Ben Hur', a small red car can be seen in the distance.", "subject": "car", "answer": "distance"},
	# 			{"text": "Because metal was scarce, the Oscars given out during World War II were made of wood.", "subject": "Oscars", "answer": "wood"},
	# 			# {"text":"By raising your legs slowly and lying on your back, you cannot sink into quicksand.", "subject": "you", "answer": "quicksand"},
	# 			{"text": "The glue on Israeli postage is certified kosher.", "subject": "glue", "answer": "kosher"},
	# 			{"text": "In 10 minutes, a hurricane releases more energy than all of the world's nuclear weapons combined.", "subject": "hurricane", "answer": "energy"},
	# 			{"text": "On average, 100 people choke to death on ball-point pens every year.", "subject": "people", "answer": "death"},
	# 			# {"text":"Thirty-five percent of the people who use personal ads for dating are already married.", "subject": "people", "answer": "married"},
	# 			{"text": "The electric chair was invented by a dentist.", "subject": "chair", "answer": "dentist"},
	# 			# {"text":"The top butterfly flight speed is 12 miles per hour.]", "subject": "speed", "answer": "miles"},
	# 			{"text": "During the Gold Rush years in northern California, Los Angeles became known as the 'Queen of the Cow Counties' for its role in supplying beef and other foodstuffs to hungry miners in the north.", "subject": "Angeles", "answer": "Queen"}
	# ]





	# test_text = [
	# 			# WHO sentences
	# 			{"text": "The pancreas produces Insulin.", "answer": "pancreas"},
	# 			{"text": "The door was opened by Ram.", "answer": "Ram"},
	# 			{"text": "Mary was appointed as the secretary.", "answer": "Mary"},
	# 			{"text": "Hari was cleaning the car yesterday.", "answer": "Hari"},
	# 			{"text": "The engineer lived far away from the chaotic life in California.", "answer": "engineer"},
	# 			{"text": "The chef took too long to prepare the lasagna.", "answer": "chef"},
	# 			{"text": "In 1980, the fastest swimmer in the world was Jefferson", "answer": "Jefferson"},
	# 			{"text": "Zookeeper takes care of animals at the zoo", "answer": "Zookeeper"},
	# 			{"text": "Dentist checks our teeth for cavities.", "answer": "Dentist"},
	# 			{"text": "Clerk helps people in the store.", "answer": "Clerk"},
	# 			{"text": "Barber has a job of cutting people's hair.", "answer": "Barber"},

	# ]

	test_text = [
				# WHEN sentences
				{"text": "We eat breakfast in the morning.", "answer": "morning"},
				# {"text": "when it is sunny outside, we wear sunglasses", "answer": "sunny"},
				# {"text": "when it is cold outside, we wear a coat", "answer": "cold"},
				{"text": "They visited the zoo at 4 o'clock.", "answer": "4"},
				{"text": "Hari stepped in the office at around 8 am.", "answer": "8"},
				{"text": "In 2001, he visited the neighbouring country India.", "answer": "2001"},
				{"text": "He left the country in 2005.", "answer": "2005"},
				{"text": "They used to visit the place every Monday.", "answer": "Monday"},

				{"text": "Wedding is scheduled on 13th of this month.", "answer": "13th"},
				{"text": "Wedding is scheduled to take place on 13th of this month.", "answer": "13th"},
				{"text": "She was born on Diwali.", "answer": "Diwali"},
				{"text": "The theft took place at 17th June.", "answer": "17th"},
				{"text": "He started studying at 8 am.", "answer": "8"},

	]





	y_test = []
	all_y_pred = []
	for text in test_text:
		text["text"] = text["text"].lower()
		text["text"] = ''.join(e for e in text["text"] if e.isalnum() or e == " ")

		print "text -> ", text["text"]

		# y_test.append(text["subject"])
		y_test.append(text["answer"])
		# y_test.append(text["verb"])

		output = call_stanford_parser_server_api.get_output(text["text"])
		# print json.dumps(output["sentences"][0]["tokens"], indent=4, sort_keys=True)
		# print "output", output

		text_stripped = text["text"].split()

		X = []
		y_label_word = []
		y_label_pos = []
		for word in text_stripped:
			x, y = feat_generator.parse_output(output, word)
			X.append(x)
			y_label_word.append(word)
			y_label_pos.append(y)

		X = numpy.array(X)

		# X = X[:, feats_select]


		# all_pred, all_pred_stats = predict_depr(X_train, X, y_train)
		# # print "all_pred_stats", all_pred_stats
		# # print "all_pred", all_pred
		# # exit(0)
		# print output["sentences"][0]["parse"]

		# y_pos_pred = None
		# y_label_pred = None
		# for pred in all_pred:
		# 	if pred in y_label_pos:
		# 		y_pos_pred = pred
		# 		index = y_label_pos.index(y_pos_pred)
		# 		y_label_pred = y_label_word[index]
		# 		break
		# all_y_pred.append(y_label_pred)




		# y_label_pred = predict(X_train, X, y_train, y_label_word)
		# all_y_pred.append(y_label_pred)

		# y_label_pred = predict_by_multi_score(X_train, X, y_train, y_label_word)
		# all_y_pred.append(y_label_pred)

		# y_label_pred = predict_by_multi_max_score(X_train, X, y_train, y_label_word)
		# all_y_pred.append(y_label_pred)

		# y_label_pred = predict_by_weighted_avg(X_train, X, y_train, y_label_word)
		# all_y_pred.append(y_label_pred)

		y_label_pred = predict_by_deep_weighted_avg(X_train, X, y_train, y_label_word)
		all_y_pred.append(y_label_pred)

		tree = Tree.fromstring(output["sentences"][0]["parse"])
		tree.draw()





	print "y_test", y_test
	print "all_y_pred", all_y_pred

	accuracy = sum([1 for index in xrange(0, len(y_test)) if y_test[index].lower() == all_y_pred[index]])/float(len(y_test))
	print "accuracy", accuracy





