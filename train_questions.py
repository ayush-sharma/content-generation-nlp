import feat_generator
import numpy
import json
from sklearn.feature_selection import VarianceThreshold
from nltk import Tree
from nltk.draw.tree import TreeView
import call_stanford_parser_server_api


if __name__ == "__main__":

	# print call_stanford_parser_server_api.get_output("During the Gold Rush years in northern California, Los Angeles became known as the 'Queen of the Cow Counties' for its role in supplying beef and other foodstuffs to hungry miners in the north.")
	# print call_stanford_parser_server_api.get_output("We decided to attend the party.")
	# print call_stanford_parser_server_api.get_output("Our skull is made up of 29 different bones.")
	# print call_stanford_parser_server_api.get_output("Los Angeles became known as the 'Queen of the Cow Countries' for its role in supplying beef and other foodstuffs to hungry miners in the north.")
	# print call_stanford_parser_server_api.get_output("Canadian researchers have found that Einstein's brain was 15% wider than normal.")
	# print call_stanford_parser_server_api.get_output("in france a five year old child can buy an alcoholic drink in a bar")
	# print call_stanford_parser_server_api.get_output("in 10 minutes a hurricane releases more energy than all of the worlds nuclear weapons combined")
	# print call_stanford_parser_server_api.get_output("The little yellow dog barked at the cat.")
	# exit(0)

	# input_text = [
	# 			{"text": "Canadian researchers have found that Einstein's brain was 15% wider than normal.", "subject": "brain", "answer": "wider", "verb": "found"},
	# 			{"text": "James had a nightmare", "subject": "James", "answer": "nightmare", "verb": "had"},
	# 			{"text": "Bird has wings", "subject": "Bird", "answer": "wings", "verb": "has"},
	# 			{"text": "we are going to the party", "subject": "we", "answer": "party", "verb": "going"},
	# 			{"text": "we decided to attend the party", "subject": "we", "answer": "party", "verb": "decided"},
	# 			{"text": "Our skull is made up of 29 different bones.", "subject": "skull", "answer": "bones", "verb": "made"},
	# 			{"text": "The human body is comprised of 80\% water.", "subject": "body", "answer": "water", "verb": "comprised"},
	# 			{"text": "Human thighbones are stronger than concrete.", "subject": "thighbones", "answer": "concrete", "verb": "stronger"},
	# 			{"text": "Frank Lloyd Wright's son invented Lincoln Logs.", "subject": "son", "answer": "Lincoln", "verb": "invented"},
	# 			{"text": "Rodent's teeth never stop growing.", "subject": "teeth", "answer": "growing", "verb": "stop"},
	# 			{"text": "The shortest British monarch was Charles I.", "subject": "shortest", "answer": "Charles", "verb": "was"},
	# 			{"text": "The penguin is the only bird that can swim.", "subject": "penguin", "answer": "swim", "verb": "is"},
	# 			{"text": "A crocodiles tongue is attached to the roof of its mouth.", "subject": "tongue", "answer": "roof", "verb": "attached"},
	# 			{"text": "Beethoven dipped his head in cold water before he composed.", "subject": "Beethoven", "answer": "water", "verb": "dipped"},
	# 			{"text": "Astronaut Neil Armstrong first stepped on the moon with his left foot.", "subject": "Astronaut", "answer": "moon", "verb": "stepped"},
	# 			{"text": "There are 45 miles of nerves in the skin of a human being.", "subject": "nerves", "answer": "skin", "verb": "in"},
	# 			{"text": "The average human will shed 40 pounds of skin in a lifetime.", "subject": "human", "answer": "skin", "verb": "shed"},
	# 			{"text": "Hair is made from the same substance as fingernails.", "subject": "Hair", "answer": "same", "verb": "made"},
	# 			{"text": "Ancient Egyptians shaved off their eyebrows to mourn the deaths of their cats.", "subject": "Egyptians", "answer": "deaths", "verb": "shaved"},
	# 			{"text": "15 million blood cells are destroyed in the human body every second.", "subject": "cells", "answer": "body", "verb": "destroyed"},

				# {"text":"Negative emotions such as anxiety and depression can weaken your immune system.", "subject": "emotions", "answer": "system"},
				# # {"text":"Stephen Hawking was born exactly 300 years after Galileo died.", "subject": "Hawking", "answer": "exactly"},
				# {"text":"Mercury is the only planet whose orbit is coplanar with its equator.", "subject": "Mercury", "answer": "coplanar"},
				# {"text":"Some moths never eat anything as adults because they don't have mouths. ", "subject": "moths", "answer": "mouths"},
				# {"text":"'Stewardesses' is the longest word typed with only the left hand.", "subject": "Stewardesses", "answer": "hand"},
				# {"text":"An average human loses about 200 head hairs per day.", "subject": "human", "answer": "hairs"},
				# {"text":"Mexico City sinks about 10 inches a year.", "subject": "City", "answer": "inches"},
				# {"text":"It is impossible to sneeze with your eyes open.", "subject": "It", "answer": "eyes"},
				# {"text":"In France, a five year old child can buy an alcoholic drink in a bar.", "subject": "child", "answer": "drink"},
				# {"text":"During the chariot scene in 'Ben Hur', a small red car can be seen in the distance.", "subject": "car", "answer": "distance"},
				# {"text":"Because metal was scarce, the Oscars given out during World War II were made of wood.", "subject": "Oscars", "answer": "wood"},
				# # {"text":"By raising your legs slowly and lying on your back, you cannot sink into quicksand.", "subject": "you", "answer": "quicksand"},
				# {"text":"The glue on Israeli postage is certified kosher.", "subject": "glue", "answer": "kosher"},
				# {"text":"In 10 minutes, a hurricane releases more energy than all of the world's nuclear weapons combined.", "subject": "hurricane", "answer": "energy"},
				# {"text":"On average, 100 people choke to death on ball-point pens every year.", "subject": "people", "answer": "death"},
				# # {"text":"Thirty-five percent of the people who use personal ads for dating are already married.", "subject": "people", "answer": "married"},
				# {"text":"The electric chair was invented by a dentist.", "subject": "chair", "answer": "dentist"},
				# # {"text":"The top butterfly flight speed is 12 miles per hour.", "subject": "speed", "answer": "miles"}
	# ]


	# input_text = [
	# 			# WHO sentences
	# 			{"text":"Ronaldo is the best football player in the world", "answer": "Ronaldo"},
	# 			{"text":"Ram is my best friend.", "answer": "Ram"},
	# 			{"text":"The strange guy over there is John.", "answer": "John"}, # passive
	# 			{"text":"James writes good poems.", "answer": "James"},
	# 			{"text":"He can speak Chinese.", "answer": "He"},
	# 			{"text":"Bill Gates is among the most rich people in the world.", "answer": "Bill"},
	# 			{"text":"Gopal loves to watch movies in theatres.", "answer": "Gopal"},
	# 			{"text":"The only person to dance in the office is Mary.", "answer": "Mary"}, # passive
	# 			{"text":"Martha loves chinese food.", "answer": "Martha"},
	# 			{"text":"In 2000, the company was led by Larry Page", "answer": "Larry"}, # passive
	# 			{"text":"The elected chairman of the committee was Michael.", "answer": "Michael"}, # passive
	# 			{"text":"Chinese food is loved by Martha.", "answer": "Martha"}, # passive
	# 			{"text":"Stephen Hawking was born exactly 300 years after Galileo died.", "answer": "Stephen"},
	# 			{"text":"Thomas knows her very well.", "answer": "Thomas"},
	# 			{"text":"The actor smiled at us vivaciously.", "answer": "actor"},
	# 			{"text":"Martha loves chinese food.", "answer": "Martha"},
	# 			{"text":"Mexico City sinks about 10 inches a year.", "answer": "Mexico"},
	# 			{"text":"The glue on Israeli postage is certified kosher.", "answer": "glue"},
	# 			{"text":"An average human loses about 200 head hairs per day.", "answer": "human"}
	# 			{"text":"Firefighter puts out the fire.", "answer": "Firefighter"},
	# 			{"text":"Teacher teaches kids at school.", "answer": "Teacher"},
	# 			{"text":"Postman delivers mail at the door.", "answer": "Postman"},
	# 			{"text":"Veterinarian takes care of sick animals.", "answer": "Veterinarian"},
	# 			{"text":"Pilot flies an airplane high up in the sky.", "answer": "Pilot"},

	# ]






	input_text = [
				# WHEN sentences
				{"text": "We sleep at night", "answer": "night"},
				# {"text": "when we are dirty, we take a bath", "answer": "dirty"},
				# {"text": "when we bleed, we put on a bandaid", "answer": "bleed"},
				# {"text": "when it is raining, we use an umbrella", "answer": "raining"},
				{"text": "he leaves the office at 9 o'clock in the evening.", "answer": "evening"},
				{"text": "Jefferson was elected as president in 1908.", "answer": "1908"},
				{"text": "In 2000, Google was led by Larry Page.", "answer": "2000"},
				{"text": "She used to go for shopping every sunday.", "answer": "every"},
				{"text": "We did not use flashlight until dark.", "answer": "dark"},
				{"text": "Rain stopped in the night.", "answer": "night"},
				{"text": "He goes to school everyday.", "answer": "everyday"},
				{"text": "Recession hit the nation in 2009.", "answer": "2009"},
				{"text": "It was raining yesterday.", "answer": "yesterday"},
				{"text": "Convocation is scheduled to happen on 26th July.", "answer": "26th"},
				{"text": "He flunked in the yesterday exam.", "answer": "yesterday"},
				{"text": "John was texting her in the night.", "answer": "night"},
				{"text": "He became 23 years old on this tuesday.", "answer": "tuesday"},
				{"text": "He is going to leave the city tomorrow.", "answer": "tomorrow"},
				{"text": "He is going to leave the city on 1st July.", "answer": "1st"},
				{"text": "I usually go to bed at around 11 o'clock.", "answer": "11"},
				{"text": "Ronaldo was the best football player in 2008.", "answer": "2008"},
				{"text": "In 1920, NASA moved its headquarters to California.", "answer": "1920"},
				{"text": "The film is going to release on 13th October.", "answer": "13th"},
				{"text": "He was born on 28th July, 1933", "answer": "28th"},
				{"text": "They left the house at 4 pm.", "answer": "4"},
				{"text": "The train arrived at the station at 3 am.", "answer": "3"},
				{"text": "I ate the food at around 8 o'clock.", "answer": "8"},
				{"text": "We had an icecream yesterday.", "answer": "yesterday"},
				{"text": "The river flooded at 4 o'clock in the morning.", "answer": "4"},
	]






	X_train = []
	y_train = []

	for text in input_text:
		text["text"] = text["text"].lower()
		text["text"] = ''.join(e for e in text["text"] if e.isalnum() or e == " ")
		
		print "text -> ", text["text"]
		output = call_stanford_parser_server_api.get_output(text["text"])

		# t = Tree.fromstring(output["sentences"][0]["parse"])
		# t.draw()

		# continue

		# print json.dumps(output["sentences"][0]["basic-dependencies"], indent=4, sort_keys=True)
		# print "output", json.dumps(output)

		# X, y_label = feat_generator.parse_output(output, text["subject"])
		X, y_label = feat_generator.parse_output(output, text["answer"])
		# X, y_label = feat_generator.parse_output(output, text["verb"])


		# print "shape", X.shape
		# print X
		# print y_label
		# raw = raw_input('paused')
		X_train.append(X)
		y_train.append(y_label)

	# exit(0)
		

	X_train = numpy.array(X_train)
	y_train = numpy.array(y_train)

	print "shape", X_train.shape

	# selector = VarianceThreshold(threshold=(.8 * (1 - .8)))
	# selector.fit_transform(X_train)
	# feats_select = selector.get_support()
	# X_train = X_train[:, feats_select]
	# numpy.save('feats_select.npy', feats_select)
	# print "shape feats_select", feats_select.shape

	numpy.save('X_train.npy', X_train)
	numpy.save('y_train.npy', y_train)

	print "shape X_train", X_train.shape
	print "shape y_train", y_train.shape

	print "\n\n\n", X_train, "\n\n", y_train

