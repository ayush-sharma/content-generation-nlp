Template.fileUpload.events({
    'change .myFileInput': function(event, template) {
        console.log('triggered');

        var files = event.target.files;
        for (var i = 0, ln = files.length; i < ln; i++) {
            FilesColl.insert(files[i], function(err, fileObj) {
                if (err) {
                    console.log('err', err);
                } else {
                    fileObj.on('uploaded', function() {
                        console.log('uploaded');
                        console.log('saved', fileObj["_id"]);
                        Meteor.call('call_pdf_parser', fileObj["_id"], function(err, result) {
                            if (err) {
                                console.log('err', err);
                            } else {
                                console.log('result', fileObj);
                                document.getElementById("parsed_text").innerHTML = "<center><h2>Parsed Text</h2>" + result + "</center>";
                            }
                        });
                    });
                }
            });
        }
    }
})
