Template.define.helpers({
    getSamples: function() {
        return Samples.find();
    }
});
Template.define.events({
    'click #submitDefine': function() {
        var text = document.getElementById('text').value
        var qs = document.getElementById('qs').value
        var ans = document.getElementById('ans').value
        Meteor.call('getParsedUnderstanding', text, qs, ans)
    }
});
