Session.setDefault('selectedSample', "")
Session.setDefault('similar', [])
Session.setDefault('selectedPdf', "")
Template.induct.helpers({
    getInducted: function() {
        return Samples.find()
    },
    getPdf: function() {
        return Base.find()
    },
    getSim: function() {
        objs = Session.get('similar')
        function compare(a, b) {
            a.score = parseFloat(a.score)
            b.score = parseFloat(b.score)
            console.log(typeof a.score, b.score)
            if (a.score < b.score)
                return -1;
            if (a.score > b.score)
                return 1;
            return 0;
        }
        objs.sort(compare);

        return objs

    }
});
Template.induct.events({
    'click #submitBase': function() {
        var name = document.getElementById('baseName').value
        var text = document.getElementById('baseText').value
        console.log(name, text)
        if (name == "") {
            name = text.substring(0, 10) + ",custom"
        }
        Base.insert({ "name": name, "text": text, "similar": [] })
    },
    'change .selectSample': function() {
        var x = document.getElementById('selectInductRule')
        var id = x.options[x.selectedIndex].id
        console.log("clicked", id)
        Session.set("selectedSample", id)
        var similar = Samples.findOne({ "_id": Session.get('selectedSample') }).similar
        var found = 0
        for (var i = 0; i < similar.length; i++) {
            console.log("-------", similar[i].base, Session.get('selectedPdf'))
            if (similar[i].base == Session.get('selectedPdf')) {
                console.log("FOUND SIMILAR", similar[i].similarArray)
                Session.set('similar', similar[i].similarArray)
                found = 1
            }
        }
        if (found == 0) {
            Session.set('similar', [{ "qs": "Nothing", "text": "similar", "ans": "generated" }])
        }


    },
    'click #updateInduct': function() {
        var baseId = Session.get('selectedPdf')
        var sampleId = Session.get('selectedSample')
        console.log(baseId, sampleId)
        Meteor.call('getSimilarContent', baseId, sampleId)
    },
    'click .selectPdf': function(event) {
        console.log(event.target.id)
        Session.set("selectedPdf", event.target.id)
        var similar = Samples.findOne({ "_id": Session.get('selectedSample') }).similar
        var found = 0
        for (var i = 0; i < similar.length; i++) {
            // console.log("-------", similar[i].base, Session.get('selectedPdf'))
            if (similar[i].base == Session.get('selectedPdf')) {
                console.log("FOUND SIMILAR", similar[i].similarArray)
                Session.set('similar', similar[i].similarArray)
                found = 1
            }
        }
        if (found == 0) {
            Session.set('similar', [{ "qs": "Nothing", "text": "similar", "ans": "generated" }])
        }
    }
});
