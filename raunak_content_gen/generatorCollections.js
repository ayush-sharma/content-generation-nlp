// Intent = new Mongo.Collection("intent")
// Rules = new Mongo.Collection("rules")
// Segments = new Mongo.Collection("segments")
// SynBank = new Mongo.Collection("synBank")
Samples = new Mongo.Collection("samples")
// Generated = new Mongo.Collection("generated")
// Inducted = new Mongo.Collection("inducted")
Base = new Mongo.Collection("base")
// Vocab = new Mongo.Collection("vocab")
// QsMap = new Mongo.Collection("qsMap")
// GramSample = new Mongo.Collection("gramSample")
// CompSample = new Mongo.Collection("compSample")
// Pdf = new Mongo.Collection("pdf")
// PdfBase = new Mongo.Collection("pdfBase")
FilesColl = new FS.Collection("files", {
    stores: [new FS.Store.FileSystem("files", { path: "/Volumes/backup/generatorDemoApi/server" })]
});

FilesColl.allow({
    'insert': function() {
        // add custom authentication code here
        return true;
    }
});