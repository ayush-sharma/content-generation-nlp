var root_folder = process.env.PWD

var pythonApi = "http://localhost:5000/"
var urlExtension = {
    "getParsedUnderstanding": "sample",
    "getSimilarContent": "similar"
}
var Future = Npm.require('fibers/future');

Meteor.methods({
    'getParsedUnderstanding': function(text, qs, ans) {
        HTTP.call('GET', pythonApi + urlExtension.getParsedUnderstanding, {
            params: {
                'text': text,
                'qs': qs,
                'ans': ans
            }
        }, function(error, response) {
            if (error) {
                console.log(error);
            } else {
                console.log(response.statusCode);
                Samples.insert({ "qs": qs, "text": text, "ans": ans })
                var id = Samples.findOne({ "qs": qs, "text": text, "ans": ans })._id
                Samples.update(id, {
                    $set: {
                        understanding: JSON.parse(
                            response.content)
                    }
                })

            }

        })
    },
    'getSimilarContent': function(baseId, sampleId) {
        sample = JSON.stringify(Samples.findOne({ "_id": sampleId }).understanding)
        base = Base.findOne({ "_id": baseId }).text
        console.log(baseId, sampleId)
        console.log("SAMPLE", sample, "BASE", base)
        HTTP.call('GET', pythonApi + urlExtension.getSimilarContent, {
            params: {
                'text': base,
                'objectsUnderstanding': sample
            }
        }, function(error, response) {
            if (error) {
                console.log(error);
            } else {
                var sim = JSON.parse(response.content)
                console.log(response.statusCode);
                console.log("sim:", sim)

                var simArray = []
                for (var i = 0; i < sim.length; i++) {
                    var text = sim[i].object[0]
                    var qs = sim[i].object[1]
                    var ans = sim[i].object[2]
                    simArray.push({
                        "text": text,
                        "qs": qs,
                        "ans": ans,
                        "score": sim[i].score.toFixed(2)
                    })
                }

                Samples.update(sampleId, {
                    $push: {
                        similar: {
                            "base": baseId,
                            "similarArray": simArray
                        }
                    }
                })
            }
        })
    },
    "call_pdf_parser": function(file_id) {
        console.log('in call_pdf_parser', file_id);
        var exec = Npm.require('child_process').exec;
        var runCmd = Meteor.wrapAsync(exec);
        var future = new Future();

        var cmdStr = "python ../../../../../server/pdf_parser.py " + String(file_id);

        var result = runCmd(cmdStr, function(err, stdout, stderr) {
            if (err) {
                console.log('err', err);
                future.return("err!!!");
            } else if (stdout) {
                // console.log('stdout', stdout);
                future.return(stdout);
            } else {
                console.log('stderr', stderr);
                future.return("err!!!");
            }
        });

        return future.wait();
    }
})
Meteor.startup(function() {
    // Samples.remove({"text":"mark is a good chef"})
    // Base.remove({"_id":"JDYC2obmyNosDZdsD"})
});
