import sys
import os
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from cStringIO import StringIO
from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:3001/meteor')
uploaded_files_collection = client['meteor']['cfs.files.filerecord']

def convert_pdf_to_txt(file_id):

	file_attr = uploaded_files_collection.find_one({"_id": file_id})
	path = os.path.split(str(os.path.dirname(os.path.realpath(__file__))))[0] + "/server/" + file_attr["copies"]["files"]["key"]

	rsrcmgr = PDFResourceManager()
	retstr = StringIO()
	codec = 'utf-8'
	laparams = LAParams()
	device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
	fp = file(path, 'rb')
	interpreter = PDFPageInterpreter(rsrcmgr, device)
	password = ""
	maxpages = 0
	caching = True
	pagenos=set()

	for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
		interpreter.process_page(page)

	text = retstr.getvalue()

	fp.close()
	device.close()
	retstr.close()
	return text



if __name__ == "__main__":
	file_id = sys.argv[1]
	# print "in pdf_parser", file_id
	print convert_pdf_to_txt(file_id)

