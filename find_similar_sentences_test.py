# -*- coding: utf-8 -*-
import feat_generator
import numpy
import json
from sklearn.feature_selection import VarianceThreshold
from nltk import Tree
from nltk.draw.tree import TreeView
import call_stanford_parser_server_api
import collections
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.externals import joblib
import sys
import operator




if __name__ == "__main__":

	input_text = [
		{"text": "My old father shoots a red car accurately.", "entities": {"subject": "father", "adj": "old", "verb": "shoots", "adverb": "accurately", "object": "car", "obj_adj": "red"}},
		{"text": "The final exams were unbelievably difficult.", "entities": {"subject": "exams", "adj": ["final", "difficult"], "verb": "were", "adverb": "unbelievably", "object": "difficult"}},
		{"text": "This pie is very delicious.", "entities": {"subject": "pie", "verb": "is", "adverb": "very", "adj": "delicious", "object": "delicious"}},
		{"text": "A person smarter than me needs to figure this out.", "entities": {"subject": "person", "adverb": "smarter", "object": "figure", "verb": "needs"}},
		{"text": "The fat boy went under the big bridge secretly at night.", "entities": {"subject": "boy", "verb": "went", "adverb": "secretly", "adj": "fat", "obj_adj": "big", "object": "bridge", "moment": "night"}},
		{"text": "John is an engineer.", "entities": {"subject": "John", "verb": "is", "object": "engineer"}},
		{"text": "Americans had been working on supercomputers since 1980.", "entities": {"subject": "Americans", "verb": "working", "object": "supercomputers", "moment": "1980"}},
		{"text": "One in every 4 Americans has appeared on television.", "entities": {"subject": "Americans", "verb": "appeared", "object": "television"}},
		{"text": "A skunk's smell can be detected by a human.", "entities": {"subject": "smell", "verb": "detected", "object": "human"}},
		{"text": "The mother of Michael Nesmith invented whiteout.", "entities": {"subject": "mother", "verb": "invented", "object": "whiteout"}},
		{"text": "Isaac Asimov is the only author to have a book in every Dewey-decimal category.", "entities": {"subject": "Asimov", "verb": "is", "adj": "only", "object": "book"}},
		{"text": "Barbie's full name is 'Babara Millicent Roberts.'", "entities": {"subject": "name", "verb": "is", "object": "Babara"}},
		{"text": "Shakespeare invented the word 'assassination' and 'bump'.", "entities": {"subject": "Shakespeare", "verb": "invented", "object": "word"}},
		{"text": "Tina Turner's real name is Annie Mae Bullock.", "entities": {"subject": "name", "verb": "is", "object": "Annie"}},
		{"text": "Beethoven dipped his head in cold water before he composed.", "entities": {"subject": "Beethoven", "verb": "dipped", "object": "head", "moment": "composed"}},
		{"text": "President John F Kennedy could read 4 newspapers in 20 minutes.", "entities": {"subject": "Kennedy", "verb": "read", "object": "newspapers", "moment": "20"}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},

		# {"text": "Barbie's full name is 'Babara Millicent Roberts.'", "entities": {"subject": "name", "verb": "is", "object": "Babara"}},

	]

	train_feats = numpy.load('train_feats.npy').tolist()


	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])

		feats = feat_generator.get_feats(stanford_output)

		feats = feats.flatten()

		print feat_generator.get_similar_sentences(feats, train_feats)
		exit(0)

