# Content Generation #

A named-entity Core-NLP powered project developed to generate realtime content similar to a given set of raw text in terms of difficulty, relevance and grammar.

### Motivation ###

To build a content generation model using NLP semantics and making the model to learn the content (be it Science, English, fiction) by making a hierarchical entity-concept mapping, so that the model itself will assess the student's performance and question with a variable/personalized difficulty from the same global set of content.

### Synopsis ###

* The short term goal is to create a entity-concept relational mapping structured in an accessible way.
* We used Stanford CoreNlp to decipher the basic dependencies, pos tagging and coreference.
* The long term goal is to create a platform where a teacher can input a few sentences and asks to create a reading passage having sentences of same difficulty, semantics, grammar and parts of speech.
* This has to be achieved in real-time after the teacher e-mails the input sample texts and expects the students get a link of the reading comprehension and its quiz.

### Approach ###

1. For a given input text, find all the mappings between relations and make the complex/multiple-sentences-in-one sentences to simple multiple sentences.
2. Now, form the sentences again from the deduced mapping.
3. Also, form sentences by shuffling the entities from other sentences so that we can provide a large number of output sentences from the same small text passage(its a requirement). Later, we will sort according to relevance.
4. Using the entity mapping data structure, form questions as well. This can be achieved by training a model having entity mappings as input and question type as output.
5. Now, rank the formed sentences according to their relevance, grammar and meaningfullness. This can be achieved by using [Concept Net - 5](https://github.com/commonsense/conceptnet5) which provides a similarity index between any entities.
6. Now with questions, text and answers, we can create a real-time reading comprehension and its questions and conve it to the students live.