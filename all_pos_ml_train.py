# -*- coding: utf-8 -*-
import feat_generator
import numpy
import json
from sklearn.feature_selection import VarianceThreshold
from nltk import Tree
from nltk.draw.tree import TreeView
import call_stanford_parser_server_api
import collections
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.externals import joblib
import sys
import os



if __name__ == "__main__":

	input_text = [
		{"text": "The final exams were unbelievably difficult.", "entities": {"subject": "exams", "adj": ["final", "difficult"], "verb": "were", "adverb": "unbelievably", "object": "difficult"}},
		{"text": "This pie is very delicious.", "entities": {"subject": "pie", "verb": "is", "adverb": "very", "adj": "delicious", "object": "delicious"}},
		{"text": "A person smarter than me needs to figure this out.", "entities": {"subject": "person", "adverb": "smarter", "object": "figure", "verb": "needs"}},
		{"text": "Everyone was extremely delighted when the winner was announced.", "entities": {"subject": "Everyone", "object": "winner", "verb": "delighted", "adverb": "extremely", "prep_ph": "announced"}},
		{"text": "The new outfit was very pricey but really beautiful.", "entities": {"subject": "outfit", "verb": "was", "adj": ["new", "pricey", "beautiful"], "adverb": ["very", "really"]}},
		{"text": "The cost of the car is way too high.", "entities": {"subject": "cost", "verb": "is", "adverb": ["way", "too"], "adj": "high"}},
		{"text": "Upset students staged a rally.", "entities": {"subject": "students", "adj": "Upset", "verb": "staged", "object": "rally"}},
		{"text": "That huge complex has quite small but cheap apartments.", "entities": {"subject": "complex", "adj": "huge", "verb": "has", "obj_adj": ["small", "cheap"], "adverb": "quite", "object": "apartments"}},
		{"text": "Her beautiful eyes were very mesmerizing to the young man.", "entities": {"subject": "eyes", "verb": "mesmerizing", "adverb": "very", "object": "man", "adj": "beautiful"}},
		{"text": "The highly emotive actor gave a wonderful performance.", "entities": {"subject": "actor", "verb": "gave", "adverb": "highly", "adj": "emotive", "obj_adj": "wonderful", "object": "performance"}},
		{"text": "The extremely tired kitten fell asleep by her food dish.", "entities": {"subject": "kitten", "verb": "fell", "adverb": "extremely", "adj": "tired", "object": "asleep"}},
		{"text": "Eating out is usually not very healthy.", "entities": {"subject": "Eating", "verb": "is", "adverb": ["usually", "not", "very"], "adj": "healthy", "object": "healthy"}},
		{"text": "I was fairly bored with her.", "entities": {"subject": "I", "verb": "bored", "adverb": "fairly", "object": "her"}},
		{"text": "The original tapestry was beautifully stitched by rugged hands.", "entities": {"subject": "tapestry", "verb": "stitched", "adverb": "beautifully", "adj": "original", "obj_adj": "rugged", "object": "hands"}},
		{"text": "James writes good poems effectively.", "entities": {"subject": "James", "verb": "writes", "adverb": "effectively", "obj_adj": "good", "object": "poems"}},
		{"text": "The overly enthusiastic fans painted their bodies with the team's colors.", "entities": {"subject": "fans", "verb": "painted", "adverb": "overly", "adj": "enthusiastic", "object": "bodies"}},
		{"text": "Ronaldo is the best football player in the world", "entities": {"subject": "Ronaldo", "verb": "is", "obj_adj": "best", "object": "player"}},
		{"text": "He can speak Chinese fluently.", "entities": {"subject": "He", "verb": "speak", "adverb": "fluently", "object": "Chinese"}},
		{"text": "Ram is my best friend.", "entities": {"subject": "Ram", "verb": "is", "adj": "best", "object": "friend"}},
		{"text": "Gopal loves to watch scary movies in theatres.", "entities": {"subject": "Gopal", "verb": "loves", "obj_adj": "scary", "object": ["watch", "movies"]}},
		{"text": "The only person to dance in the office is Mary.", "entities": {"subject": "Mary", "verb": "dance", "obj_adj": "only", "object": "person"}},
		{"text": "Martha loves spicy chinese food.", "entities": {"subject": "Martha", "verb": "loves", "obj_adj": ["spicy", "chinese"], "object": "food"}},
		{"text": "In 2000, the insolvent company was led by well-knowned entrepreneur Larry", "entities": {"subject": "Larry", "verb": "led", "adj": "well-knowned", "obj_adj": "insolvent", "object": "company", "prep_ph": "2000"}},
		{"text": "Stephen was born after 300 years apparently.", "entities": {"subject": "Stephen", "verb": "born", "adverb": "apparently", "obj_adj": "300", "object": "years"}},
		{"text": "An average human loses about 200 hairs per day obscurely.", "entities": {"subject": "human", "verb": "loses", "adverb": "obscurely", "adj": ["average", "human"], "obj_adj": "200", "object": "hairs"}},
		{"text": "Thomas knows her very well.", "entities": {"subject": "Thomas", "verb": "knows", "adverb": ["very", "well"], "object": "her"}},
		{"text": "The histrionic actor smiled at the waiting audience vivaciously.", "entities": {"subject": "actor", "verb": "smiled", "adverb": "vivaciously", "adj": "histrionic", "obj_adj": "waiting", "object": "audience"}},
		{"text": "The skilled Veterinarian took care of the sick animals empathically.", "entities": {"subject": "Veterinarian", "verb": "took", "adverb": "empathically", "adj": "skilled", "object": "care"}},
		{"text": "The tired kids slept at dark night soundlessly.", "entities": {"subject": "kids", "verb": "slept", "adverb": "soundlessly", "adj": "tired", "obj_adj": "dark", "object": "night", "prep_ph": "night"}},
		{"text": "He silently left the office in the evening.", "entities": {"subject": "He", "verb": "left", "adverb": "silently", "prep_ph": "evening", "object": "office"}},
		{"text": "Jefferson was elected as honorable president in 1908 widely.", "entities": {"subject": "Jefferson", "verb": "elected", "prep_ph": "1908", "obj_adj": "honorable", "object": "president"}},
		{"text": "The well-acquainted philosopher went to the mighty United States on Halloween secretly.", "entities": {"subject": "philosopher", "verb": "went", "adj": "well-acquainted", "obj_adj": "mighty", "object": "States", "adverb": "secretly"}},
		{"text": "The mighty King visits the astonishing palace every Monday secretly.", "entities": {"subject": "King", "verb": "visits", "adverb": "secretly", "adj": "mighty", "obj_adj": "astonishing", "object": "palace", "prep_ph": "Monday"}},
		{"text": "The unstoppable rain ceased at night.", "entities": {"subject": "rain", "verb": "ceased", "adj": "unstoppable", "object": "night", "prep_ph": "night"}},
		{"text": "He goes to school everyday.", "entities": {"subject": "He", "verb": "goes", "object": "school", "prep_ph": "everyday"}},
		{"text": "Recession hit the nation in 2009.", "entities": {"subject": "Recession", "verb": "hit", "object": "nation", "prep_ph": "2009"}},
		{"text": "It was raining heavily yesterday.", "entities": {"subject": "It", "verb": "raining", "adverb": "heavily", "object": "yesterday", "prep_ph": "yesterday"}},
		{"text": "Bryan Adams will perform on 8th July apparently.", "entities": {"subject": "Adams", "verb": "perform", "adverb": "apparently", "object": ["8th", "July"], "prep_ph": ["8th", "July"]}},
		{"text": "Convocation is scheduled on 26th of this month.", "entities": {"subject": "Convocation", "verb": "scheduled", "object": ["26th", "month"], "prep_ph": ["26th", "month"]}},
		{"text": "He flunked in the yesterday's exam.", "entities": {"subject": "He", "verb": "flunked", "obj_adj": "yesterday", "object": "exam", "prep_ph": "yesterday"}},
		{"text": "Silly John was texting her in the night endlessly.", "entities": {"subject": "John", "verb": "texting", "adverb": "endlessly", "adj": "Silly", "object": "her", "prep_ph": "night"}},
		{"text": "He is leaving the city tomorrow.", "entities": {"subject": "He", "verb": "leaving", "object": "city", "prep_ph": "tomorrow"}},
		{"text": "He is leaving the city on 1st July.", "entities": {"subject": "He", "verb": "leaving", "object": "city", "prep_ph": ["1st", "July"]}},
		{"text": "I usually go to bed at around 11 o'clock.", "entities": {"subject": "I", "verb": "go", "adverb": "usually", "object": "bed", "prep_ph": "11"}},
		{"text": "In 1920, NASA moved its headquarters to California.", "entities": {"subject": "NASA", "verb": "moved", "object": "headquarters", "prep_ph": "1920"}},
		{"text": "The film is releasing on 13th October.", "entities": {"subject": "film", "verb": "releasing", "object": "13th", "prep_ph": ["13th", "October"]}},
		{"text": "He was born on 28th July, 1933", "entities": {"subject": "He", "verb": "born", "object": "28th", "prep_ph": ["28th", "July", "1933"]}},
		{"text": "The train arrived at the station at 3 am.", "entities": {"subject": "train", "verb": "arrived", "object": "station", "prep_ph": ["3", "am"]}},
		{"text": "Laura has appeared on the television.", "entities": {"subject": "Laura", "verb": "appeared", "object": "television"}},
		{"text": "My old friends met the histrionic actor.", "entities": {"subject": "friends", "verb": "met", "adj": "old", "obj_adj": "histrionic", "object": "actor"}},
		{"text": "The cat was hiding from the dog under the bed.", "entities": {"subject": "cat", "verb": "hiding", "object": "dog", "prep_ph": "bed"}},
		{"text": "Under the presidential rule, Kejriwal was removed from the post.", "entities": {"subject": "Kejriwal", "verb": "removed", "object": "post", "prep_ph": "rule"}},
		{"text": "In the king's reign, the theives were scared of stealing.", "entities": {"subject": "theives", "verb": "scared", "object": "stealing", "prep_ph": "reign"}},
		{"text": "The successful Americans had been working on supercomputers since 1980.", "entities": {"subject": "Americans", "adj": "successful", "verb": "working", "object": "supercomputers", "prep_ph": "1980"}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},
		# {"text": "", "entities": {"subject": "", "verb": "", "adverb": "", "adj": "", "obj_adj": "", "object": ""}},


	]



	X_train_all = []
	y_train_tree_all = {}
	y_train_label_all = {}
	all_train_stanford_output = []

	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		all_train_stanford_output.append(stanford_output)
		feats = feat_generator.get_feats(stanford_output)


		for entity in text["entities"]:

			y_train_tree_all.setdefault(entity, [])
			y_train_label_all.setdefault(entity, [])

			if type(text["entities"][entity]) == list:
				for multi_entity in text["entities"][entity]:
					y_label_entity, y_train_tree_all[entity] = feat_generator.get_train_answer_label(stanford_output, multi_entity, y_train_tree_all[entity])
					y_train_label_all[entity].append((index, y_label_entity))
			else:
				y_label_entity, y_train_tree_all[entity] = feat_generator.get_train_answer_label(stanford_output, text["entities"][entity], y_train_tree_all[entity])
				y_train_label_all[entity].append((index, y_label_entity))


		feats = feats.flatten()

		X_train_all.append(feats)

	print "X_train_all", len(X_train_all), ",", len(X_train_all[0])

	# sel = VarianceThreshold(threshold=(.8 * (1 - .8)))
	# X_train_all = sel.fit_transform(X_train_all)
	# print "X_train_all", len(X_train_all), ",", len(X_train_all[0])


	print "y_train_label_all", y_train_label_all
	print "y_train_tree_all", y_train_tree_all

	numpy.save('y_train_tree_all.npy', y_train_tree_all)
	numpy.save('y_train_label_all.npy', y_train_label_all)


	for entity in y_train_label_all:

		print "\n\n     Traing :", entity

		X_train = []
		y_train_label = []
		entity_stanford_output = []

		for i in y_train_label_all[entity]:
			X_train.append(X_train_all[i[0]])
			y_train_label.append(i[1])
			entity_stanford_output.append(all_train_stanford_output[i[0]])
		
		numpy.save('stanford_output_train_' + entity + '.npy', entity_stanford_output)

		try:
			os.remove('model_' + entity + '.pkl')
		except OSError:
			pass

		if min(y_train_label) == max(y_train_label):
			print "Only 1 class label found\n\n**********************************"
			continue

		model = OneVsRestClassifier(LinearSVC(random_state=0)).fit(X_train, y_train_label)
		print "classes", len(model.classes_)

		joblib.dump(model, 'model_' + entity + '.pkl', compress=1)

		print "predict", model.predict(X_train)

		print "score", model.score(X_train, y_train_label)

		print "\n\n##############\n\n"



