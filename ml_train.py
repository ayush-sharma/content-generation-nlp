import feat_generator
import numpy
import json
from sklearn.feature_selection import VarianceThreshold
from nltk import Tree
from nltk.draw.tree import TreeView
import call_stanford_parser_server_api
import collections
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.externals import joblib
import sys



if __name__ == "__main__":

	sentence_type = sys.argv[1]
	input_text = []

	if sentence_type.lower() == "when":

		input_text = [
					# WHEN sentences
					{"text": "We sleep at night", "answer": "night"},
					{"text": "he leaves the office at 9 o'clock in the evening.", "answer": "evening"},
					{"text": "Jefferson was elected as president in 1908.", "answer": "1908"},
					{"text": "In 2000, Google was led by Larry Page.", "answer": "2000"},
					{"text": "He went to United States on Halloween.", "answer": "Halloween"},
					{"text": "They used to visit the place every Monday.", "answer": "Monday"},
					{"text": "We did not use flashlight until dark.", "answer": "dark"},
					{"text": "Rain stopped in the night.", "answer": "night"},
					{"text": "He goes to school everyday.", "answer": "everyday"},
					{"text": "Recession hit the nation in 2009.", "answer": "2009"},
					{"text": "It was raining yesterday.", "answer": "yesterday"},
					{"text": "Bryan Adams is going to perform on 8th of this month.", "answer": "8th"},
					{"text": "Convocation is scheduled on 26th July.", "answer": "26th"},
					{"text": "He flunked in the yesterday exam.", "answer": "yesterday"},
					{"text": "John was texting her in the night.", "answer": "night"},
					{"text": "He became 23 years old on this tuesday.", "answer": "tuesday"},
					{"text": "He is going to leave the city tomorrow.", "answer": "tomorrow"},
					{"text": "He is going to leave the city on 1st July.", "answer": "1st"},
					{"text": "I usually go to bed at around 11 o'clock.", "answer": "11"},
					{"text": "Ronaldo was the best football player in 2008.", "answer": "2008"},
					{"text": "In 1920, NASA moved its headquarters to California.", "answer": "1920"},
					{"text": "The film is going to release on 13th October.", "answer": "13th"},
					{"text": "He was born on 28th July, 1933", "answer": "28th"},
					{"text": "They left the house at 4 pm.", "answer": "4"},
					{"text": "The train arrived at the station at 3 am.", "answer": "3"},
					{"text": "I ate the food at around 8 o'clock.", "answer": "8"},
					{"text": "We had an icecream yesterday.", "answer": "yesterday"},
					{"text": "The river flooded at 4 o'clock in the morning.", "answer": "4"},
		]




	elif sentence_type.lower() == "who":		

		input_text = [
					# WHO sentences
					{"text":"James writes good poems.", "answer": "James"},
					{"text":"Rahul was elected as the Enigineering Manager", "answer": "Rahul"},

					{"text":"The girl recited the poems by reading the book.", "answer": "girl"},
					{"text":"He can speak Chinese.", "answer": "He"},
					{"text":"Ronaldo is the best football player in the world", "answer": "Ronaldo"},
					{"text":"Ram is my best friend.", "answer": "Ram"},
					{"text":"The strange guy over there is John.", "answer": "John"}, # passive
					{"text":"Gopal loves to watch movies in theatres.", "answer": "Gopal"},
					{"text":"The only person to dance in the office is Mary.", "answer": "Mary"}, # passive
					{"text":"Martha loves chinese food.", "answer": "Martha"},
					{"text":"In 2000, the company was led by Larry", "answer": "Larry"}, # passive
					{"text":"The elected chairman of the committee was Michael.", "answer": "Michael"}, # passive
					{"text":"Chinese food is loved by Martha.", "answer": "Martha"}, # passive
					{"text":"Stephen was born exactly 300 years after Galileo died.", "answer": "Stephen"},
					{"text":"Thomas knows her very well.", "answer": "Thomas"},
					{"text":"The actor smiled at us vivaciously.", "answer": "actor"},
					{"text":"Martha loves chinese food.", "answer": "Martha"},
					{"text":"Mexico sinks about 10 inches a year.", "answer": "Mexico"},
					{"text":"The glue on Israeli postage is certified kosher.", "answer": "glue"},
					{"text":"A human loses about 200 head hairs per day.", "answer": "human"},
					{"text":"Firefighter puts out the fire.", "answer": "Firefighter"},
					{"text":"Teacher teaches kids at school.", "answer": "Teacher"},
					{"text":"Postman delivers mail at the door.", "answer": "Postman"},
					{"text":"Veterinarian takes care of sick animals.", "answer": "Veterinarian"},
					{"text":"Pilot flies an airplane high up in the sky.", "answer": "Pilot"},
		]


	X_train = []
	y_train_tree = []
	y_train_label = []
	all_stanford_output = []

	for text in input_text:

		# text = input_text[0]
		print "text ->", text["text"]

		# print "in get_feats"
		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		all_stanford_output.append(stanford_output)
		# print stanford_output["sentences"][0]["basic-dependencies"]

		feats = feat_generator.get_feats(stanford_output)

		# print "y_train_tree", y_train_tree, stanford_output, text["answer"]

		y_label, y_train_tree = feat_generator.get_train_answer_label(stanford_output, text["answer"], y_train_tree)

		# print "y_label, y_train_tree", y_label, y_train_tree

		feats = feats.flatten()

		X_train.append(feats)
		y_train_label.append(y_label)

	print "X_train", len(X_train), ",", len(X_train[0])
	print "y_train_label", y_train_label
	print "y_train_tree", y_train_tree

	numpy.save('y_train_tree.npy', y_train_tree)
	numpy.save('stanford_output_train.npy', all_stanford_output)

	model = OneVsRestClassifier(LinearSVC(random_state=0)).fit(X_train, y_train_label)

	joblib.dump(model, 'model.pkl', compress=1)

	print "predict", model.predict(X_train)

	print "score", model.score(X_train, y_train_label)


