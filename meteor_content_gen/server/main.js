var root_folder = process.env.PWD

var pythonApi = "http://localhost:5000/"
var urlExtension = {
    "getParsedUnderstanding": "sample",
    "getSimilarContent": "similar",
    "upload_pdf": "parse_pdf"
}
var Future = Npm.require('fibers/future');

Meteor.methods({
    'getParsedUnderstanding': function(text, qs, ans) {
        HTTP.call('POST', pythonApi + urlExtension.getParsedUnderstanding, {
            params: {
                'text': text,
                'qs': qs,
                'ans': ans
            }
        }, function(error, response) {
            if (error) {
                console.log(error);
            } else {
                Samples.insert({ "qs": qs, "text": text, "ans": ans, "understanding": JSON.parse(response.content), "similar": [] })
                return response.content;

            }

        })
    },
    'getSimilarContent': function(baseId, sampleId) {
        var sample = JSON.stringify(Samples.findOne({ "_id": sampleId }).understanding);
        
        if (baseId.substr(0,6) == "Object"){
            baseId = baseId.substring(baseId.lastIndexOf("(")+2, baseId.lastIndexOf(")")-1);
            baseId = new Meteor.Collection.ObjectID(baseId);
        }

        var base = Base.findOne({ "_id": baseId }).text

        HTTP.call('POST', pythonApi + urlExtension.getSimilarContent, {
            params: {
                'text': base,
                'objectsUnderstanding': sample
            }
        }, function(error, response) {
            if (error) {
                console.log(error);
            } else {
                var sim = JSON.parse(response.content);

                var simArray = []
                for (var i = 0; i < sim["in_passage"].length; i++) {
                    simArray.push({
                        "text": sim["in_passage"][i]["sentence"],
                        "question": sim["in_passage"][i]["question"],
                        "answer": sim["in_passage"][i]["answer"],
                        "score": sim["in_passage"][i]["score"],
                        "category": "In - Passage"
                    })
                }

                for (var i = 0; i < sim["out_passage"].length; i++) {
                    simArray.push({
                        "text": sim["out_passage"][i]["sentence"],
                        "question": sim["out_passage"][i]["question"],
                        "answer": sim["out_passage"][i]["answer"],
                        "score": sim["out_passage"][i]["score"],
                        "category": "Out - Passage"
                    })
                }

                Samples.update(sampleId, {
                    $set: {
                        similar: {
                            "base": baseId,
                            "similarArray": simArray
                        }
                    }
                })
            }
        })
    },
    "call_pdf_parser": function(file_id, file_name) {
        console.log('in call_pdf_parser', file_id, file_name);
        var exec = Npm.require('child_process').exec;
        var runCmd = Meteor.wrapAsync(exec);
        var future = new Future();

        var cmdStr = "python ../../../../../server/pdf_parser.py " + String(file_id) + " " + String(file_name);

        var result = runCmd(cmdStr, function(err, stdout, stderr) {
            if (err) {
                console.log('err', err);
                future.return("err!!!");
            } else if (stdout) {
                future.return(stdout);
            } else {
                console.log('stderr', stderr);
                future.return("err!!!");
            }
        });

        return future.wait();
    },
    "upload_pdf": function(file, file_id){
        HTTP.call('POST', pythonApi + urlExtension.upload_pdf, {
            params: {
                'file_id': file_id,
            }
        }, function(error, response) {
            if (error) {
                console.log(error);
            } else {
                console.log(response.content);
                Base.insert({ "name": file["name"], "text": response.content, "similar": [] });
            }
        })
    }
})
// Meteor.startup(function() {
//     Samples.remove({"text":"mark is a good chef"})
//     Base.remove({"_id":"JDYC2obmyNosDZdsD"})
// });
