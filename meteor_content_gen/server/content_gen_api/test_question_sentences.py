# -*- coding: utf-8 -*-
import feat_generator
import numpy
import call_stanford_parser_server_api
from sklearn.externals import joblib


if __name__ == "__main__":

	input_text = [
		{"text": "Who went to the mall.", "entities": {}},
	]



	X_test_all = []
	all_test_stanford_output = []

	XY_train = numpy.load('XY_question_train.npy').tolist()

	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		all_test_stanford_output.append(stanford_output)

		feats = feat_generator.get_feats(stanford_output)

		feats = feats.flatten()

		X_test_all.append(feats)

	pred_entities = [{} for i in range(0, len(input_text))]

	for entity, XY in XY_train.iteritems():
		if entity not in ["adj", "obj_adj", "prep_ph"]:

			print "\n     Testing :", entity

			for index, X_test in enumerate(X_test_all):
				matches = feat_generator.get_similar_sentences(X_test, XY)
				# print "matches", matches[:3]

				pred_answer_tree = XY["y"][matches[0][2]]
				stanford_output_test = all_test_stanford_output[index]
				stanford_output_pred = XY["stanford_output"][matches[0][2]]

				pred_word = feat_generator.get_main_entities(pred_answer_tree, stanford_output_test, input_text[index], entity, stanford_output_pred)
				pred_entities[index][entity] = pred_word

				# print "pred_word", pred_word, "\n#######\n"
				
			# print "\n##################################\n"


	for index, text in enumerate(input_text):
		test_stanford_output = all_test_stanford_output[index]
		pred_entities[index] = feat_generator.predict_next_entities(test_stanford_output, pred_entities[index], text)

	print "\npred_entities\n", pred_entities


