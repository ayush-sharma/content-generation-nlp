# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify
from pprint import pprint
import json
import os
from werkzeug.utils import secure_filename
import pdf_parse
import similar_text_gen
import numpy
import time
import generate_sentences
import nltk.data
import pdf_parser_meteor
import re

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

UPLOAD_FOLDER = os.path.dirname(os.path.realpath(__file__)) + '/uploads'
ALLOWED_EXTENSIONS = set(['pdf'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/recieveEmail', methods=['GET', 'POST'])
def upload_file():
    print "in recieveEmail"
    if request.method == 'POST':
        # import pdb;pdb.set_trace()
        if not request.files.keys():
            return 'No file part'
        file = request.files['attachment1']
        if file.filename == '':
            return 'No selected file'
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            print "saving to", os.path.join(app.config['UPLOAD_FOLDER'], filename)
            raw_text = pdf_parse.convert_pdf_to_txt(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            
            input_text = tokenizer.tokenize(raw_text)

            input_text = ' '.join(str(x) for x in input_text[:10])
            input_text = ''.join([i if ord(i) < 128 else ' ' for i in input_text])

            # input_text = "Bud is a bird. He wakes up early to catch a worm. Bud is very hungry. Will is a worm. He moves around in the dirt. Will sees Bud flying in the sky. Will moves near a tree. Bud flies around the tree. Will moves to the garden. He still sees Bud flying in the sky. I must find some place safe to hide. Will looks around and disappears as Bud swoops down. Bud eats some bird seeds that he finds on the ground. Bud did not see Will. Some fireflies glow to warn other animals that they don't taste good. Frogs, bats, and birds do not like to eat animals that glow. The glow helps keep fireflies safe. Ben and his dad went fishing. Ben took the pole. Ben liked the boat ride. Ben put a worm on the hook. He put his line in the water. He felt the pole pull. Was it a fish? He reeled it in. He did not get a fish. Ben got an old can."

            object_understanding = numpy.load('OU.npy').tolist()
            questions_generated = similar_text_gen.generate_questions(input_text, object_understanding)

            return jsonify(questions_generated)

        return "error on backend"


@app.route('/parse_pdf', methods=['GET', 'POST'])
def parse_pdf():
    print "in parse_pdf"
    
    file_id = request.form.get("file_id")
    print "file_id", file_id
    raw_text = pdf_parser_meteor.convert_pdf_to_txt(file_id)

    return raw_text


@app.route('/similar', methods=['GET', 'POST'])
def similar_api():
    print "in similar api"

    input_text = request.form.get("text")
    # input_text = input_text.encode('ascii', 'ignore')
    # input_text = re.sub(r'\n', ' ', input_text)
    input_text = ''.join([i if ord(i) < 128 else ' ' for i in input_text])

    object_understanding = json.loads(request.form.get("objectsUnderstanding"))

    print "similar input", input_text, object_understanding

    questions_generated = similar_text_gen.generate_questions(input_text, object_understanding)
    return jsonify(questions_generated)



@app.route('/sample', methods=['GET', 'POST'])
def sample_api():
    print "sample entered"

    sample_input = {"text": request.form.get("text"), "question": request.form.get("qs"), "answer": request.form.get("ans")}
    print "sample_input", sample_input

    object_understanding = generate_sentences.create_sample_entities(sample_input)
    numpy.save('OU.npy', object_understanding)

    return jsonify(object_understanding)



if __name__ == '__main__':
    app.run(debug=True)