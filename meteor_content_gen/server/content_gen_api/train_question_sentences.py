# -*- coding: utf-8 -*-
import feat_generator
import numpy
import json
from sklearn.feature_selection import VarianceThreshold
from nltk import Tree
from nltk.draw.tree import TreeView
import call_stanford_parser_server_api
import collections
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.externals import joblib
import sys



if __name__ == "__main__":

	input_text = [
		{"text": "Who goes to the school.", "entities": {"subject": "Who", "verb": "goes", "object": "school"}},
		{"text": "Who found the rock.", "entities": {"subject": "Who", "verb": "found", "object": "rock"}},
		{"text": "What did the clown say?", "entities": {"subject": "What", "verb": "found", "object": "rock"}},
		{"text": "Who found the rock.", "entities": {"subject": "Who", "verb": "found", "object": "rock"}},
		
	]



	X_train_all = []
	y_train_tree_all = {}
	
	XY_train = {}

	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		feats = feat_generator.get_feats(stanford_output)

		for entity in text["entities"]:
			XY_train.setdefault(entity, {})
			XY_train[entity].setdefault("feats", [])
			XY_train[entity].setdefault("y", [])
			XY_train[entity].setdefault("input_text", [])
			XY_train[entity].setdefault("stanford_output", [])

			y_train_tree_all.setdefault(entity, [])

			if type(text["entities"][entity]) == list:
				for multi_entity in text["entities"][entity]:
					y_label_entity, y_train_tree_all[entity] = feat_generator.get_train_answer_label(stanford_output, multi_entity, y_train_tree_all[entity])
					
					XY_train[entity]["feats"].append(feats.flatten())
					XY_train[entity]["y"].append(y_train_tree_all[entity][y_label_entity])
					XY_train[entity]["input_text"].append(text)
					XY_train[entity]["stanford_output"].append(stanford_output)
			else:
				y_label_entity, y_train_tree_all[entity] = feat_generator.get_train_answer_label(stanford_output, text["entities"][entity], y_train_tree_all[entity])

				XY_train[entity]["feats"].append(feats.flatten())
				XY_train[entity]["y"].append(y_train_tree_all[entity][y_label_entity])
				XY_train[entity]["input_text"].append(text)
				XY_train[entity]["stanford_output"].append(stanford_output)


	numpy.save('XY_question_train.npy', XY_train)




