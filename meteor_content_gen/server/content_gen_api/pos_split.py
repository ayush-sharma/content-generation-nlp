import re
from nltk import Tree
import json
import call_stanford_parser_server_api

def tree2list(tree):
    pos_list = []

    for ind, t in enumerate(tree):
        # print "before cond", ind
        if type(t) == Tree:
            # print "if", t.label() , "->"
            pos_list.append([t.label(), tree2list(t)])
            # print "updated pos_list", pos_list
        else:
            # print "else", tree.label() , "->", t
            pos_list = t
            # print "updated pos_list", pos_list

    # print "return pos_list", pos_list
    return pos_list



def get_NP_VP_VPindex(pos_list):
    for index, pos in enumerate(pos_list):
        if pos[0] == "VP":
            return pos_list[index - 1], pos, index




def check_child_pos_depr(pos_list, match_pos):
    for pos in pos_list[1]:
        if pos[0] == match_pos:
            return pos

def check_child_pos(pos_list, match_pos):
    for pos in pos_list[1]:
        if re.match(match_pos, pos[0]):
            return pos


def get_leaves(pos_list):
    leaves = []
    if type(pos_list) == list:
        if (type(pos_list[0]) == str or type(pos_list[0]) == unicode) and type(pos_list[1]) == list:
            leaves += get_leaves(pos_list[1])
        elif type(pos_list[0]) == list:
            for pos in pos_list:
                leaves += get_leaves(pos)
        elif (type(pos_list[0]) == str or type(pos_list[0]) == unicode) and (type(pos_list[1]) == str or type(pos_list[1]) == unicode):
            leaves.append(pos_list[1])

    return leaves


def extract_from_VP(VP, start_VP=None):
    till_VP = None
    if not start_VP:
        start_VP = get_leaves(VP[1])[0]
    # print "here 1"
    if len(VP[1]) > 2:
        # print "len(VP[1]) > 2"
        till_VP = get_leaves(VP[1][-2])[0]
        return start_VP, till_VP
    
    # print "here 2"
    VP2 = check_child_pos(VP, "VP")
    if VP2:
        # print "here 3 got VP2"
        VP = VP2
        # print "VP", VP
        # till_VP = get_leaves(VP)[0]
        # return start_VP, till_VP
        return extract_from_VP(VP, start_VP)
    else:
        # print "here 4 no VP2"
        AD = check_child_pos(VP, "^AD.*")
        if AD:
            till_VP = get_leaves(AD)[0]

    if not till_VP:
        # print "here 5 till_VP None"
        till_VP = get_leaves(VP)[0]

    return start_VP, till_VP


def complete_str(all_words, start, end):
    return all_words[all_words.index(start):all_words.index(end)+1]


def get_word_classes(pos):
    tree = Tree.fromstring(pos)
    pos_list = tree2list(tree)

    ret = find_S(pos_list)
    pos_list = ret

    all_words = get_leaves(pos_list)

    NP, VP, VP_index = get_NP_VP_VPindex(pos_list[0][1])

    subject_list = get_leaves(NP)

    verb_start, verb_end = extract_from_VP(VP)

    verb_end_index = all_words.index(verb_end)

    # object_list = get_leaves(VP)[verb_end_index+1:]
    object_list = all_words[verb_end_index+1:]

    verb_list = complete_str(all_words, verb_start, verb_end)

    # print "####get_word_classes", subject_list, verb_list, object_list

    return subject_list, verb_list, object_list


def find_S(pos_list):
    all_S = []
    # print "in find_S"
    if type(pos_list) == list:
        # print "in if 1"
        if (type(pos_list[0]) == str or type(pos_list[0]) == unicode) and type(pos_list[1]) == list:
            # print "in if 2"
            if pos_list[0] == "S" or pos_list[0] == "SINV":
                # print "in if 3"
                all_S.append(pos_list)
            all_S += find_S(pos_list[1])
        elif type(pos_list[0]) == list:
            # print "in elif 4 ->", len(pos_list)
            for pos in pos_list:
                all_S += find_S(pos)
        elif (type(pos_list[0]) == str or type(pos_list[0]) == unicode) and (type(pos_list[1]) == str or type(pos_list[1]) == unicode):
            # print "in elif 5"
            if pos_list[0] == "S" or pos_list[0] == "SINV":
                # print "in if 6"
                all_S.append(pos_list)

    return all_S



if __name__ == "__main__":

    # output = call_stanford_parser_server_api.get_output("The city's chief manufactures are processed food, lumber, and textiles.")
    # output = call_stanford_parser_server_api.get_output("Frank Lloyd Wright's son invented Lincoln Logs.")
    # output = call_stanford_parser_server_api.get_output("Darwin studied how species evolve.")
    # output = call_stanford_parser_server_api.get_output("Jefferson, the third president of US, died in 1999.")
    # output = call_stanford_parser_server_api.get_output("John studied on Monday but went to the park on Tuesday.")
    # output = call_stanford_parser_server_api.get_output("Ram, Shyam and Gita are my best friends.")
    # output = call_stanford_parser_server_api.get_output("The only person to dance in the office is Mary.")
    # output = call_stanford_parser_server_api.get_output("The strange guy over there is John.")
    # output = call_stanford_parser_server_api.get_output("Chinese food is loved by Martha.")
    # output = call_stanford_parser_server_api.get_output("Jefferson was elected as president in 1908.")
    # output = call_stanford_parser_server_api.get_output("We eat breakfast in the morning.")
    # output = call_stanford_parser_server_api.get_output("Dentist checked our teeth for cavities.")
    # output = call_stanford_parser_server_api.get_output("One in every 4 Americans has appeared on television.")
    
    # output = call_stanford_parser_server_api.get_output("My old father shoots a red car accurately.")
    # output = call_stanford_parser_server_api.get_output("My old father and I shoots a red car and a doll accurately.")
    # output = call_stanford_parser_server_api.get_output("Cal was running under the table and chases Mel away.")
    # output = call_stanford_parser_server_api.get_output("The firm stopped using crocodilite in its cigarette filters.")
    output = call_stanford_parser_server_api.get_output("The firm stopped using crocodilite in its cigarette filters to prevent lung diseases.")

    pos = output["sentences"][0]["parse"]
    print "pos", pos




    # pos = u"(ROOT\n  (S\n    (PP (IN During)\n      (NP\n        (NP (DT the) (NN Gold) (NN Rush) (NNS years))\n        (PP (IN in)\n          (NP (JJ northern) (NNP California)))))\n    (, ,)\n    (NP (NNP Los) (NNP Angeles))\n    (VP (VBD became)\n      (VP (VBN known)\n        (PP (IN as)\n          (NP\n            (NP (DT the) (`` `) (NN Queen))\n            (PP (IN of)\n              (NP\n                (NP (DT the) (NN Cow) (NNS Counties) (POS '))\n                (PP (IN for)\n                  (NP (PRP$ its) (NN role)))))))\n        (PP (IN in)\n          (S\n            (VP (VBG supplying)\n              (NP (NN beef)\n                (CC and)\n                (JJ other) (NNS foodstuffs))\n              (PP (TO to)\n                (NP\n                  (NP (JJ hungry) (NNS miners))\n                  (PP (IN in)\n                    (NP (DT the) (NN north))))))))))\n    (. .)))"
    # pos = u'(ROOT\n  (S\n    (NP (PRP$ Our) (NN skull))\n    (VP (VBZ is)\n      (VP (VBN made)\n        (PRT (RP up))\n        (PP (IN of)\n          (NP (CD 29) (JJ different) (NNS bones)))))\n    (. .)))'
    # pos = u'(ROOT\n  (S\n    (NP (PRP we))\n    (VP (VBP are)\n      (VP (VBG going)\n        (PP (TO to)\n          (NP (DT the) (NN party)))))))'
    # pos = u'(ROOT\n  (S\n    (NP\n      (NP (NNP Bell))\n      (, ,)\n      (VP (VBN based)\n        (PP (IN in)\n          (NP (NNP Los) (NNP Angeles))))\n      (, ,))\n    (VP (VBZ makes)\n      (CC and)\n      (VBZ distributes)\n      (NP\n        (UCP (JJ electronic) (, ,) (NN computer)\n          (CC and)\n          (NN building))\n        (NNS products)))\n    (. .)))'
    # pos = u'(ROOT\n  (S\n    (NP (PRP we))\n    (VP (VBD decided)\n      (S\n        (VP (TO to)\n          (VP (VB attend)\n            (NP (DT the) (NN party))))))))'
    # pos = u'(ROOT\n  (PP (IN in)\n    (NP\n      (NP (NN france))\n      (SBAR\n        (S\n          (NP (DT a)\n            (ADJP\n              (NP (CD five) (NN year))\n              (JJ old))\n            (NN child))\n          (VP (MD can)\n            (VP (VB buy)\n              (NP (DT an) (JJ alcoholic) (NN drink))\n              (PP (IN in)\n                (NP (DT a) (NN bar))))))))))'
    # pos = u'(ROOT\n  (SINV\n    (PP (IN in)\n      (NP\n        (NP (CD 10) (NNS minutes))\n        (NP (DT a) (NN hurricane))))\n    (VP (VBZ releases)\n      (ADVP (RBR more))\n      (NP (NN energy))\n      (PP (IN than)\n        (NP\n          (NP (DT all))\n          (PP (IN of)\n            (NP (DT the) (NNS worlds))))))\n    (NP\n      (NP (JJ nuclear) (NNS weapons))\n      (VP (VBN combined)))))'
    # pos = u'(ROOT\n  (S\n    (NP (DT The) (JJ little) (JJ yellow) (NN dog))\n    (VP (VBD barked)\n      (PP (IN at)\n        (NP (DT the) (NN cat))))\n    (. .)))'

    tree = Tree.fromstring(pos)

    tree.draw()

    exit(0)

    pos_list = tree2list(tree)
    print "pos_list", pos_list, "\n\n"
    # exit(0)

    ret = find_S(pos_list)
    pos_list = [ret[0]]
    print "ret", ret
    print len(ret)
    # exit(0)

    all_words = get_leaves(pos_list)

    print "all_words", all_words

    NP, VP, VP_index = get_NP_VP_VPindex(pos_list[0][1])

    print "here VP", VP

    subject_str = ' '.join(get_leaves(NP))

    verb_start, verb_end = extract_from_VP(VP)

    # print "verb_start, verb_end", verb_start, verb_end

    verb_end_index = all_words.index(verb_end)

    # print "verb_end_index", verb_end_index

    # print "get_leaves(VP)", get_leaves(VP)

    # object_str = ' '.join(get_leaves(VP)[verb_end_index+1:])
    object_str = ' '.join(all_words[verb_end_index+1:])

    verb_str = ' '.join(complete_str(all_words, verb_start, verb_end))

    print subject_str, "->", verb_str, "->", object_str

    # tree.draw()







