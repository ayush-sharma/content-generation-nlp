# from nltk.corpus import wordnet as wn
import nltk, os, sys, pprint, json
# from pymongo import MongoClient
from extractingFunctions import extractDeps
# from mainParser import getParse
# from qsansst import *
from subprocess import *
# from pos import getPos
# from grammarExtractors import *


root_folder = str(os.path.dirname(os.path.realpath(__file__)))

# line = "Tiny John ran quickly towards the door"
# line="Arteries take the blood away from the heart"
# line="The skeletal system basically controls the overall shape of the human body "
# line="When one part of the body fails, it sometimes triggers other organs to fail"
# line = "New nail cells take place as the nail moves forward and grows in length"
# line="It also contains enzymes that help break down food mixtures so it can easily be absorbed"



def objectToStatement(obj):
    text =""
    SUBJ = obj['SUBJ']
    OBJ = obj['OBJ']
    VERB = obj['VERB']
    order = ['noun','adj','det','prep']
    orderV = ['verb','adv']

    # parts = ['SUBJ', 'OBJ', 'VERB']
    # for k in SUBJ.keys():

    for o in order:
        if len(SUBJ[o])>0:
            text+=SUBJ[o]+","+"subj"+o+","

    for o in order:
        if len(OBJ[o])>0:
            text+=OBJ[o]+","+"obj"+o+","

    for o in orderV:
        if len(VERB[o])>0:
            text+=VERB[o]+","+o+","


    # print text[:-1]
    arg = text[:-1]
    return arg


def getObjects(line, linePos, Mdeps):
    deps = Mdeps
    treedeps=Mdeps
    root = ""
    tempRootPos=""
    tempRootPosNN=""
    tree={}
    rbexists=0
    for i,d in enumerate(treedeps):
        if d[2].lower()=='root':
            root=d[0]
            tree['root']=root
            # print "root", root
            if len([x[0] for x in linePos if "NN"==x[1][0:2] and x[0]==root])>0:
                tempRootPos = [x[0] for x in linePos if "NN"==x[1][0:2] and x[0]==root][0]
                tempRootPosNN="NN"

        tree[root]=[]
    def addChildren(node, tree, treedeps, tempRootPos):
        temp=[]
        tempIndexes=[]
        if len(treedeps)>0:
            for i,d in enumerate(treedeps):
                if d[1] == node and d[0] not in temp:
                    temp.append(d[0])
                    if len(tempRootPos)<1 and len([x[0] for x in linePos if "NN"==x[1][0:2] and x[0]==root])>0:
                        tempRootPos = [x[0] for x in linePos if "NN"==x[1][0:2] and x[0]==root][0]
                        tempRootPosNN="NN"

                    tempIndexes.append(d)
        tree[node] = temp
        for t in temp:
            addChildren(t, tree, treedeps, tempRootPos)
    addChildren(root, tree, treedeps, tempRootPos)
    deps=Mdeps
    subjects=['nsubj', 'dep', 'advcl']
    objects = ['dobj']


    SUBJ = {}
    OBJ = {}
    VERB ={}


    subj=''
    # root=''
    obj=''
    verb = ''
    subjIN = ''
    subjJJ = ''
    objJJ = ''
    objIN = ''
    subjDT = ''
    objDT = ''
    adv=''

    if len(tempRootPos)>0:
        root=tempRootPos

    for d in deps:
        if tempRootPosNN=="NN":
            subj = root
        elif d[2] in subjects and d[1]==root and subj=="":
            subj = d[0]

        if d[2][0:4]=='nmod' or d[2][0:4]=='dobj' and obj=="":
            if d[1]==root:
                # print "adding shape", d
                obj = d[0]

    if obj=="":
        for d in deps:
            if d[2][0:4]=='nmod' or d[2][0:4]=='dobj':
                if obj=="":
                    obj = d[0]

    for d in deps:
        tempV = [x[0] for x in linePos if "VB"==x[1][0:2]]
        # print tempV
        for tV in tempV:
            # print subj in d, tempV in d, d, subj, tV
            if subj in d and tV in d:
                verb = tV
                break

        if len([x[0] for x in linePos if x[1][0:2] in ["IN", "TO"] ])>0:
            for tempIN in [x[0] for x in linePos if x[1][0:2] in ["IN", "TO"]]:
                if subj in d and tempIN in d:
                    subjIN = tempIN
                elif obj in d and tempIN in d:
                    objIN = tempIN

        if len([x[0] for x in linePos if x[1][0:2] in ["DT", "PR"]])>0:
            # print [x[0] for x in linePos if x[1][0:2] in ["DT", "PR"]]
            for tempDT in [x[0] for x in linePos if x[1][0:2] in ["DT", "PR"]]:
                # tempDT = [x[0] for x in linePos if x[1][0:2] in ["DT", "PR"]][0]
                # print tempDT
                if subj in d and tempDT in d and tempDT!=subj:
                    subjDT = tempDT
                elif obj in d and tempDT in d:
                    objDT = tempDT

        if len([x[0] for x in linePos if "JJ"==x[1][0:2]])>0:
            for tempJJ in [x[0] for x in linePos if "JJ"==x[1][0:2]]:
                if subj in d and tempJJ in d:
                    subjJJ = tempJJ
                elif obj in d and tempJJ in d:
                    objJJ = tempJJ
        
        if len([x[0] for x in linePos if "RB"==x[1][0:2]])>0:
            rbexists=1
            tempDT = [x[0] for x in linePos if "RB"==x[1][0:2]][0]
            if verb in d and tempDT in d:
                adv = tempDT
            
    # print "subjDT, subj, subjIN, verb, adv,objIN,objDT, obj"
    # print subjDT, "-",subj,"-", subjIN,"-", verb,"-", adv,"-",objIN,"-",objDT,"-", obj

    if rbexists==1 and len(adv)<1:
        adv = [x[0] for x in linePos if "RB"==x[1][0:2]][0]
        
    SUBJ['noun'] = subj
    SUBJ['adj'] = subjJJ
    SUBJ['det'] = subjDT
    SUBJ['prep'] = subjIN

    OBJ['noun'] = obj
    OBJ['adj'] = objJJ
    OBJ['det'] = objDT
    OBJ['prep'] = objIN

    VERB['verb'] = verb
    VERB['adv'] = adv

    # print "SUBJ, OBJ, VERB", SUBJ, OBJ, VERB
    subS = subj
    subAdj = subjJJ
    subDet = subjDT
    subPrep = subjIN
    objAdj = objJJ
    objDet = objDT
    objPrep = objIN
    objS = obj


    # arg=objectToStatement({'SUBJ':SUBJ, 'OBJ':OBJ, 'VERB': VERB})
    # return {"SUBJ":SUBJ, "OBJ":OBJ, "VERB":VERB, "nlgText":arg, "pos":linePos, "deps": deps}
    return {"subS":subS, "subAdj":subAdj, "subDet":subDet, "subPrep":subPrep, "objS":objS, "objDet":objDet, "objPrep":objPrep, "objAdj":objAdj, "verb":verb, "adv":adv}



# line = "My old father shoots a red car"
# line = "Smart Katy ran quickly towards the door"
# Mdeps, linePos, tokenObjs = extractDeps(line)
# print getObjects(line, linePos, Mdeps)

# lines = [
# "My old father shoots a red car", 
# "Smart Katy ran quickly towards the door",
# "Father sat on the tree",
# ]

# for line in lines:
#     deps, pos, tokenObjs = extractDeps(line.lower())
#     print pos
#     print deps
#     print getObjects(line, pos, deps)
# # obj= getObjects(line)
# # arg=obj['nlgText']+",WHERE,inter"
# # print arg


# def jarWrapper(*args):
#     process = Popen(['java', '-jar']+list(args), stdout=PIPE, stderr=PIPE)
#     ret = []
#     while process.poll() is None:
#         line = process.stdout.readline()
#         if line != '' and line.endswith('\n'):
#             ret.append(line[:-1])
#     stdout, stderr = process.communicate()
#     ret += stdout.split('\n')
#     if stderr != '':
#         ret += stderr.split('\n')
#     ret.remove('')
#     return ret


# args = ['/Users/raunakjain/Desktop/test.jar', arg] # Any number of args to be passed to the jar file
# result = jarWrapper(*args)
# print "result", result










# grammar = """
#   NP: {<DT>*<NN>*<NNP>*<NNS>*}
#   PP: {<IN>+<DT>*<JJ>*<NP>?<PRP.>?}
#   PP: {<DT>*<JJ>*<NP>?<PRP.>?<IN>+}
#   VP: {<RB>*<VB.>*<VB>*<RB>*}
#   """
# cp = nltk.RegexpParser(grammar)

# print linePos
# result = cp.parse(linePos) 
# # print result, NP(result)
# result.draw()

