import numpy, os, pprint
import nltk
import json
from get_compatible_JSON_by_scraping import get_json
from pycorenlp import StanfordCoreNLP
# from stemming.porter2 import stem
# from stanford_coreNLP_offline import parse
# a="John ran quickly towards the door."
# a="A nutritionist wants to compare nutritional value offered by a glass of apple juice with the value offered by a glass of spinach soup. What limitations of food tests make them a poor choice for use in this situation?"

root_folder = str(os.path.dirname(os.path.realpath(__file__)))

# STOP_WORDS_FILE = root_folder + "/english.stop"
# stopwords_io_stream = open(STOP_WORDS_FILE, 'r')
# stoplist = stopwords_io_stream.read().split()

def extractDeps(a):
	b= get_json(a, get_scrape_json=True)

	sentences = b['root']['document']['sentences']
	tokenObjs = sentences['sentence']['tokens']['token']
	# pprint.pprint(sentences['sentence']['tokens']['token'])
# 	# print type(sentences['sentence'])
	dependencies = []
	pos=[]
# 	# print len(sentences['sentence'])
	if type(sentences['sentence']) is dict:
# 		# print "inside if",sentences['sentence'].keys()
		depend = sentences['sentence']['dependencies']
		for depend2 in depend:
# 		# print depend
			for actualDepend in depend2['dep']:
				temp = actualDepend['dependent']['#text'],actualDepend['governor']['#text'],actualDepend['@type']
# 				# print actualDepend['dependent']['@idx'], temp
				dependencies.append(temp)
	else:
		for sentence in sentences['sentence']:
# 			# print sentence['dependencies'][0]
# 			# print len(sentence['dependencies'])
			for depend in sentence['dependencies']:
				for actualDepend in depend['dep']:
					temp = actualDepend['dependent']['#text'],actualDepend['governor']['#text'],actualDepend['@type']
# 					# print actualDepend['dependent']['@idx'], temp
					# if temp[0] not in stoplist and temp[1] not in stoplist:
					dependencies.append(temp)
# 	# print len(dependencies)
# 	# print dependencies
	temp=[]
	for i,d in enumerate(dependencies):
		if d[2] == 'root':
			# d[2]=dependencies[i][2].upper()
			temp.append([d[0],d[1],'ROOT'])
		# else:
		elif [d[0],d[1],d[2]] not in temp:
			temp.append([d[0],d[1],d[2]])
	# pprint.pprint(temp)

	for p in tokenObjs:
		pos.append([p['word'], p['POS']])
	return temp, pos, tokenObjs


	# nlp = StanfordCoreNLP('http://localhost:9000')
	# output = nlp.annotate(a, properties={
	#   'annotators': 'tokenize,ssplit,pos,depparse,parse',
	#   'outputFormat': 'json'
	#   })
	# deps=[]
	# pos=[]
	# if type(output) is dict:
	# 	for sent in output['sentences']:
	# 		# pprint.pprint(sent['tokens'])
	# 		de=sent['collapsed-dependencies']
	# 		for d in de:
	# 			# print d
	# 			if 'dependentGloss' in d.keys():
	# 				# print stem(d['dependentGloss'].lower()),stem(d['governorGloss'].lower())
	# 				deps.append((d['dependentGloss'],d['governorGloss'],d['dep']))
	# 		# pprint.pprint(deps)
	# 		for p in sent['tokens']:
	# 			pos.append((p['word'],p['pos']))
	# 		# print pos
	# 	# print deps, pos
	# 	return deps, pos

















	# b = parse(a)['sentences']
	# posT = b[0]['pos']
	# depsT = b[0]['deps_cc']
	# tokens = b[0]['tokens']
	# pos=[]
	# deps=[]
	# for i,p in enumerate(posT):
	# 	pos.append([tokens[i], posT[i]])
	# 	if depsT[i][0] == 'root':
	# 		deps.append([tokens[depsT[i][2]],'ROOT','ROOT'])
	# 	else:
	# 		deps.append([tokens[depsT[i][2]],tokens[depsT[i][1]],depsT[i][0]])

	# # print '\n\n\n\n\n\n\n\n\n\n\n\n',  b[0]['tokens'], b[0]['pos'],b[0]['deps_cc'], b[0].keys()
	# print '\n\n\n\n\n\n\n\n\n\n\n\n',  pos, deps
	# return deps, pos

# a="John quickly ran towards the door"
# print extractDeps(a)
