__author__ = 'siddarth'

import os
import pprint
import nltk
from nltk import tree
from stanford_corenlp_pywrapper import CoreNLP


def parse(inputText):

	coreNlpPath = str(os.path.dirname(os.path.realpath(__file__))) + "/stanford-corenlp-full-2015-12-09/*"

	proc = CoreNLP(configdict={'annotators':'tokenize, ssplit, pos, parse'}, output_types=['pos','parse'], corenlp_jars=[coreNlpPath])

	data = proc.parse_doc(inputText)


	nouns = ["NN", "NNP", "NNS", "NNPS", "NP"]
	fullstop = [".", "?", ",", "!", "-"]
	verbs = ["VB", "VBZ", "VBP", "VBG", "VBN", "VBD"]
	det = ["DT"]
	lasttry = []
	pronouns = ["PRP", "PRP$"]
	outermost = {}
	outermost["sentences"] = []
	outerDiction = {}
	sentctr = 0
	for da in data["sentences"]:
	    sentctr = sentctr + 1
	    # print("\n**********************************************************")


	    # print(corefer["coreference"]["coreference"])
	    newDict = {}
	    POS = {"ROOT": ""}
	    listofwords = da["tokens"]

	    for matar in range(0,len(da["tokens"])):
	        POS[da["tokens"][matar]] = da["pos"][matar]
	    # pprint.pprint(POS)
	    # print(sent.replace("        ","").replace("      ",""))
	    tempsent = " ".join(da["parse"].split())
	    tempsent = tempsent.replace(" (", "(")
	    # print(tempsent)
	    sent = tempsent

	    t = nltk.tree.Tree.fromstring(sent)
	    completeSe = " ".join(t.leaves())
	    # print(completeSe)
	    # pprint.pprint(t)
	    # t.pretty_print()

	    np = [" ".join(i.leaves()) for i in t.subtrees() if
	          i.label() == 'NP']  # in nouns or i.label() in pronouns ] # == 'NP' or i.label() == 'NN' ]
	    np.sort(key=len)

	    # print "np", np
	    # exit(0)
	    # for nnn in np:
	    #     print(np.index(nnn),nnn)

	    # ----------------------------------------------------------------------------------------------------------
	    answerDiction = {}
	    unredundantNNs1 = []
	    unredundantNNs = []
	    flagmarker = []
	    for ind in range(0, len(np)):
	        npeas = np[ind]
	        flag = 0
	        for ind2 in range(0, len(np)):
	            if (ind != ind2):
	                # print(npeas in np[ind2],"\t",npeas)
	                if (npeas in np[ind2]):
	                    # print("------",npeas,"===", np[ind2])
	                    flagmarker.append(ind2)
	                    flag = 1

	                    break
	                    # if(flag==0):
	                    #     unredundantNNs1.append(npeas)
	    # print(set(flagmarker))
	    # print()



	    for x in range(0, len(np)):
	        if (x not in flagmarker):
	            unredundantNNs.append(np[x])
	            # print(np[x])

	    # print("~~~~~~~~~~~~~~~~~~~~~```")
	    # for nnn in unredundantNNs:
	        # print(nnn)

	    # ----------------------------------------------------------------------------------------------------------





	    nounsHere = []
	    for t in POS:
	        if (POS[t] in nouns):
	            # print("*****",t["word"])
	            nounsHere.append(t)
	    # print("Nouns: ")
	    # print(nounsHere)
	    # print()


	    inkePOS = {}
	    # for t in da["tokens"]:
	    #     inkePOS[t["word"]] = t["pos"]
	    inkePOS=POS
	    for d in da["deps_basic"]:
	        # print(da["tokens"][d[1]],da["tokens"][d[2]])
	        if (inkePOS[da["tokens"][d[2]]] in nouns):
	            if (da["tokens"][d[1]] not in newDict):
	                newDict[da["tokens"][d[1]]] = [da["tokens"][d[2]]]
	            else:
	                newDict[da["tokens"][d[1]]].append(da["tokens"][d[2]])
	    # pprint.pprint(newDict)


	    nayiwaaliDict = {}
	    for nou in nounsHere:
	        if (nou in newDict):
	            # print("\n",nou, " : ",newDict[nou])
	            nayiwaaliDict[nou] = newDict[nou]
	        else:
	            doo = 0
	            # print("\n",nou)

	    # print()

	    plusminusDiction = {}
	    for nnn in unredundantNNs:
	        # print("++", nnn)
	        plusminusDiction[nnn] = []
	        splitwordsoflambesentence = nnn.split(" ")
	        for eachw in splitwordsoflambesentence:
	            if (eachw in nayiwaaliDict):
	                itsVal = nayiwaaliDict[eachw]
	                for v in itsVal:
	                    for init in unredundantNNs:
	                        # print("     ", v, nnn)
	                        if (init != nnn):
	                            if (v in init):
	                                # print("  --", init)
	                                if (init not in plusminusDiction[nnn]):
	                                    plusminusDiction[nnn].append(init)
	                                break

	    # pprint.pprint(plusminusDiction)

	    # entity creation part
	    listOfEntities = {}
	    listctr = 0
	    count = 1
	    for eachkey in plusminusDiction:
	        if (len(plusminusDiction[eachkey])):
	            # print(tempsent)
	            for eachval in plusminusDiction[eachkey]:
	                temp = {}
	                listctr += 1

	                # temporary=tempsent
	                tlis = eachkey.split()
	                lastword = " " + tlis[len(tlis) - 1]

	                tlisval = eachval.split()

	                # print(lastword)
	                lastwordindex = tempsent.find(lastword)
	                # print(lastwordindex,len(tempsent))
	                indexOfPrep = tempsent.find("(IN", lastwordindex) + 3
	                indexOfConj = tempsent.find("(CC", lastwordindex) + 3
	                # print(indexOfPrep,tempsent[indexOfPrep:])
	                indexofbracket = tempsent.find(")(", indexOfPrep)

	                if (indexOfPrep - indexOfConj > 0):
	                    loooo = indexOfPrep
	                else:
	                    loooo = indexOfConj

	                lwordofKey = completeSe.find(lastword)
	                fwordofVal = completeSe.find(tlisval[0])
	                # print(completeSe[lwordofKey+len(lastword):fwordofVal])

	                i0 = listofwords.index(lastword.strip())
	                i1 = listofwords.index(tlisval[0])
	                between = []
	                inbetween = " "
	                # print(i0, i1)
	                for ctr in range(i0 + 1, i1):
	                    itspos = POS[listofwords[ctr]]
	                    # print(itspos)
	                    if (itspos not in nouns and itspos not in fullstop):
	                        between.append(listofwords[ctr])
	                        inbetween = inbetween + " " + listofwords[ctr]

	                temp["sentenceCtr"] = sentctr
	                temp["POS"]=POS
	                temp["independent"] = "False"
	                # temp["connection"]=tempsent[loooo:indexofbracket]
	                # temp["related"]=completeSe[lwordofKey+len(lastword):fwordofVal]
	                temp["relation"] = inbetween
	                temp["id"] = eachkey
	                temp["relatedto"] = eachval
	                listOfEntities[listctr] = temp
	                # else:
	                #     temp={}
	                #     listctr+=1
	                #     partsofkey=eachkey.split()
	                #     beedee=da["basic-dependencies"]
	                #     for p in partsofkey:
	                #         for bd in beedee:
	                #             if(p in bd["dependentGloss"]):
	                #                 if(POS[bd["governorGloss"]] in nouns and bd["governorGloss"] not in partsofkey):
	                #                     temp["id"]=eachkey
	                #                     temp["relatedto"]=bd["governorGloss"]
	                #                     temp["independent"]="~True"
	                #
	                #     # temp["id"]=eachkey
	                #                     listOfEntities[listctr]=temp

	    # add the VERB dependencies
	    alistctr = 0
	    anotherlistOfEntities = []
	    for dep in da["deps_basic"]:
	        if (POS[da["tokens"][dep[1]]] in verbs):
	            for nnn in unredundantNNs:
	                if (da["tokens"][dep[2]] in nnn):
	                    # print("\n\n***********************")
	                    # print(da["tokens"][dep[1]], nnn)
	                    alistctr += 1
	                    temp = {}
	                    temp["independent"] = "--False"
	                    temp["relation"] = "VERB"
	                    temp["id"] = da["tokens"][dep[1]]
	                    temp["relatedto"] = nnn
	                    anotherlistOfEntities.append(temp)
	    # print(anotherlistOfEntities)

	    for itemmctr in range(0, len(anotherlistOfEntities)):
	        itemm = anotherlistOfEntities[itemmctr]
	        verb = itemm["id"]
	        for doosriitemmctr in range(itemmctr + 1, len(anotherlistOfEntities)):
	            doosriitemm = anotherlistOfEntities[doosriitemmctr]
	            if (itemm != doosriitemm and verb == doosriitemm["id"]):
	                listctr += 1
	                temp = {}
	                temp["sentenceCtr"] = sentctr
	                temp["POS"]=POS
	                temp["independent"] = "False"
	                temp["relation"] = verb
	                temp["id"] = itemm["relatedto"]
	                temp["relatedto"] = doosriitemm["relatedto"]
	                listOfEntities[listctr] = temp

	    # print()
	    # print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
	    # pprint.pprint(listOfEntities)
	    for lie in listOfEntities:
	        # print(listOfEntities[lie],",")
	        lasttry.append(listOfEntities[lie])
	        # print(",")
	    # print()
	    outerDiction["index"] = sentctr
	    outerDiction["statement"] = completeSe
	    outerDiction["processed-output"] = listOfEntities


	    # outermost["sentences"].append(outerDiction)

	# pprint.pprint(outermost)
	# pprint.pprint(lasttry)

	return data, lasttry, np








def getNNN(data):

	nouns = ["NN", "NNP", "NNS", "NNPS", "NP"]
	fullstop = [".", "?", ",", "!", "-"]
	verbs = ["VB", "VBZ", "VBP", "VBG", "VBN", "VBD"]
	det = ["DT"]
	lasttry = []
	pronouns = ["PRP", "PRP$"]
	outermost = {}
	outermost["sentences"] = []
	outerDiction = {}
	sentctr = 0

	all_nnn = []

	for da in data["sentences"]:
	    sentctr = sentctr + 1

	    newDict = {}
	    POS = {"root": ""}
	    listofwords = []

	    for matar in da["tokens"]:
	        POS[matar["word"]] = matar["pos"]
	        listofwords.append(matar["word"])
	    tempsent = " ".join(da["parse"].split())
	    tempsent = tempsent.replace(" (", "(")
	    sent = tempsent

	    t = nltk.tree.Tree.fromstring(sent)
	    completeSe = " ".join(t.leaves())

	    np = [" ".join(i.leaves()) for i in t.subtrees() if
	          i.label() == 'NP']  # in nouns or i.label() in pronouns ] # == 'NP' or i.label() == 'NN' ]
	    np.sort(key=len)

	    all_nnn = all_nnn + np

	# return all_nnn

	    answerDiction = {}
	    unredundantNNs1 = []
	    unredundantNNs = []
	    flagmarker = []
	    for ind in range(0, len(np)):
	        npeas = np[ind]
	        flag = 0
	        for ind2 in range(0, len(np)):
	            if (ind != ind2):
	                if (npeas in np[ind2]):
	                    flagmarker.append(ind2)
	                    flag = 1

	                    break



	    for x in range(0, len(np)):
	        if (x not in flagmarker):
	            unredundantNNs.append(np[x])


	    all_nnn = all_nnn + unredundantNNs

	raw_nnn_txt = ' '.join(all_nnn)

	raw_nnn_txt = raw_nnn_txt.split()

	taggings = []
	noun_nnn = []
	excluding_tags = []

	for txt in raw_nnn_txt:
		tag = nltk.pos_tag(nltk.word_tokenize(txt))
		if tag[0][1] in ['NN', 'NNS', 'NNP', 'NNPS']:
			if tag[0][0] != "":
				noun_nnn.append(tag[0][0])
		else:
			excluding_tags.append(tag[0])

	# print "noun_nnn", noun_nnn
	# print "excluding_tags", excluding_tags

	return noun_nnn





def parseFromScrape(data):

	print "data", data
	
	nouns = ["NN", "NNP", "NNS", "NNPS", "NP"]
	fullstop = [".", "?", ",", "!", "-"]
	verbs = ["VB", "VBZ", "VBP", "VBG", "VBN", "VBD"]
	det = ["DT"]
	lasttry = []
	pronouns = ["PRP", "PRP$"]
	outermost = {}
	outermost["sentences"] = []
	outerDiction = {}
	sentctr = 0
	for da in data["sentences"]:
	    sentctr = sentctr + 1
	    # print("\n**********************************************************")


	    # print(corefer["coreference"]["coreference"])
	    newDict = {}
	    POS = {"root": ""}
	    listofwords = []

	    for matar in da["tokens"]:
	        POS[matar["word"]] = matar["pos"]
	        listofwords.append(matar["word"])
	    # print(POS)
	    # print(listofwords)
	    # pprint.pprint(POS)
	    # print(sent.replace("        ","").replace("      ",""))
	    tempsent = " ".join(da["parse"].split())
	    tempsent = tempsent.replace(" (", "(")
	    # print(tempsent)
	    sent = tempsent

	    t = nltk.tree.Tree.fromstring(sent)
	    completeSe = " ".join(t.leaves())
	    # print(completeSe)
	    # pprint.pprint(t)
	    # t.pretty_print()

	    np = [" ".join(i.leaves()) for i in t.subtrees() if
	          i.label() == 'NP']  # in nouns or i.label() in pronouns ] # == 'NP' or i.label() == 'NN' ]
	    np.sort(key=len)

	    # print(np)
	    # for nnn in np:
	    #     print(np.index(nnn),nnn)

	    # ----------------------------------------------------------------------------------------------------------
	    answerDiction = {}
	    unredundantNNs1 = []
	    unredundantNNs = []
	    flagmarker = []
	    for ind in range(0, len(np)):
	        npeas = np[ind]
	        flag = 0
	        for ind2 in range(0, len(np)):
	            if (ind != ind2):
	                # print(npeas in np[ind2],"\t",npeas)
	                if (npeas in np[ind2]):
	                    # print("------",npeas,"===", np[ind2])
	                    flagmarker.append(ind2)
	                    flag = 1

	                    break
	                    # if(flag==0):
	                    #     unredundantNNs1.append(npeas)
	    # print(set(flagmarker))
	    # print()



	    for x in range(0, len(np)):
	        if (x not in flagmarker):
	            unredundantNNs.append(np[x])
	            # print(np[x])

	    # print("~~~~~~~~~~~~~~~~~~~~~```")
	    # for nnn in unredundantNNs:
	    #     print(nnn)

	    # ----------------------------------------------------------------------------------------------------------





	    nounsHere = []
	    for t in da["tokens"]:
	        if (t["pos"] in nouns):
	            # print("*****",t["word"])
	            nounsHere.append(t["word"])
	    # print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	    # print("Nouns: ")
	    # print(nounsHere)
	    # print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	    # print()


	    inkePOS = {}
	    for t in da["tokens"]:
	        inkePOS[t["word"]] = t["pos"]
	    # print("**********")
	    # print(inkePOS)

	    for d in da["basic-dependencies"]:
	        if (inkePOS[d["dependentGloss"]] in nouns):
	            if (d["governorGloss"] not in newDict):
	                newDict[d["governorGloss"]] = [d["dependentGloss"]]
	            else:
	                newDict[d["governorGloss"]].append(d["dependentGloss"])
	    # pprint.pprint(newDict)


	    nayiwaaliDict = {}
	    for nou in nounsHere:
	        if (nou in newDict):
	            # print("\n",nou, " : ",newDict[nou])
	            nayiwaaliDict[nou] = newDict[nou]
	        else:
	            doo = 0
	            # print("\n",nou)

	    # print()

	    plusminusDiction = {}
	    for nnn in unredundantNNs:
	        # print("++", nnn)
	        plusminusDiction[nnn] = []
	        splitwordsoflambesentence = nnn.split(" ")
	        for eachw in splitwordsoflambesentence:
	            if (eachw in nayiwaaliDict):
	                itsVal = nayiwaaliDict[eachw]
	                for v in itsVal:
	                    for init in unredundantNNs:
	                        # print("     ", v, nnn)
	                        if (init != nnn):
	                            if (v in init):
	                                # print("  --", init)
	                                if (init not in plusminusDiction[nnn]):
	                                    plusminusDiction[nnn].append(init)
	                                break

	    # pprint.pprint(plusminusDiction)

	    # entity creation part
	    listOfEntities = {}
	    listctr = 0
	    count = 1
	    for eachkey in plusminusDiction:
	        if (len(plusminusDiction[eachkey])):
	            # print(tempsent)
	            for eachval in plusminusDiction[eachkey]:
	                temp = {}
	                listctr += 1

	                # temporary=tempsent
	                tlis = eachkey.split()
	                lastword = " " + tlis[len(tlis) - 1]

	                tlisval = eachval.split()

	                # print(lastword)
	                lastwordindex = tempsent.find(lastword)
	                # print(lastwordindex,len(tempsent))
	                indexOfPrep = tempsent.find("(IN", lastwordindex) + 3
	                indexOfConj = tempsent.find("(CC", lastwordindex) + 3
	                # print(indexOfPrep,tempsent[indexOfPrep:])
	                indexofbracket = tempsent.find(")(", indexOfPrep)

	                if (indexOfPrep - indexOfConj > 0):
	                    loooo = indexOfPrep
	                else:
	                    loooo = indexOfConj

	                lwordofKey = completeSe.find(lastword)
	                fwordofVal = completeSe.find(tlisval[0])
	                # print(completeSe[lwordofKey+len(lastword):fwordofVal])

	                i0 = listofwords.index(lastword.strip())
	                i1 = listofwords.index(tlisval[0])
	                between = []
	                inbetween = " "
	                # print(i0, i1)
	                for ctr in range(i0 + 1, i1):
	                    itspos = POS[listofwords[ctr]]
	                    # print(itspos)
	                    if (itspos not in nouns and itspos not in fullstop):
	                        between.append(listofwords[ctr])
	                        inbetween = inbetween + " " + listofwords[ctr]

	                temp["sentenceCtr"] = sentctr
	                temp["POS"]=POS
	                temp["independent"] = "False"
	                # temp["connection"]=tempsent[loooo:indexofbracket]
	                # temp["related"]=completeSe[lwordofKey+len(lastword):fwordofVal]
	                temp["relation"] = inbetween
	                temp["id"] = eachkey
	                temp["relatedto"] = eachval
	                listOfEntities[listctr] = temp
	                # else:
	                #     temp={}
	                #     listctr+=1
	                #     partsofkey=eachkey.split()
	                #     beedee=da["basic-dependencies"]
	                #     for p in partsofkey:
	                #         for bd in beedee:
	                #             if(p in bd["dependentGloss"]):
	                #                 if(POS[bd["governorGloss"]] in nouns and bd["governorGloss"] not in partsofkey):
	                #                     temp["id"]=eachkey
	                #                     temp["relatedto"]=bd["governorGloss"]
	                #                     temp["independent"]="~True"
	                #
	                #     # temp["id"]=eachkey
	                #                     listOfEntities[listctr]=temp

	    # add the VERB dependencies
	    alistctr = 0
	    anotherlistOfEntities = []
	    for dep in da["basic-dependencies"]:
	        if (POS[dep["governorGloss"]] in verbs):
	            for nnn in unredundantNNs:
	                if (dep["dependentGloss"] in nnn):
	                    # print("\n\n***********************")
	                    # print(dep["governorGloss"], nnn)
	                    alistctr += 1
	                    temp = {}
	                    temp["independent"] = "--False"
	                    temp["relation"] = "VERB"
	                    temp["id"] = dep["governorGloss"]
	                    temp["relatedto"] = nnn
	                    anotherlistOfEntities.append(temp)
	    # print(anotherlistOfEntities)

	    for itemmctr in range(0, len(anotherlistOfEntities)):
	        itemm = anotherlistOfEntities[itemmctr]
	        verb = itemm["id"]
	        for doosriitemmctr in range(itemmctr + 1, len(anotherlistOfEntities)):
	            doosriitemm = anotherlistOfEntities[doosriitemmctr]
	            if (itemm != doosriitemm and verb == doosriitemm["id"]):
	                listctr += 1
	                temp = {}
	                temp["sentenceCtr"] = sentctr
	                temp["POS"]=POS
	                temp["independent"] = "False"
	                temp["relation"] = verb
	                temp["id"] = itemm["relatedto"]
	                temp["relatedto"] = doosriitemm["relatedto"]
	                listOfEntities[listctr] = temp

	    # print()
	    # print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
	    # pprint.pprint(listOfEntities)
	    for lie in listOfEntities:
	        # print(listOfEntities[lie],",")
	        lasttry.append(listOfEntities[lie])
	        # print(",")
	    # print()
	    outerDiction["index"] = sentctr
	    outerDiction["statement"] = completeSe
	    outerDiction["processed-output"] = listOfEntities


	    # outermost["sentences"].append(outerDiction)

	# pprint.pprint(outermost)
	# pprint.pprint(lasttry)

	return lasttry, np