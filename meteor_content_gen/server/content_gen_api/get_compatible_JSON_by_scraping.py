# -*- coding: utf-8 -*-
__author__ = 'ayush'

import stanfordParserModule
import scrapeStanfordNLPmodule
import nltk.data
import scrape_json_to_offline_json

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

def get_json(input_text, get_scrape_json=None):

	sentences = tokenizer.tokenize(input_text)

	input_text = ' '.join(str(x) for x in sentences[:10])
	input_text = ''.join([i if ord(i) < 128 else ' ' for i in input_text])
	
	print "scraping...", input_text
	scrape_json = scrapeStanfordNLPmodule.scrape(input_text)

	if get_scrape_json:
		return scrape_json

	offline_json = scrape_json_to_offline_json.convert_json(scrape_json)
	
	return offline_json


if __name__ == "__main__":
	input_text = "I take care of the puppy with my grandma. Grandma shows me how to take care of the puppy."
	compatibleJSON = get_json(input_text)
	print compatibleJSON

