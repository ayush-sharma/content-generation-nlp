# -*- coding: utf-8 -*-
from pycorenlp import StanfordCoreNLP
import json
import get_compatible_JSON_by_scraping

nlp = StanfordCoreNLP('http://localhost:9000')
stanford_type = "online"

def get_output(text):

	if stanford_type == "offline":
		output = nlp.annotate(text, properties={
			'annotators': 'tokenize, ssplit, pos, ner, parse',
			'outputFormat': 'json'
		})
		# print "output ->>", output string of crashed
		return output
	else:
		try:
			return get_compatible_JSON_by_scraping.get_json(text)
		except Exception as e:
			print "no working internet connection"
			output = nlp.annotate(text, properties={
				'annotators': 'tokenize, ssplit, pos, ner, parse',
				'outputFormat': 'json'
			})
			# print "output ->>", output string of crashed
			return output


if __name__ == "__main__":

	# out = get_output("The atom is a basic unit of matter, it consists of a dense central nucleus surrounded by a cloud of negatively charged electrons.")
	# out = get_output("The atoms are a basic unit of matter, they consists of a dense central nucleus surrounded by a cloud of negatively charged electrons.")
	# out = get_output("I take care of the puppy with my grandma. Grandma shows me how to take care of the puppy.")
	# out = get_output("Raunak just met a boy. He is so young that he did not have a name yet. His feet were very small.")
	out = get_output("Bud is a bird. He wakes up early to catch a worm. Bud is very hungry. Will is a worm. He moves around in the dirt. Will sees Bud flying in the sky. Will moves near a tree. Bud flies around the tree. Will moves to the garden. He still sees Bud flying in the sky. I must find some place safe to hide. Will looks around and disappears as Bud swoops down. Bud eats some bird seeds that he finds on the ground. Bud did not see Will.Some fireflies glow to warn other animals that they don't taste good. Frogs, bats, and birds do not like to eat animals that glow. The glow helps keep fireflies safe. Ben and his dad went fishing. Ben took the pole. Ben liked the boat ride. Ben put a worm on the hook. He put his line in the water. He felt the pole pull. Was it a fish? He reeled it in. He did not get a fish. Ben got an old can.")
	print out
