# from wit import Wit
from pprint import pprint
import json, requests, copy
from subprocess import *
from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
# from GetObjects import *
from getObjectsInternal import getObjects
from extractingFunctions import extractDeps
main = ['subS',
 'subAdj',
 'subDet',
 'subPrep',
 'objS',
 'objAdj',
 'objDet',
 'objPrep',
 'verb',
 'adv',
 'inter'
 ]

allQsTypes = {
 'who':'WHO_SUBJECT',
 'where':'WHERE',
 'how':'HOW_PREDICATE',
 'what_subject':'WHAT_SUBJECT',
 'what_object':'WHAT_OBJECT',
 }

def fromWit(text, ttype):
    mainObj = {}

    # print "WIT CALLED", text, ttype
    if ttype=="qs":
        token = '6KSZIJOSZVA2PAJPH54BIPWREE4HTY53'
    else:
        token = 'IVVQLDL5WZSLPBU33PKPJ232MMBQUOBX'

    client = Wit(access_token=token)
    resp = client.message(text)
    allEnt = [ x for x in json.loads(requests.get('https://api.wit.ai/entities?v=20141022', headers={'Authorization': 'Bearer ' + token}).text) if x[0:3] != 'wit' ]
    for m in main:
        if m in resp['entities'].keys():
            values = resp['entities'][m]
            mainObj[m] = ''
            for val in values:
                # print "val", val
                mainObj[m] = val['value']
    # print '\n\n',"MAINOBJ", mainObj
    return mainObj


def objectToStatement(obj):
    obj = json.loads(obj)
    text = ''
    for m in main:
        if m in obj.keys() and obj[m]!="" and m!="inter":
            text += obj[m].lower() + ',' + m + ','
        if m=="inter" and m in obj.keys():
            text += obj[m] + ',' + m + ','
    return text


def statementToLine(statement):
    args = ['./test.jar', statement]
    process = Popen(['java', '-jar'] + list(args), stdout=PIPE, stderr=PIPE)
    ret = []
    while process.poll() is None:
        line = process.stdout.readline()
        if line != '' and line.endswith('\n'):
            ret.append(line[:-1])

    stdout, stderr = process.communicate()
    ret += stdout.split('\n')
    if stderr != '':
        ret += stderr.split('\n')
    ret.remove('')
    return ret


def createParsedUnderstanding(text, qs, ans):
    objects = {}
    ansType = {}
    textType = fromWit(text, "ans")
    objects['qsType'] = fromWit(qs, "qs")
    objects['qsType']['inter'] = allQsTypes[objects['qsType']['inter'].lower()]
    objects['textType'] = textType
    ansLemmas = [wordnet_lemmatizer.lemmatize(qx, pos='v') for qx in ans.split(' ') ]
    for key in textType.keys():
        ansType[key] = ''
        if key != 'verb':
            if textType[key] != '' and textType[key].lower() in ansLemmas:
                ansType[key] = textType[key]
        elif textType[key] != '' and wordnet_lemmatizer.lemmatize(textType[key], pos='v') in ansLemmas:
            ansType[key] = textType[key]
    objects['ansType'] = ansType
    return objects


def createParsedUnderstandingFromInternal(text, qs, ans):
    objects = {}
    deps, text_pos, tokenObjs_text = extractDeps(text)
    textType = getObjects(text, text_pos, deps)
    deps, qs_pos, tokenObjs_qs = extractDeps(qs)
    qsType = getObjects(qs, qs_pos, deps)

    ans = ans.split()

    inter = None
    for word in qs_pos:
        if word[1][0] == "W":
            inter = word[0]
            break

    print "captured inter", inter

    for entity in qsType:
        if qsType[entity].lower() == inter.lower():
            qsType[entity] = ''

    qsType['inter'] = inter

    print "textType", textType
    print "ans", ans

    ansType = {}
    for k in textType.keys():
        ansType[k]=""
        if textType[k]!="":
            if textType[k] in ans:
                ansType[k] = textType[k]

                if qsType['inter'] == "what":
                    if k == "subS":
                        qsType['inter'] = allQsTypes["what_subject"]
                    else:
                        qsType['inter'] = allQsTypes["what_object"]
                else:
                    qsType['inter'] = allQsTypes[inter]


    objects["textType"] = textType
    objects["qsType"] = qsType
    objects["ansType"] = ansType

    nerMapping = {}
    for attr in tokenObjs_text:
        nerMapping[attr["word"]] = attr["NER"]
    objects["nerMapping"] = nerMapping

    objects["subject_ner"] = objects["nerMapping"][objects["textType"]["subS"]]

    objects["subject_ner"] = 'O'
    if objects["textType"]["subS"] in objects["nerMapping"]:
        objects["subject_ner"] = objects["nerMapping"][objects["textType"]["subS"]]


    objects["object_ner"] = 'O'
    if objects["textType"]["objS"] in objects["nerMapping"]:
        objects["object_ner"] = objects["nerMapping"][objects["textType"]["objS"]]

    pred_ners = {objects["textType"]["subS"]: {"entity": "subject", "ner": [objects["subject_ner"]]}, objects["textType"]["objS"]: {"entity": "object", "ner": [objects["object_ner"]]}}
    sample = {"qsSampleObject": qsType, "ansSampleObject": ansType, "pred_entities": textType, "pred_ners": pred_ners}


    
    print "sample in getObjectsWit", sample
    return sample

def parsedUnderstandingSimilar(textObj, qsSampleObject, ansSampleObject):
    # print "------------------"
    print "\n\n","new textObj",textObj, qsSampleObject, ansSampleObject
    # exit(0)
    # print "\n\n","qsSampleObject old", qsSampleObject 
    # print "\n\n","ansSampleObject old", ansSampleObject
    simQsObject= {}
    simAnsObject= {}
    text = ""
    qs = ""
    ans = ""
    for m in main:
        simQsObject[m]=""
        simAnsObject[m]=""
        if m in qsSampleObject.keys() and m !="inter":
            if qsSampleObject[m]!="":
                simQsObject[m]=textObj[m]
        elif m =="inter":
            simQsObject[m]=qsSampleObject[m]
            # simQsObject[m]="HOW_PREDICATE"
        if m in ansSampleObject.keys() and m !="inter":
            if ansSampleObject[m]!="":
                if textObj[m] !="":
                    simAnsObject[m]=textObj[m]
                else: 
                    simAnsObject[m] = "NA"
    # simAnsObject['inter']="WHO_INDIRECT_OBJECT" 

    # print "\n\n","simQsObject", simQsObject
    # print "\n\n","simAnsObject", simAnsObject
    qsState = objectToStatement(json.dumps(simQsObject))
    print "qsState", type(qsState), qsState
    # print "qState[0]", qsState[0]
    qs = str(statementToLine(qsState[:-1])[0])
    # print "qs", qs
    ansState = objectToStatement(json.dumps(simAnsObject))
    # print "ansState", ansState
    ans = str(statementToLine(ansState[:-1])[0])
    # print "ans", ans
    textState = objectToStatement(json.dumps(textObj))
    # print "textState", textState
    text = str(statementToLine(textState[:-1])[0])
    # print "text", text

    return [text, qs, ans]

# print statementToLine("sam,subS,sat,verb,HOW_PREDICATE,inter")

if __name__ == "__main__":
    # createParsedUnderstandingFromInternal("John goes to the school.", "who goes to the school.", "John")
    createParsedUnderstandingFromInternal("John goes to the school", "where does John go", "school")