# -*- coding: utf-8 -*-
import feat_generator
import numpy
import call_stanford_parser_server_api
import generate_sentences
import time
from operator import mul
import nltk.data
import json
import itertools

MAX_QUESTIONS = 10
NER_INDUCTION = False


def get_combos_from_combo(combo):
	entities_words = {}
	for entity, words in combo.iteritems():
		if type(words) == list:
			entities_words[entity] = words
		else:
			entities_words[entity] = [words]
	
	entities = entities_words.keys()
	words = entities_words.values()
	combos_from_combo = []

	all_combos = list(itertools.product(*words))
	
	combos_from_combo = []
	for tup in all_combos:
		new_combo = {}
		for index, word in enumerate(tup):
			new_combo[entities[index]] = word
		combos_from_combo.append(new_combo)
	return combos_from_combo



def get_satisfied_combos(corpus_dict, entity_mappings, subject_verb_object_combo, required_entities):

	satisfied_in_passage_combos = []
	satisfied_non_passage_combos = []

	for each_combo in subject_verb_object_combo:
		for required_entity in required_entities:
			main_entity = required_entity.split('_')
			if main_entity[0] == "sub":
				if main_entity[1] in entity_mappings["subjects"][each_combo["subject"]]:					
					for value in entity_mappings["subjects"][each_combo["subject"]][main_entity[1]]:
						if value:
							each_combo.setdefault(required_entity, [])
							each_combo[required_entity].append(value)
			elif main_entity[0] == "obj":
				if main_entity[1] in entity_mappings["objects"][each_combo["object"]]:					
					for value in entity_mappings["objects"][each_combo["object"]][main_entity[1]]:
						if value:
							each_combo.setdefault(required_entity, [])
							each_combo[required_entity].append(value)
			elif main_entity[0] == "verb":
				if main_entity[1] in entity_mappings["verbs"][each_combo["verb"]]:					
					for value in entity_mappings["verbs"][each_combo["verb"]][main_entity[1]]:
						if value:
							each_combo.setdefault(required_entity, [])
							each_combo[required_entity].append(value)
			else:
				# main_entity == "adverb"
				if main_entity[0] in entity_mappings["verbs"][each_combo["verb"]]:					
					for value in entity_mappings["verbs"][each_combo["verb"]][main_entity[0]]:
						if value:
							each_combo.setdefault(required_entity, [])
							each_combo[required_entity].append(value)

		combos_from_combo = get_combos_from_combo(each_combo)

		for combo_from_combo in combos_from_combo:
			if check_all_entity_satisfied(required_entities, combo_from_combo):
				if check_sentence_in_passage(combo_from_combo, corpus_dict):
					satisfied_in_passage_combos.append(combo_from_combo)
				else:
					satisfied_non_passage_combos.append(combo_from_combo)

	# print "satisfied_in_passage_combos", satisfied_in_passage_combos
	# print "satisfied_non_passage_combos", satisfied_non_passage_combos

	satisfied_combos = {"in_passage": satisfied_in_passage_combos, "out_passage": satisfied_non_passage_combos}
	print "\nsatisfied_combos", satisfied_combos
	# inp = raw_input('paused')
	return satisfied_combos





def check_sentence_in_passage(combo, corpus_dict):
	sentence_index = None
	for entity, value in combo.iteritems():
		if type(value) == list:
			value = value[0]
		value_sentence_index = corpus_dict[value.lower()]["__Sentence_Index__"]

		if sentence_index == None:
			if len(value_sentence_index) > 1:
				sentence_index = value_sentence_index
			else:
				sentence_index = value_sentence_index[0]
			continue

		if type(sentence_index) == list:
			if len(value_sentence_index) > 1:
				common_sentence_index = list(set(sentence_index).intersection(value_sentence_index))
				if len(common_sentence_index) == 0:
					return False
				if len(common_sentence_index) == 1:
					sentence_index = common_sentence_index[0]
				else:
					sentence_index = common_sentence_index
			else:
				if value_sentence_index[0] in sentence_index:
					sentence_index = value_sentence_index[0]
				else:
					return False
		else:
			if sentence_index in value_sentence_index:
				continue
			else:
				return False
	return True




def generate_questions(input_text, sample):

	tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
	sentences = tokenizer.tokenize(input_text)
	input_text = [{"text": i} for i in sentences]
	print "input_text", json.dumps(input_text, indent=4)

	corpus_dict = get_nearby_words(input_text)

	pred_entities, pred_ners = get_predicted_entities(input_text)
	# inp = raw_input('paused')

	entity_mappings = get_entity_mappings(pred_entities)
	print "entity_mappings", entity_mappings, "\n"

	subject_verb_object_combo = get_subject_verb_object_combo(entity_mappings, pred_ners, sample["pred_ners"])
	# subject_verb_object_combo = get_subject_verb_object_combo(entity_mappings, pred_ners)
	print "subject_verb_object_combo", subject_verb_object_combo
	print "len subject_verb_object_combo", len(subject_verb_object_combo)
	# inp = raw_input('paused')

	required_entities = get_required_entities(sample["pred_entities"])
	print "\nrequired_entities", required_entities

	satisfied_combos = get_satisfied_combos(corpus_dict, entity_mappings, subject_verb_object_combo, required_entities)


	# inp = raw_input('paused')
	start_time = time.time()

	questions_generated = {}

	for type_of_combos in satisfied_combos:
		questions_generated[type_of_combos] = []
		for index, combo in enumerate(satisfied_combos[type_of_combos]):
			question = generate_sentences.parsedUnderstandingSimilar(combo, sample)
			score, score_stats = get_combo_distance(combo, corpus_dict)
			question["score"] = score
			question["score_stats"] = score_stats
			questions_generated[type_of_combos].append(question)
			print "count", type_of_combos, index, " / ", len(satisfied_combos[type_of_combos])
			if type_of_combos != "in_passage" and index >= 20:
				break

	# print "\nquestions_generated", questions_generated

	print "\nTIME TAKEN:", time.time() - start_time

	sorted_questions_generated = {}
	for type_of_combos in questions_generated:
		sorted_questions_generated[type_of_combos] = sorted(questions_generated[type_of_combos], key=lambda k: k['score'])[:MAX_QUESTIONS]

	print "\nsorted_questions_generated", sorted_questions_generated

	return sorted_questions_generated



def get_predicted_entities(input_text):
	X_test_all = []
	all_test_stanford_output = []

	XY_train = numpy.load('XY_train.npy').tolist()

	for index, text in enumerate(input_text):

		print "text ->", text["text"]

		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		all_test_stanford_output.append(stanford_output)

		feats = feat_generator.get_feats(stanford_output)

		feats = feats.flatten()

		X_test_all.append(feats)

	pred_entities = [{} for i in range(0, len(input_text))]
	pred_ners = {}

	for entity, XY in XY_train.iteritems():
		if entity not in ["adj", "obj_adj", "prep_ph"]:

			print "\n     Testing :", entity

			for index, X_test in enumerate(X_test_all):
				matches = feat_generator.get_similar_sentences(X_test, XY)

				pred_answer_tree = XY["y"][matches[0][2]]
				stanford_output_test = all_test_stanford_output[index]
				stanford_output_pred = XY["stanford_output"][matches[0][2]]

				pred_word = feat_generator.get_main_entities(pred_answer_tree, stanford_output_test, input_text[index], entity, stanford_output_pred)
				pred_entities[index][entity] = pred_word

				if entity in ["subject", "object"]:
					pred_ner = feat_generator.get_token_attr(stanford_output_test, pred_word)["ner"]
					pred_ners.setdefault(pred_word.lower(), {"ner": []})
					pred_ners[pred_word.lower()]["ner"].append(pred_ner)
					pred_ners[pred_word.lower()]["entity"] = entity

	print "init pred_entities", pred_entities

	for index, text in enumerate(input_text):
		test_stanford_output = all_test_stanford_output[index]
		pred_entities[index] = feat_generator.predict_next_entities(test_stanford_output, pred_entities[index], text)

	print "\npred_entities\n", pred_entities, "\n"

	return pred_entities, pred_ners



def get_words_distance(word1, word2, corpus_dict):
	if word1 in corpus_dict:
		if word2 in corpus_dict[word1]:
			return corpus_dict[word1][word2]
	return corpus_dict["__MAX__"]

def get_combo_distance(combo, corpus_dict):

	entity_score = {}
	verb_object_prep_det_score = []
	
	subject_verb_score = get_words_distance(combo["subject"], combo["verb"], corpus_dict)
	verb_object_score = get_words_distance(combo["verb"], combo["object"], corpus_dict)

	entity_score["subject_verb_score"] = subject_verb_score
	entity_score["verb_object_score"] = verb_object_score
	verb_object_prep_det_score.append(verb_object_score)

	if "verb_prep" in combo:
		verb_prep_score = 0
		object_prep_score = 0
		for prep in combo["verb_prep"]:
			verb_prep_score += get_words_distance(combo["verb"], prep, corpus_dict)
			object_prep_score += get_words_distance(combo["object"], prep, corpus_dict)
		verb_prep_score = verb_prep_score/float(len(combo["verb_prep"]))
		object_prep_score = object_prep_score/float(len(combo["verb_prep"]))

		entity_score["verb_prep_score"] = verb_prep_score
		entity_score["object_prep_score"] = object_prep_score
		verb_object_prep_det_score += [verb_prep_score, object_prep_score]

	if "sub_adj" in combo:
		subject_adjective_score = 0
		for adj in combo["sub_adj"]:
			subject_adjective_score += get_words_distance(combo["subject"], adj, corpus_dict)
		subject_adjective_score = subject_adjective_score/float(len(combo["sub_adj"]))

		entity_score["subject_adjective_score"] = subject_adjective_score

	if "obj_adj" in combo:
		object_adjective_score = 0
		for adj in combo["obj_adj"]:
			object_adjective_score += get_words_distance(combo["object"], adj, corpus_dict)
		object_adjective_score = object_adjective_score/float(len(combo["obj_adj"]))

		entity_score["object_adjective_score"] = object_adjective_score

	if "sub_det" in combo:
		subject_det_score = 0
		for det in combo["sub_det"]:
			subject_det_score += get_words_distance(combo["subject"], det, corpus_dict)
		subject_det_score = subject_det_score/float(len(combo["sub_det"]))

		entity_score["subject_det_score"] = subject_det_score

	if "obj_det" in combo:
		object_det_score = 0
		for det in combo["obj_det"]:
			object_det_score += get_words_distance(combo["object"], det, corpus_dict)
		object_det_score = object_det_score/float(len(combo["obj_det"]))

		entity_score["object_det_score"] = object_det_score
		verb_object_prep_det_score.append(object_det_score)

	if "adverb" in combo:
		verb_adverb_score = get_words_distance(combo["verb"], combo["adverb"], corpus_dict)
		entity_score["verb_adverb_score"] = verb_adverb_score

	# entity_score["verb_object_prep_det_score"] = pow(reduce(mul, verb_object_prep_det_score, 1), 1/len(verb_object_prep_det_score))
	# entity_score["verb_object_prep_det_score"] = sum(verb_object_prep_det_score)/float(len(verb_object_prep_det_score))
	entity_score["verb_object_prep_det_score"] = sum(verb_object_prep_det_score)

	score = sum([value for value in entity_score.values()])

	return score, entity_score



def get_nearby_words(input_text):
	corpus_dict = {}
	max_distance = 0

	for text_index, text in enumerate(input_text):
		text = text["text"].lower()
		text = ''.join(e for e in text if e.isalnum() or e==" ")
		text_split = text.split()
		for word_index, word in enumerate(text_split):
			corpus_dict.setdefault(word, {"__Sentence_Index__": []})
			corpus_dict[word]["__Sentence_Index__"].append(text_index)
			for compare_word_index, compare_word in enumerate(text_split):
				if word == compare_word:
					continue
				distance = abs(word_index - compare_word_index)

				if max_distance < distance:
					max_distance = distance + 1

				if compare_word in corpus_dict[word]:
					if corpus_dict[word][compare_word] > distance:
						corpus_dict[word][compare_word] = distance
				else:
					corpus_dict[word][compare_word] = distance

				# corpus_dict[word].setdefault(compare_word, [])
				# corpus_dict[word][compare_word].append(distance)

	corpus_dict["__MAX__"] = max_distance
	return corpus_dict



def check_all_entity_satisfied(required_entities, each_combo):
	for i in required_entities:
		if i not in each_combo:
			return False
	return True



def get_required_entities(sample):
	required_entities = []
	for entity in sample:
		if entity in ["subject", "object", "verb"]:
			continue
		if sample[entity]:
			required_entities.append(entity)
	return required_entities

def get_subject_verb_object_combo(entity_mappings, pred_ners, sample_pred_ners=None):

	unmatched_ners = {}

	if sample_pred_ners:
		sample_subject_ner = None
		sample_object_ner = None
		for word in sample_pred_ners:
			if sample_pred_ners[word]["entity"] == "subject":
				sample_subject_ner = sample_pred_ners[word]["ner"][0]
			else:
				sample_object_ner = sample_pred_ners[word]["ner"][0]

	subject_verb_object_combo = []
	for subject in entity_mappings["subjects"]:
		for verb in entity_mappings["verbs"]:
			for object in entity_mappings["objects"]:

				if not NER_INDUCTION:
					if subject.lower() != object.lower():
						subject_verb_object_combo.append({"subject": subject, "verb": verb, "object": object})
				else:					
					# ner induced
					if subject.lower() in pred_ners:
						if sample_subject_ner in pred_ners[subject.lower()]["ner"]:
							if object.lower() in pred_ners:
								if sample_object_ner in pred_ners[object.lower()]["ner"]:
									subject_verb_object_combo.append({"subject": subject, "verb": verb, "object": object})
								else:
									unmatched_ners.setdefault("object_" + pred_ners[object.lower()]["ner"], 0)
									unmatched_ners["object_" + pred_ners[object.lower()]["ner"]] += 1
							else:
								unmatched_ners.setdefault("object_absent", 0)
								unmatched_ners["object_absent"] += 1
						else:
							unmatched_ners.setdefault("subject_" + pred_ners[subject.lower()]["ner"], 0)
							unmatched_ners["subject_" + pred_ners[subject.lower()]["ner"]] += 1
					else:
						unmatched_ners.setdefault("subject_absent", 0)
						unmatched_ners["subject_absent"] += 1

	return subject_verb_object_combo


def get_entity_mappings(pred_entities):
	entity_mappings = {"subjects": {}, "verbs": {}, "objects": {}}
	for sentence in pred_entities:

		entity_mappings["subjects"].setdefault(sentence["subject"], {})
		entity_mappings["subjects"][sentence["subject"]].setdefault("adj", [])
		# entity_mappings["subjects"][sentence["subject"]].setdefault("prep", [])
		entity_mappings["subjects"][sentence["subject"]].setdefault("det", [])
		entity_mappings["subjects"][sentence["subject"]].setdefault("verb", [])

		entity_mappings["subjects"][sentence["subject"]]["adj"] += sentence["sub_adj"]
		# entity_mappings["subjects"][sentence["subject"]]["prep"].append(sentence["sub_prep"])
		entity_mappings["subjects"][sentence["subject"]]["det"].append(sentence["sub_det"])
		entity_mappings["subjects"][sentence["subject"]]["verb"].append(sentence["verb"])


		entity_mappings["objects"].setdefault(sentence["object"], {})
		entity_mappings["objects"][sentence["object"]].setdefault("adj", [])
		# entity_mappings["objects"][sentence["object"]].setdefault("prep", [])
		entity_mappings["objects"][sentence["object"]].setdefault("det", [])
		entity_mappings["objects"][sentence["object"]].setdefault("verb", [])

		entity_mappings["objects"][sentence["object"]]["adj"] += sentence["obj_adj"]
		# entity_mappings["objects"][sentence["object"]]["prep"].append(sentence["obj_prep"])
		entity_mappings["objects"][sentence["object"]]["det"].append(sentence["obj_det"])
		entity_mappings["objects"][sentence["object"]]["verb"].append(sentence["verb"])


		entity_mappings["verbs"].setdefault(sentence["verb"], {})
		entity_mappings["verbs"][sentence["verb"]].setdefault("adverb", [])
		entity_mappings["verbs"][sentence["verb"]].setdefault("prep", [])

		entity_mappings["verbs"][sentence["verb"]]["adverb"].append(sentence["adverb"])
		entity_mappings["verbs"][sentence["verb"]]["prep"].append(sentence["obj_prep"])

	# print "\nentity_mappings", entity_mappings
	return entity_mappings



if __name__ == "__main__":

	# input_text = "Bud is a bird. He wakes up early to catch a worm. Bud is very hungry. Will is a worm. He moves around in the dirt. Will sees Bud flying in the sky. Will moves near a tree. Bud flies around the tree. Will moves to the garden. He still sees Bud flying in the sky. I must find some place safe to hide. Will looks around and disappears as Bud swoops down. Bud eats some bird seeds that he finds on the ground. Bud did not see Will.Some fireflies glow to warn other animals that they don't taste good. Frogs, bats, and birds do not like to eat animals that glow. The glow helps keep fireflies safe. Ben and his dad went fishing. Ben took the pole. Ben liked the boat ride. Ben put a worm on the hook. He put his line in the water. He felt the pole pull. Was it a fish? He reeled it in. He did not get a fish. Ben got an old can."
	# input_text = "There is a hole in the wall. A mouse lives in the hole. The mouse is named Mel. There is a box in the house. A cat lives in the box. The cat is named Cal. Mel runs out of the hole and under the table. He gets some cheese. Cal runs under the table and chases Mel away. Mel runs back into the hole. Mel runs out of the hole and under the chair. He gets some bread. Cal runs under the chair and chases Mel away. Mel runs back into the hole. Mel runs out of the hole and across the floor. He gets some crackers. Cal runs across the floor and chases Mel away. Mel runs out the back door. He is full and free. Cal runs behind him, but Cal stops at the door. There is a crate behind the back door. Dan lives there. Dan is a dog. Dan chases Cal back into the house."
	# input_text = "Emma's friend, Alice, had a new lunch box. Emma ran to her room. She counted the coins in her piggy bank. I only have fifty cents. Ten dollars will take forever. She tried to think of other ways. Mrs. Robinson gave the class a pop quiz. She had an idea. She put the sticker on her lunch box. Emma put the sticker on her lunch box. Even the scratch was covered up."
	# input_text = "Alice went to a new restaurant. Emma ran to a room."
	# input_text = "In the driveway, Cody found a black rock. It had yellow and red marks on it. Cody touched the rock. A head poked out of one end. Two orange eyes peeked at Cody. Cody ran to the house. Mommy and Daddy came outside. Mommy and Cody picked up the turtle. They carried it into the back yard. There were trees there."
	
	# input_text = "My name is Priya. I am a student. My mother is a teacher. My father is an engineer. My sister is a doctor. My brother is a carpenter."
	# input_text = "My name is Vidhi. In my house, my brother and I do the dishes. My father does the shopping. My mother does the cooking. Our maid does the cleaning and the laundry."
	# input_text = "My name is Riya. My favourite colour is blue. My favourite animal is the cat. My favourite place is school. My favourite person is my brother."
	# input_text = "A bird is on the tree. My cat is next to the tree. My father is under the tree. My mother is in front of the tree."
	input_text = "My name is Ankur. Your name is Priya. I go to school. You go to school. I sit on a chair. You read a book. I take a pencil. You take a pen. I write in my book. You write on your paper."

	# sample = {"pred_entities": {'obj_adj': None, 'adverb': None, 'object': u'school', 'sub_adj': None, 'obj_det': u'the', 'verb': u'went', 'sub_det': None, 'sub_prep': None, 'verb_prep': u'to', 'subject': u'John'}}
	# sample = {"ansSampleObject": {"adv": "", "objPrep": "", "objS": "", "objAdj": "", "subS": "John", "subDet": "", "objDet": "", "verb": "", "subAdj": "", "subPrep": ""}, "text": {"adv": "", "objPrep": "to", "objS": "school", "objAdj": "", "subS": "John", "subDet": "", "objDet": "the", "verb": "goes", "subAdj": "", "subPrep": ""}, "pred_entities": {"obj_adj": '', "adverb": '', "object": "school", "sub_adj": '', "obj_det": "the", "verb": "goes", "sub_det": '', "sub_prep": '', "obj_prep": "to", "subject": "John"}, "qsSampleObject": {"adv": "", "objPrep": "to", "objS": "school", "objAdj": "", "subS": "who", "subDet": "", "objDet": "the", "verb": "goes", "subAdj": "", "subPrep": ""}}
	

	# where
	# sample = {'ansSampleObject': {'adv': '', 'objPrep': '', 'objS': 'school', 'objAdj': '', 'subS': '', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}, 'pred_entities': {'adverb': '', 'obj_adj': '', 'object': 'school', 'verb_prep': 'to', 'sub_adj': '', 'obj_det': 'the', 'verb': 'goes', 'sub_det': '', 'sub_prep': '', 'subject': 'John'}, 'qsSampleObject': {'objAdj': '', 'objDet': '', 'verb': 'go', 'subAdj': '', 'adv': '', 'objPrep': '', 'objS': '', 'subS': 'John', 'subDet': '', 'inter': 'WHERE', 'subPrep': ''}, 'pred_ners': {'school': {'ner': ['O'], 'entity': 'object'}, 'John': {'ner': ['PERSON'], 'entity': 'subject'}}}

	# who
	# sample = {'ansSampleObject': {'adv': '', 'objPrep': '', 'objS': '', 'objAdj': '', 'subS': 'John', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}, 'pred_entities': {'adverb': '', 'obj_adj': '', 'object': 'school', 'verb_prep': 'to', 'sub_adj': '', 'obj_det': 'the', 'verb': 'goes', 'sub_det': '', 'sub_prep': '', 'subject': 'John'}, 'qsSampleObject': {'objAdj': '', 'objDet': 'the', 'verb': 'goes', 'subAdj': '', 'adv': '', 'objPrep': 'to', 'objS': 'school', 'subS': '', 'subDet': '', 'inter': 'WHO_SUBJECT', 'subPrep': ''}, 'pred_ners': {'school': {'ner': ['O'], 'entity': 'object'}, 'John': {'ner': ['PERSON'], 'entity': 'subject'}}}

	# what
	# sample = {'ansSampleObject': {'adv': '', 'objPrep': '', 'objS': 'rock', 'objAdj': '', 'subS': '', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}, 'pred_entities': {'adverb': '', 'obj_adj': '', 'object': 'rock', 'verb_prep': '', 'sub_adj': '', 'obj_det': 'a', 'verb': 'picked', 'sub_det': '', 'sub_prep': '', 'subject': 'John'}, 'qsSampleObject': {'objAdj': '', 'objDet': '', 'verb': 'pick', 'subAdj': '', 'adv': '', 'objPrep': '', 'objS': '', 'subS': 'John', 'subDet': '', 'inter': 'WHAT_OBJECT', 'subPrep': ''}, 'pred_ners': {'John': {'ner': ['PERSON'], 'entity': 'subject'}, 'rock': {'ner': ['O'], 'entity': 'object'}}}
	
	# how
	# sample = {'ansSampleObject': {'adv': 'furiously', 'objPrep': '', 'objS': '', 'objAdj': '', 'subS': '', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}, 'pred_entities': {'adverb': 'furiously', 'obj_adj': '', 'object': '', 'verb_prep': '', 'sub_adj': '', 'obj_det': '', 'verb': 'ran', 'sub_det': '', 'sub_prep': '', 'subject': 'John'}, 'qsSampleObject': {'objAdj': '', 'objDet': '', 'verb': 'run', 'subAdj': '', 'adv': '', 'objPrep': '', 'objS': '', 'subS': 'John', 'subDet': '', 'inter': 'HOW_PREDICATE', 'subPrep': ''}, 'pred_ners': {'': {'ner': ['O'], 'entity': 'object'}, 'John': {'ner': ['PERSON'], 'entity': 'subject'}}}
	


	# what_object
	# sample = {'ansSampleObject': {'adv': '', 'objPrep': '', 'objS': 'rock', 'objAdj': '', 'subS': '', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}, 'pred_entities': {'adverb': '', 'obj_adj': '', 'object': 'rock', 'verb_prep': '', 'sub_adj': '', 'obj_det': 'a', 'verb': 'picked', 'sub_det': '', 'sub_prep': '', 'subject': 'John'}, 'qsSampleObject': {'objAdj': '', 'objDet': '', 'verb': 'pick', 'subAdj': '', 'adv': '', 'objPrep': '', 'objS': '', 'subS': 'John', 'subDet': '', 'inter': 'WHAT_OBJECT', 'subPrep': ''}, 'pred_ners': {'John': {'ner': ['PERSON'], 'entity': 'subject'}, 'rock': {'ner': ['O'], 'entity': 'object'}}}
	# sample = {'ansSampleObject': {'adv': '', 'objPrep': '', 'objS': '', 'objAdj': '', 'subS': 'John', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}, 'pred_entities': {'adverb': '', 'obj_adj': '', 'object': 'rock', 'verb_prep': '', 'sub_adj': '', 'obj_det': 'a', 'verb': 'picked', 'sub_det': '', 'sub_prep': '', 'subject': 'John'}, 'qsSampleObject': {'objAdj': '', 'objDet': 'a', 'verb': 'picked', 'subAdj': '', 'adv': '', 'objPrep': '', 'objS': 'rock', 'subS': '', 'subDet': '', 'inter': 'WHAT_SUBJECT', 'subPrep': ''}, 'pred_ners': {'John': {'ner': ['PERSON'], 'entity': 'subject'}, 'rock': {'ner': ['O'], 'entity': 'object'}}}

	# sample = {'ansSampleObject': {'adv': '', 'objPrep': '', 'objS': '', 'objAdj': '', 'subS': 'Martha', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}, 'pred_entities': {'adverb': '', 'obj_adj': '', 'object': 'cooking', 'verb_prep': '', 'sub_adj': '', 'obj_det': 'the', 'verb': 'does', 'sub_det': '', 'sub_prep': '', 'subject': 'Martha'}, 'qsSampleObject': {'objAdj': '', 'objDet': 'the', 'verb': 'does', 'subAdj': '', 'adv': '', 'objPrep': '', 'objS': 'cooking', 'subS': '', 'subDet': '', 'inter': 'WHO_SUBJECT', 'subPrep': ''}, 'pred_ners': {'Martha': {'ner': ['PERSON'], 'entity': 'subject'}, 'cooking': {'ner': ['O'], 'entity': 'object'}}}

	# sample = {'ansSampleObject': {'adv': '', 'objPrep': '', 'objS': 'cooking', 'objAdj': '', 'subS': '', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}, 'pred_entities': {'adverb': '', 'obj_adj': '', 'object': 'cooking', 'verb_prep': '', 'sub_adj': '', 'obj_det': 'the', 'verb': 'does', 'sub_det': '', 'sub_prep': '', 'subject': 'Martha'}, 'qsSampleObject': {'objAdj': '', 'objDet': '', 'verb': 'do', 'subAdj': '', 'adv': '', 'objPrep': '', 'objS': '', 'subS': 'Martha', 'subDet': '', 'inter': 'WHAT_OBJECT', 'subPrep': ''}, 'pred_ners': {'Martha': {'ner': ['PERSON'], 'entity': 'subject'}, 'cooking': {'ner': ['O'], 'entity': 'object'}}}
	# sample = {'ansSampleObject': {'adv': '', 'objPrep': '', 'objS': '', 'objAdj': '', 'subS': '', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': 'blue', 'subPrep': ''}, 'pred_entities': {'adverb': '', 'obj_adj': '', 'object': 'Her', 'verb_prep': '', 'sub_adj': 'blue', 'obj_det': '', 'verb': '', 'sub_det': 'Her', 'sub_prep': '', 'subject': 'colour'}, 'qsSampleObject': {'objAdj': '', 'objDet': '', 'verb': '', 'subAdj': 'favourite', 'adv': '', 'objPrep': '', 'objS': 'her', 'subS': 'color', 'subDet': 'her', 'inter': 'WHAT_OBJECT', 'subPrep': ''}, 'pred_ners': {'colour': {'ner': ['O'], 'entity': 'subject'}, 'Her': {'ner': ['O'], 'entity': 'object'}}}
	# sample = {'ansSampleObject': {'adv': '', 'objPrep': '', 'objS': '', 'objAdj': '', 'subS': 'I', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}, 'pred_entities': {'adverb': '', 'obj_adj': '', 'object': 'school', 'verb_prep': 'to', 'sub_adj': '', 'obj_det': '', 'verb': 'go', 'sub_det': '', 'sub_prep': '', 'subject': 'I'}, 'qsSampleObject': {'objAdj': '', 'objDet': '', 'verb': 'goes', 'subAdj': '', 'adv': '', 'objPrep': 'to', 'objS': 'school', 'subS': '', 'subDet': '', 'inter': 'WHO_SUBJECT', 'subPrep': ''}, 'pred_ners': {'I': {'ner': ['O'], 'entity': 'subject'}, 'school': {'ner': ['O'], 'entity': 'object'}}}
	sample = {'ansSampleObject': {'adv': '', 'objPrep': '', 'objS': 'book', 'objAdj': '', 'subS': '', 'subDet': '', 'objDet': '', 'verb': '', 'subAdj': '', 'subPrep': ''}, 'pred_entities': {'adverb': '', 'obj_adj': '', 'object': 'book', 'verb_prep': '', 'sub_adj': '', 'obj_det': 'a', 'verb': 'read', 'sub_det': '', 'sub_prep': '', 'subject': 'I'}, 'qsSampleObject': {'objAdj': '', 'objDet': '', 'verb': 'read', 'subAdj': '', 'adv': '', 'objPrep': '', 'objS': '', 'subS': 'i', 'subDet': '', 'inter': 'WHAT_OBJECT', 'subPrep': ''}, 'pred_ners': {'I': {'ner': ['O'], 'entity': 'subject'}, 'book': {'ner': ['O'], 'entity': 'object'}}}

	questions_generated = generate_questions(input_text, sample)








