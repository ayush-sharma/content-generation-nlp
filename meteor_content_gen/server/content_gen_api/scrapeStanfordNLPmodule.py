__author__ = 'ayush'

import requests
import HTMLParser
import re
import xmltodict
import json

h = HTMLParser.HTMLParser()

def scrape(inputText):

    # inputText = "In 1853, one adobe hut stood in Nopalera , named for the Mexican Nopal cactus indigenous to the area. By 1870, an agricultural community flourished. The area was known as the Cahuenga Valley, after the pass in the Santa Monica Mountains immediately to the north.According to the diary of H. J. Whitley, known as the 'Father of Hollywood', on his honeymoon in 1886 he stood at the top of the hill looking out over the valley. Along came a Chinese man in a wagon carrying wood. The man got out of the wagon and bowed. The Chinese man was asked what he was doing and replied, I holly-wood, meaning 'hauling wood.' H. J. Whitley had an epiphany and decided to name his new town Hollywood. Holly would represent England and wood would represent his Scottish heritage. Whitley had already started over 100 towns across the western United States."
    
    postData = {"Process": "Submit Query", "input": inputText, "outputFormat": "xml"}
    r = requests.post("http://nlp.stanford.edu:8080/corenlp/process", data=postData)
    
    rawText = r.text
    # print "rawText", rawText
    
    match = re.match(r".*?<pre>(.*?)</pre>.*?", rawText, re.DOTALL)
    
    if match:
        rawXml = match.group(1)
        rawXml = h.unescape(rawXml)
        # print rawXml
    
        scrapeJSON = json.dumps(xmltodict.parse(rawXml))
    
        scrapeJSON = eval(scrapeJSON)
        # print scrapeJSON
        return scrapeJSON
    else:
        return 0