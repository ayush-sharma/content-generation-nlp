Router.configure({
    layoutTemplate: 'layout'
});
Router.route('/home', function() {
    this.render('home');
});
Router.route('/', function() {
    this.render('home');
});
Router.route('/rules', function() {
    this.render('rules');
});

Router.route('/generate', function() {
    this.render('generate');
});

Router.route('/induct', function() {
    this.render('induct');
});

Router.route('/fileUpload', function() {
    this.render('fileUpload');
});
Router.route('/add', function() {
    this.render('add');
});
Router.route('/play', function() {
    this.render('play');
});
Router.route('/define', function() {
    this.render('define');
});
Router.route('/vocab', function() {
    this.render('vocab');
});
Router.route('/gram', function() {
    this.render('gram');
});
Router.route('/comp', function() {
    this.render('comp');
});
Router.route('/vocabRules', function() {
    this.render('vocabRules');
});
Router.route('/inductRules', function() {
    this.render('inductRules');
});