if (Meteor.isClient) {

    Session.setDefault('selectedSample', '');
    Session.setDefault('similar', '');
    Session.setDefault('selectedPdf', '');
    
    Template.generate.helpers({
        'vocabType': function() {
            if (Session.get('selectedPdf') == "") {
                return Vocab.find({ "base": "custom" })
            } else {
                return Vocab.find({ "base": Session.get('selectedPdf') })
            }
        },
        'typeWords': function() {
            return this.words
        },
        generatedSamples: function() {
            return Generated.find()
        },
        getSamples: function() {
            return Samples.find()
        },
        getSimilar: function() {
            var similar = Samples.findOne({ "_id": Session.get('selectedSample') }).similar
            Session.set('selectedSimilar', similar)
            console.log(similar)
            return Session.get('selectedSimilar')
        },
        getpdf: function() {
            return Base.find()
        }
    });
    Template.generate.events({
        'click #addVocab': function() {
            var vocabType = document.getElementById('vocabType').value
            if (Session.get('selectedPdf') == "") {
                Vocab.insert({ base: "custom", type: vocabType, words: [] })
            } else {
                Vocab.insert({ base: Session.get('selectedPdf'), type: vocabType, words: [] })
            }
        },
        'keyup .newWord': function(event) {
            if (event.which === 13) {
                console.log(event.target.value)
                var vocabWordsTemp = event.target.value
                var vocabWords = vocabWordsTemp.split(' ')
                for (var i = 0; i < vocabWords.length; i++) {
                    vocabWords[i] = vocabWords[i].trim()
                    var existing = Vocab.findOne({ _id: this._id }).words
                    if (existing.indexOf(vocabWords[i]) == -1) {
                        existing.push(vocabWords[i])
                        Vocab.update(this._id, { $set: { words: existing } })
                    }

                }
                event.target.value = ""
            }
        },
        'change .selectSample': function() {
            var x = document.getElementById('selectGenerateRule')
            var id = x.options[x.selectedIndex].id
            Session.set('selectedSample', id)
            console.log("clicked", id)

        },
        'click #updateSimilar': function() {
            var x = document.getElementById('selectGenerateRule')
            var id = x.options[x.selectedIndex].id
            console.log(x.options[x.selectedIndex].text, x.options[x.selectedIndex].id)
            var pdf = Session.get('selectedPdf')
            var arg = id + "," + pdf
            Meteor.call('generateSampleSimilar', arg)

        },
        'click .selectPdf': function(event) {
            console.log(event.target.id)
            Session.set("selectedPdf", event.target.id)
        }
    });
}
