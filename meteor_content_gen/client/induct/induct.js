if(Meteor.isClient){
    Session.setDefault('selectedSample', '');
    // Session.setDefault('similar', '');
    Session.setDefault('selectedPdf', '');

    Template.induct.helpers({
        getInducted: function() {
            return Samples.find()
        },
        getPdf: function() {
            return Base.find()
        },
        getSim: function() {
            return displaySimilar();
        }
    });

    function compare(a, b) {
        a.score = parseFloat(a.score)
        b.score = parseFloat(b.score)
        console.log(typeof a.score, b.score)
        if (a.score < b.score)
            return -1;
        if (a.score > b.score)
            return 1;
        return 0;
    }

    function displaySimilar(){
        if (Session.get('selectedSample') != undefined && Session.get('selectedSample') != "" && Session.get('selectedPdf') != undefined && Session.get("selectedPdf") != ""){
            var similar = Samples.findOne({ "_id": Session.get('selectedSample') }).similar
            console.log('similar', similar);
            if (similar.length == 0){
                console.log('blank array');
                // Session.set('similar', [{ "qs": "Nothing", "text": "similar", "ans": "generated" }])
                return [{ "question": "Nothing", "text": "similar", "answer": "generated" }];
            }
            if (similar.base == Session.get('selectedPdf')) {
                // console.log("FOUND SIMILAR", similar[0].similarArray)
                // Session.set('similar', similar.similarArray)
                return similar.similarArray;
            }
        }
    }

    Template.induct.events({
        'click #submitBase': function() {
            var name = document.getElementById('baseName').value;
            var text = document.getElementById('baseText').value;
            console.log(name, text)
            if (name == "") {
                name = text.substring(0, 10) + ",custom"
            }
            Base.insert({ "name": name, "text": text, "similar": [] });
        },
        'change .selectSample': function() {
            var x = document.getElementById('selectInductRule');
            // Session.set('similar', '');
            var id = x.options[x.selectedIndex].id;
            Session.set("selectedSample", id);
            displaySimilar();
        },
        'click #updateInduct': function() {
            var baseId = Session.get('selectedPdf');
            var sampleId = Session.get('selectedSample');

            if(baseId != "" && baseId != undefined && sampleId != "" && sampleId != undefined){
                Meteor.call('getSimilarContent', baseId, sampleId);
            }else{
                alert('Select pdf and sample');
            }
        },
        'click .selectPdf': function(event) {
            Session.set("selectedPdf", event.target.id);
            // Session.set('similar', '');
            displaySimilar();
        },
        'click #upload_pdf': function(){
            console.log('in upload_pdf');
            var files = document.getElementById("pdf").files;

            if(files.length > 0){
                FilesColl.insert(files[0], function(err, fileObj) {
                    var file = files[0];
                    console.log('uploading file', file);
                    if (err) {
                        console.log('err', err);
                    } else {
                        fileObj.on('uploaded', function() {
                            console.log('uploaded', fileObj["_id"]);
                            Meteor.call('call_pdf_parser', fileObj["_id"], files[0]["name"], function(err, result) {
                                if (err) {
                                    console.log('err', err);
                                } else {
                                    // result = result.replace(/(?:\r\n|\r|\n)/g, ' ');
                                    console.log('result', result);
                                    Base.insert({ "name": files[0]["name"], "text": result, "similar": [] });
                                }
                            });
                            // Meteor.call('upload_pdf', file, fileObj["_id"]);
                        });
                    }
                });
            }
        }
    });
}