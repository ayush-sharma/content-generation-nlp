import feat_generator
import numpy
import json
from sklearn.feature_selection import VarianceThreshold
from nltk import Tree
from nltk.draw.tree import TreeView
import call_stanford_parser_server_api
import collections
from sklearn.externals import joblib
import sys



def y_label2words(preds, y_train_tree, all_stanford_output, input_text, stanford_output_train):
	preds_word = []

	for index, pred in enumerate(preds):
		# if pred:
		pred_answer_label = y_train_tree[pred]
		stanford_output_pred = stanford_output_train[pred]
	
		stanford_output_test = all_stanford_output[index]

		# pred_word = feat_generator.get_test_answer_label_distance(stanford_output, pred_answer_label)
		pred_word = feat_generator.get_test_answer_label_distance(stanford_output_test, pred_answer_label, stanford_output_pred)

		preds_word.append(pred_word)
		# else:
		# 	preds_word.append(None)

	actual_word = [i["answer"] for i in input_text]
	# if "answer" in input_text[0]:
	print "\n\nactual_word", actual_word

	print "preds_word  ", preds_word

	accuracy = sum([1 for index in xrange(0, len(preds_word)) if actual_word[index] == preds_word[index]])/float(len(preds_word))
	print "accuracy", accuracy

	return preds_word



def test(input_text):

	X_test = []
	y_test_tree = []
	y_test_label = []
	all_stanford_output = []

	y_train_tree = numpy.load('y_train_tree.npy')
	y_train_tree = y_train_tree.tolist()

	stanford_output_train = numpy.load('stanford_output_train.npy')

	for text in input_text:

		print "text ->", text["text"]

		# print "in get_feats"
		stanford_output = call_stanford_parser_server_api.get_output(text["text"])
		all_stanford_output.append(stanford_output)
		# print stanford_output["sentences"][0]["basic-dependencies"]

		feats = feat_generator.get_feats(stanford_output)
		feats = feats.flatten()
		X_test.append(feats)

	# 	y_label, y_test_tree = feat_generator.get_train_answer_label(stanford_output, text["answer"], y_test_tree)

	# 	if y_test_tree[y_label] in y_train_tree:
	# 		print "yes", y_test_tree[y_label]
	# 		y_test_label.append(y_train_tree.index(y_test_tree[y_label]))
	# 	else:
	# 		print "no", y_test_tree[y_label]
	# 		y_test_label.append(None)

	# print "y_test_label", y_test_label
	# print "y_test_tree", y_test_tree

	model = joblib.load('model.pkl')

	# print "\n\nactual ", y_test_label
	preds = model.predict(X_test)
	print "predict", preds.tolist()

	# print "score", model.score(X_test, y_test_label)

	preds_word = y_label2words(preds, y_train_tree, all_stanford_output, input_text, stanford_output_train)

	return preds_word





if __name__ == "__main__":

	sentence_type = sys.argv[1]
	input_text = []

	if sentence_type.lower() == "when":

		input_text = [
					# WHEN sentences
					{"text": "We eat breakfast in the morning.", "answer": "morning"},
					{"text": "They visited the zoo at 4 o'clock.", "answer": "4"},
					{"text": "Hari stepped in the office at around 8 am.", "answer": "8"},
					{"text": "In 2001, he visited the neighbouring country India.", "answer": "2001"},
					{"text": "He left the country in 2005.", "answer": "2005"},
					{"text": "Rita used to visit the place every Monday.", "answer": "Monday"},
					{"text": "Wedding is scheduled on 13th June.", "answer": "13th"},
					{"text": "Wedding is going to happen on 13th of this month.", "answer": "13th"},
					{"text": "She was born on Diwali.", "answer": "Diwali"},
					{"text": "The theft took place on 17th June.", "answer": "17th"},
					{"text": "He started studying at 8 am.", "answer": "8"},
		]

	elif sentence_type.lower() == "who":

		input_text = [
					# WHO sentences
					{"text": "The pancreas produces Insulin.", "answer": "pancreas"},
					{"text": "The door was opened by Ram.", "answer": "Ram"},
					{"text": "Mary was appointed as the secretary.", "answer": "Mary"},
					{"text": "Hari was cleaning the car yesterday.", "answer": "Hari"},
					{"text": "The engineer lived far away from the chaotic life in California.", "answer": "engineer"},
					{"text": "The chef took too long to prepare the lasagna.", "answer": "chef"},
					{"text": "In 1980, the fastest swimmer in the world was Jefferson", "answer": "Jefferson"},
					{"text": "Zookeeper takes care of animals at the zoo", "answer": "Zookeeper"},
					{"text": "Dentist checked our teeth for cavities.", "answer": "Dentist"},
					{"text": "Clerk helps people in the store.", "answer": "Clerk"},
					{"text": "Barber has a job of cutting people's hair.", "answer": "Barber"},
					{"text": "The enthusiastic fans painted their bodies with the team's colors.", "answer": "fans"}
		]

	test(input_text)








