from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
import numpy

X_train = numpy.load('X_train.npy')
y_train = numpy.load('y_train.npy')

X_test = numpy.load('X_test.npy')
y_test = numpy.load('y_test.npy')

print "y_train", y_train
print "y_test", y_test

model = OneVsRestClassifier(LinearSVC(random_state=0)).fit(X_train, y_train)

print "y_train_test", model.predict(X_train)

print "train score", model.score(X_train, y_train)

print "test score", model.score(X_test, y_test)