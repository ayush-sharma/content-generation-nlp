from flask import Flask, request
from flask_cors import CORS, cross_origin
import ml_test

app = Flask(__name__)

CORS(app)

@app.route('/')
def hello_world():
	return 'Hello World'


@app.route('/test',methods = ['POST', 'GET'])
def test_api():
	if request.method == 'POST':
		print "recieved post"
		input_text = request.data
		print "input_text", input_text

		try:
			return ml_test.test([{"text": input_text}])[0]
		except Exception as e:
			return None

	else:
		print "recieved get"
		return 'got it'

# app.add_url_rule('/hello', 'hello', hello_world)

if __name__ == '__main__':
	app.debug = True
	app.run(host='0.0.0.0', port=3000, debug = True)
