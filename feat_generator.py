import re
import json
import numpy
import pos_split
import operator

deps_list = ['sdep', 'xsubj', 'cc', 'number', 'intj', 'possessive', 'remnant', 'nsubjpass', 'csubj', 'subj', 'adp', 'discourse', 'mark', '_obj', 'mwe', 'advcl', 'dislocated', 'aux', 'mod', 'prep', 'parataxis', 'adpos', 'name', 'nsubj', 'complm', 'quantmod', 'tmod', 'gen', 'npadvmod', 'attr', 'purpcl', 'det', 'nmod', 'amod', 'dep', 'pobj', 'iobj', 'expl', 'predet', 'preconj', 'root', 'TDT', 'voc', 'list', 'some', 'agent', 'ccomp', 'prt', 'num', 'arg', 'conj', 'foreign', 'vocative', 'nn', 'neg', 'csubjpass', 'auxpass', 'infmod', 'rel', 'reparandum', 'ellipsis', 'compounding', 'ref', 'fw', 'comp', 'rcmod', 'advmod', 'compl', 'punct', 'acomp', 'English', 'pcomp', 'poss', 'goeswith', 'xcomp', 'cop', 'obj', 'partmod', 'appos', 'dobj', 'abbrev', 'sc', 'case', 'compound:prt', 'nummod', 'nmod:poss', 'compound', 'acl:relcl', 'nmod:tmod', 'nmod:npmod', 'acl']
pos_list_all_char = ["CC", "CD", "DT", "EX", "FW", "IN", "JJ", "JJR", "JJS", "LS", "MD", "NN", "NNS", "NNP", "NNPS", "PDT", "POS", "PRP", "PRP$", "RB", "RBR", "RBS", "RP", "SYM", "TO", "UH", "VB", "VBD", "VBG", "VBN", "VBP", "VBZ", "WDT", "WP", "WP$", "WRB", ".", ","]
pos_list = ['WDT', 'FW', 'WRB', 'DT', 'RP', '.', 'TO', 'LS', 'PDT', 'POS', 'CC', 'CD', 'EX', 'IN', 'MD', 'SYM', 'UH', 'JJ', 'WP', 'PR', 'NN', 'VB', 'RB']

pos_list_2_char = ['WD', 'FW', 'WR', 'DT', 'RP', '.', ',', 'TO', 'LS', 'PD', 'PO', 'CC', 'CD', 'EX', 'IN', 'MD', 'SY', 'UH', 'JJ', 'WP', 'PR', 'NN', 'VB', 'RB', 'ROOT']
deps_list_base = ['sdep', 'xsubj', 'cc', 'number', 'intj', 'possessive', 'remnant', 'nsubjpass', 'csubj', 'subj', 'adp', 'discourse', 'mark', '_obj', 'mwe', 'advcl', 'dislocated', 'aux', 'mod', 'prep', 'parataxis', 'adpos', 'name', 'nsubj', 'complm', 'quantmod', 'tmod', 'gen', 'npadvmod', 'attr', 'purpcl', 'det', 'nmod', 'amod', 'dep', 'pobj', 'iobj', 'expl', 'predet', 'preconj', 'ROOT', 'TDT', 'voc', 'list', 'some', 'agent', 'ccomp', 'prt', 'num', 'arg', 'conj', 'foreign', 'vocative', 'nn', 'neg', 'csubjpass', 'auxpass', 'infmod', 'rel', 'reparandum', 'ellipsis', 'compounding', 'ref', 'fw', 'comp', 'rcmod', 'advmod', 'compl', 'punct', 'acomp', 'English', 'pcomp', 'poss', 'goeswith', 'xcomp', 'cop', 'obj', 'partmod', 'appos', 'dobj', 'abbrev', 'sc', 'case', 'compound', 'nummod', 'nmod', 'compound', 'acl', 'nmod', 'nmod', 'acl']

ner_list = ["PERSON", "LOCATION", "O", "DATE", "TIME", "ORGANIZATION", "MONEY", "PERCENT", "SET", "ORDINAL", "MISC", "NUMBER", "DURATION"]


MAX_DEP_COUNT = 3


def predict_next_entities(test_stanford_output, pred_entities, text):

	all_subject_adjs = []
	all_object_adjs = []
	
	subject_label = get_answer_label(test_stanford_output, pred_entities["subject"])
	subject_adjs = get_adjectives(subject_label["deps_with_words"])
	subject_det = get_determinants(subject_label["deps_with_words"])
	subject_prep = get_prepositions(subject_label["deps_with_words"])
	# print "actual subject_adjs", [text["entities"]["adj"] if "adj" in text["entities"] else None]
	# print "predict subject_adjs", subject_adjs

	pred_entities["sub_adj"] = subject_adjs
	pred_entities["sub_det"] = subject_det
	pred_entities["sub_prep"] = subject_prep

	object_label = get_answer_label(test_stanford_output, pred_entities["object"])
	object_adjs = get_adjectives(object_label["deps_with_words"])
	object_det = get_determinants(object_label["deps_with_words"])
	object_prep = get_prepositions(object_label["deps_with_words"])
	# print "actual object_adjs", text["entities"]["obj_adj"] if "obj_adj" in text["entities"] else None]
	# print "predict object_adjs", object_adjs
	
	pred_entities["obj_adj"] = object_adjs
	pred_entities["obj_det"] = object_det
	pred_entities["obj_prep"] = object_prep

	# verb = get_verb(test_stanford_output, pred_entities["verb"])
	# pred_entities["verb"] = verb

	verb_label = get_answer_label(test_stanford_output, pred_entities["verb"])
	deps_with_words = verb_label["deps_with_words"]
	
	for dep in deps_with_words:
		if dep[:3] == "aux":
			if deps_with_words[dep][0]["word"] == pred_entities["verb"]:
				pred_entities["verb"] = deps_with_words[dep][1]["word"]
				break
			elif deps_with_words[dep][1]["word"] == pred_entities["verb"]:
				pred_entities["verb"] = deps_with_words[dep][0]["word"]
				break

	return pred_entities


def get_main_entities(pred_answer_tree, stanford_output_test, input_text, entity, stanford_output_pred):

	pred_word, score_stats = get_test_answer_label_distance(stanford_output_test, pred_answer_tree, stanford_output_pred)

	pred_word_pos = get_token_attr(stanford_output_test, pred_word)["pos"]

	if entity == "adverb":
		if pred_word_pos[:2] != "RB":
			pred_word = None
	elif pred_word_pos[:2] == ".":
		pred_word = None

	if "entities" in input_text and entity in input_text["entities"]:
		actual_word = input_text["entities"][entity]
		print "\nactual_word", actual_word
		print "pred_word  ", pred_word
		print "accuracy", True if pred_word == actual_word else False

	return pred_word


def get_prepositions(label):
	for dep, entities in label.iteritems():
		for entity in entities:
			if entity["pos"] == "IN" or entity["pos"] == "TO":
				return entity["word"]


def get_verb(stanford_output):
	basic_deps = stanford_output["sentences"][0]["basic-dependencies"]
	return basic_deps[0]["dependentGloss"]


def get_similar_sentences(test_feat, train_feats):

	matches = []
	for index, train_feat in enumerate(train_feats["feats"]):
		score = euclidean_distance(test_feat, train_feat)
		matches.append((score, train_feats["input_text"][index]["text"], index))
	matches.sort(key=operator.itemgetter(0), reverse=True)

	return matches

def get_adjectives(label):
	adjectives = []

	for dep, entities in label.iteritems():
		for entity in entities:
			if entity["pos"] == "JJ":
				adjectives.append(entity["word"])

	return adjectives


def get_determinants(label):
	for dep, entities in label.iteritems():
		for entity in entities:
			if entity["pos"] == "DT":
				return entity["word"]



def euclidean_distance(list1, list2):
	sum_of_squares = sum([pow(list1[index] - list2[index], 2) for index in xrange(0, len(list1))])
	return 1 / (1 + sum_of_squares)


def get_answer_feats(answer_label, stanford_output):
	deps_feats = numpy.zeros((len(deps_list_base), len(pos_list_2_char)))
	for dep, pos in answer_label["deps"].iteritems():
		dep_index = deps_list_base.index(dep)

		deps_feats[dep_index][pos[0]] = 1
		deps_feats[dep_index][pos[1]] = -1

	ner_feats = numpy.zeros(len(ner_list))

	# include ner feats

	ner_tag = answer_label["ner_tag"]
	ner_feats[ner_list.index(ner_tag)] = 1


	split_feat = numpy.zeros(3)

	NP, VP, OP = pos_split.get_word_classes(stanford_output["sentences"][0]["parse"])

	if answer_label["word"] in NP:
		split_feat[0] = 1
	elif answer_label["word"] in VP:
		split_feat[1] = 1
	else:
		split_feat[2] = 1



	pos_2char_feats = numpy.zeros(len(pos_list_2_char))
	pos_index = pos_list_2_char.index(answer_label["pos"][:2])
	pos_2char_feats[pos_index] = 1

	pos_all_char_feats = numpy.zeros(len(pos_list_all_char))
	pos_index = pos_list_all_char.index(answer_label["pos"])
	pos_all_char_feats[pos_index] = 1

	return deps_feats.flatten(), ner_feats, pos_2char_feats, pos_all_char_feats, split_feat




def get_test_answer_label_distance(stanford_output_test, pred_answer_label, stanford_output_pred):
	pred_deps_feats, pred_ner_feats, pred_pos_2char_feats, pred_pos_all_char_feats, pred_split_feat = get_answer_feats(pred_answer_label, stanford_output_pred)
	pred_feats = pred_deps_feats.tolist() + pred_ner_feats.tolist() + pred_pos_2char_feats.tolist() + pred_pos_all_char_feats.tolist() + pred_split_feat.tolist()
	max_score = 0
	best_match = stanford_output_test["sentences"][0]["tokens"][0]["word"]
	score_stats = []
	for token in stanford_output_test["sentences"][0]["tokens"]:
		answer_label = get_answer_label(stanford_output_test, token["word"])
		answer_deps_feats, answer_ner_feats, answer_2char_pos_feats, answer_all_char_pos_feats, answer_split_feat = get_answer_feats(answer_label, stanford_output_test)
		answer_feats = answer_deps_feats.tolist() + answer_ner_feats.tolist() + answer_2char_pos_feats.tolist() + answer_all_char_pos_feats.tolist() + answer_split_feat.tolist()

		# scoring #######

		# score = euclidean_distance(answer_feats, pred_feats)
		# score_stats.append((token["word"], round(score, 3)))


		deps_score = euclidean_distance(answer_deps_feats, pred_deps_feats)
		pos_2_char_score = euclidean_distance(answer_2char_pos_feats, pred_pos_2char_feats)
		pos_all_char_score = euclidean_distance(answer_all_char_pos_feats, pred_pos_all_char_feats)
		ner_score = euclidean_distance(answer_ner_feats, pred_ner_feats)
		pos_split_score = euclidean_distance(answer_split_feat, pred_split_feat)

		score = deps_score + ner_score + pos_2_char_score + pos_all_char_score + pos_split_score
		score_stats.append((token["word"], round(score, 4), str({"deps_score": deps_score, "ner_score": ner_score, "pos_2_char_score": pos_2_char_score, "pos_all_char_score": pos_all_char_score, "pos_split_score": pos_split_score})))

		#################


		if max_score < score:
			max_score = score
			best_match = token["word"]
	
	score_stats.sort(key=operator.itemgetter(1), reverse=True)
	print "\nscore_stats", json.dumps(score_stats[:2], indent=4, sort_keys=True)
	return best_match, score_stats



def get_unique_answer_labels(answer_label, y_train_tree):

	for index, y in enumerate(y_train_tree):

		# if answer_label == y:
		# 	return index, y_train_tree

		if answer_label["deps"] == y["deps"] and answer_label["pos"] == y["pos"]:
			return index, y_train_tree

	y_train_tree.append(answer_label)
	return len(y_train_tree) - 1, y_train_tree


def get_test_answer_label(stanford_output, pred_answer_label):

	for token in stanford_output["sentences"][0]["tokens"]:
		answer_label = get_answer_label(stanford_output, token["word"])
		if pred_answer_label == answer_label:
			return token["word"]
	return 'NO_MATCH'



def get_train_answer_label(stanford_output, answer, y_train_tree):

	answer_label = get_answer_label(stanford_output, answer)
	return get_unique_answer_labels(answer_label, y_train_tree)


def get_answer_label(stanford_output, answer):
	basic_deps = stanford_output["sentences"][0]["basic-dependencies"]
	token_attr = get_token_attr(stanford_output, answer)
	pos_token = token_attr["pos"]
	ner_tag = token_attr["ner"]
	word_tag = token_attr["word"]
	answer_label = {"deps": {}, "pos": pos_token, "ner_tag": ner_tag, "word": word_tag, "deps_with_words": {}}
	
	for dep in basic_deps:

		# if answer is governorGloss
		if dep["governorGloss"] == answer:

			dep["dep"] = dep["dep"].split(':')[0]

			if dep["dep"] != "ROOT":
				governor_pos = get_token_attr(stanford_output, dep["governorGloss"])["pos"]
				pos_index_governor = pos_list_2_char.index(governor_pos[:2])
			else:
				governor_pos = "ROOT"
				pos_index_governor = pos_list_2_char.index(governor_pos)
			dependent_pos = get_token_attr(stanford_output, dep["dependentGloss"])["pos"]
			pos_index_dependent = pos_list_2_char.index(dependent_pos[:2])

			answer_label["deps"][dep["dep"]] = [pos_index_governor, pos_index_dependent]
			answer_label["deps_with_words"][dep["dep"]] = [{"word": dep["governorGloss"], "pos": governor_pos[:2]}, {"word": dep["dependentGloss"], "pos":  dependent_pos[:2]}]


		# if answer is dependentGloss
		if dep["dependentGloss"] == answer:

			if dep["dep"] != "ROOT":
				governor_pos = get_token_attr(stanford_output, dep["governorGloss"])["pos"]
				pos_index_governor = pos_list_2_char.index(governor_pos[:2])
			else:
				governor_pos = "ROOT"
				pos_index_governor = pos_list_2_char.index(governor_pos)
			dependent_pos = get_token_attr(stanford_output, dep["dependentGloss"])["pos"]
			pos_index_dependent = pos_list_2_char.index(dependent_pos[:2])

			answer_label["deps"][dep["dep"]] = [pos_index_governor, pos_index_dependent]
			answer_label["deps_with_words"][dep["dep"]] = [{"word": dep["governorGloss"], "pos": governor_pos[:2]}, {"word": dep["dependentGloss"], "pos":  dependent_pos[:2]}]

	return answer_label




def get_feats(stanford_output):
	basic_deps = stanford_output["sentences"][0]["basic-dependencies"]
	tokens = stanford_output["sentences"][0]["tokens"]

	feats = numpy.zeros((len(deps_list_base), MAX_DEP_COUNT, len(pos_list_2_char)))
	# feats_dict = {}

	dep_count = {}
	# print "stanford_output", stanford_output
	for dep in basic_deps:
		dep["dep"] = dep["dep"].split(':')[0]
		dep_count.setdefault(dep["dep"], -1)
		dep_count[dep["dep"]] += 1

		if dep_count[dep["dep"]] >= 3:
			print "\n\n################\ngoing to break. MAX_DEP_COUNT exceeded\n################\n", basic_deps
			exit(0)

		dep_index = deps_list_base.index(dep["dep"])

		# dep_direction = 1
		# if dep["governor"] > dep["dependent"]:
		# 	dep_direction = -1
		
		if dep["dep"] != "ROOT":
			governor_pos = get_token_attr(stanford_output, dep["governorGloss"])["pos"]
			pos_index_governor = pos_list_2_char.index(governor_pos[:2])
		else:
			governor_pos = "ROOT"
			pos_index_governor = pos_list_2_char.index(governor_pos)

		dependent_pos = get_token_attr(stanford_output, dep["dependentGloss"])["pos"]
		pos_index_dependent = pos_list_2_char.index(dependent_pos[:2])

		feats[dep_index][dep_count[dep["dep"]]][pos_index_governor] = 1
		feats[dep_index][dep_count[dep["dep"]]][pos_index_dependent] = -1

		# feats_dict.setdefault(dep["dep"], [])
		# feats_dict[dep["dep"]].append({governor_pos[:2]: 1, dependent_pos[:2]: -1})
	# if get_feats_dict:
	# 	return feats_dict

	return feats








#####################################################################################




def get_ner_tag(output, match_word):
	tokens = output["sentences"][0]["tokens"]
	for token in tokens:
		if token["word"].lower() == match_word.lower():
			return token["ner"]
	return None


def get_token_attr(output, match_word):
	tokens = output["sentences"][0]["tokens"]
	for token in tokens:
		if token["word"].lower() == match_word.lower():
			return {"pos": token["pos"], "ner": token["ner"], "word": match_word}
	return {"pos": None, "ner": None}

def get_associated_term_attr_depricated(output, match_word):
	basic_deps = output["sentences"][0]["basic-dependencies"]
	tokens = output["sentences"][0]["tokens"]
	associated_term_attr = {"governor": [], "dependent": [], "token": None}
	for dep in basic_deps:
		if dep["governorGloss"].lower() == match_word.lower():
			associated_term_attr["governor"].append(dep["dep"])

		if dep["dependentGloss"].lower() == match_word.lower():
			associated_term_attr["dependent"].append(dep["dep"])

	associated_term_attr["token"] = get_token_attr(output, match_word)["pos"]

	return associated_term_attr

def get_associated_term_attr(output, match_word):
	basic_deps = output["sentences"][0]["basic-dependencies"]
	tokens = output["sentences"][0]["tokens"]
	associated_term_attr = {"governor": [], "dependent": [], "token": None, "word": match_word}
	for dep in basic_deps:
		if dep["governorGloss"].lower() == match_word.lower():
			associated_term_attr["governor"].append({"dep": dep["dep"], "dep_pos": get_token_attr(output, dep["dependentGloss"])["pos"] })

		if dep["dependentGloss"].lower() == match_word.lower():
			associated_term_attr["dependent"].append({"dep": dep["dep"], "dep_pos": get_token_attr(output, dep["governorGloss"])["pos"] })

	associated_term_attr["token"] = get_token_attr(output, match_word)["pos"]

	return associated_term_attr


def find_pos_token_index(pos_list, match_token):
	for ind, val in enumerate(pos_list):
		if val[0:2] == match_token[0:2]:
			# print "match_token", match_token, ind
			return ind, val



def parse_output_pos_index(output, match_word):
	match_word = match_word.lower()
	associated_term_attr = get_associated_term_attr(output, match_word)
	print "associated_term_attr", associated_term_attr

	match_word_deps_feats = numpy.zeros(len(deps_list))
	match_word_pos_feats = numpy.zeros(len(pos_list))
	match_word_pos_feats[pos_list.index(associated_term_attr["token"])] = 1

	for dep in associated_term_attr["governor"]:
		index = deps_list.index(dep["dep"].lower())
		# match_word_deps_feats[index] = 1
		match_word_deps_feats[index] = pos_list.index(dep["dep_pos"])

	for dep in associated_term_attr["dependent"]:
		index = deps_list.index(dep["dep"].lower())
		match_word_deps_feats[index] = -(pos_list.index(dep["dep_pos"]))

	X = numpy.hstack((match_word_pos_feats, match_word_deps_feats))
	y_label = associated_term_attr["token"]

	return X, y_label

def parse_output_wo_split(output, match_word):
	match_word = match_word.lower()
	associated_term_attr = get_associated_term_attr(output, match_word)
	# print "associated_term_attr", associated_term_attr
	# print len(deps_list), len(pos_list)

	match_word_deps_feats = numpy.zeros((len(deps_list), len(pos_list)))
	match_word_pos_feats = numpy.zeros(len(pos_list))
	# match_word_pos_feats[pos_list.index(associated_term_attr["token"])] = 1
	match_word_pos_feats[find_pos_token_index(pos_list, associated_term_attr["token"])[0]] = 1

	for dep in associated_term_attr["governor"]:
		if dep["dep"].lower() == "root":
			continue
		index = deps_list.index(dep["dep"].lower())
		# match_word_deps_feats[index][pos_list.index(dep["dep_pos"])] = 1
		match_word_deps_feats[index][find_pos_token_index(pos_list, dep["dep_pos"])[0]] = 1

	for dep in associated_term_attr["dependent"]:
		if dep["dep"].lower() == "root":
			continue
		index = deps_list.index(dep["dep"].lower())
		# match_word_deps_feats[index][pos_list.index(dep["dep_pos"])] = -1
		match_word_deps_feats[index][find_pos_token_index(pos_list, dep["dep_pos"])[0]] = -1

	X = numpy.hstack((match_word_pos_feats.flatten(), match_word_deps_feats.flatten()))
	# y_label = associated_term_attr["token"]
	y_label = find_pos_token_index(pos_list, associated_term_attr["token"])[1]

	return X, y_label


def parse_output(output, match_word):
	match_word = match_word.lower()
	associated_term_attr = get_associated_term_attr(output, match_word)
	# print "associated_term_attr", associated_term_attr
	# print len(deps_list), len(pos_list)

	match_word_deps_feats = numpy.zeros((len(deps_list), len(pos_list)))
	match_word_pos_feats = numpy.zeros(len(pos_list))
	match_word_pos_split = numpy.zeros(3)
	# match_word_pos_feats[pos_list.index(associated_term_attr["token"])] = 1
	match_word_pos_feats[find_pos_token_index(pos_list, associated_term_attr["token"])[0]] = 1

	for dep in associated_term_attr["governor"]:
		if dep["dep"].lower() == "root":
			continue
		index = deps_list.index(dep["dep"].lower())
		# match_word_deps_feats[index][pos_list.index(dep["dep_pos"])] = 1
		match_word_deps_feats[index][find_pos_token_index(pos_list, dep["dep_pos"])[0]] = 1

	for dep in associated_term_attr["dependent"]:
		if dep["dep"].lower() == "root":
			continue
		index = deps_list.index(dep["dep"].lower())
		# match_word_deps_feats[index][pos_list.index(dep["dep_pos"])] = -1
		match_word_deps_feats[index][find_pos_token_index(pos_list, dep["dep_pos"])[0]] = -1


	NP, VP, OP = pos_split.get_word_classes(output["sentences"][0]["parse"])
	
	if match_word in NP:
		match_word_pos_split[0] = 1
	elif match_word in VP:
		match_word_pos_split[1] = 1
	else:
		match_word_pos_split[2] = 1


	X = numpy.hstack((match_word_pos_feats.flatten(), match_word_deps_feats.flatten(), match_word_pos_split.flatten()))
	# y_label = associated_term_attr["token"]
	y_label = find_pos_token_index(pos_list, associated_term_attr["token"])[1]

	return X, y_label


if __name__ == "__main__":
	output = {u'sentences': [{u'openie': [{u'subjectSpan': [1, 3], u'relationSpan': [3, 4], u'objectSpan': [8, 11], u'object': u'secretly night', u'relation': u'went at', u'subject': u'fat boy'}, {u'subjectSpan': [2, 3], u'relationSpan': [3, 4], u'objectSpan': [10, 11], u'object': u'night', u'relation': u'went at', u'subject': u'boy'}, {u'subjectSpan': [1, 3], u'relationSpan': [3, 5], u'objectSpan': [6, 8], u'object': u'big bridge', u'relation': u'went under', u'subject': u'fat boy'}, {u'subjectSpan': [1, 3], u'relationSpan': [3, 5], u'objectSpan': [7, 8], u'object': u'bridge', u'relation': u'went under', u'subject': u'fat boy'}, {u'subjectSpan': [2, 3], u'relationSpan': [3, 5], u'objectSpan': [7, 8], u'object': u'bridge', u'relation': u'went under', u'subject': u'boy'}, {u'subjectSpan': [2, 3], u'relationSpan': [3, 4], u'objectSpan': [8, 11], u'object': u'secretly night', u'relation': u'went at', u'subject': u'boy'}, {u'subjectSpan': [2, 3], u'relationSpan': [3, 5], u'objectSpan': [6, 8], u'object': u'big bridge', u'relation': u'went under', u'subject': u'boy'}, {u'subjectSpan': [1, 3], u'relationSpan': [3, 4], u'objectSpan': [10, 11], u'object': u'night', u'relation': u'went at', u'subject': u'fat boy'}], u'index': 0, u'basic-dependencies': [{u'dep': u'ROOT', u'dependent': 4, u'governorGloss': u'ROOT', u'governor': 0, u'dependentGloss': u'went'}, {u'dep': u'det', u'dependent': 1, u'governorGloss': u'boy', u'governor': 3, u'dependentGloss': u'The'}, {u'dep': u'amod', u'dependent': 2, u'governorGloss': u'boy', u'governor': 3, u'dependentGloss': u'fat'}, {u'dep': u'nsubj', u'dependent': 3, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'boy'}, {u'dep': u'case', u'dependent': 5, u'governorGloss': u'bridge', u'governor': 8, u'dependentGloss': u'under'}, {u'dep': u'det', u'dependent': 6, u'governorGloss': u'bridge', u'governor': 8, u'dependentGloss': u'the'}, {u'dep': u'amod', u'dependent': 7, u'governorGloss': u'bridge', u'governor': 8, u'dependentGloss': u'big'}, {u'dep': u'nmod', u'dependent': 8, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'bridge'}, {u'dep': u'advmod', u'dependent': 9, u'governorGloss': u'night', u'governor': 11, u'dependentGloss': u'secretly'}, {u'dep': u'case', u'dependent': 10, u'governorGloss': u'night', u'governor': 11, u'dependentGloss': u'at'}, {u'dep': u'nmod', u'dependent': 11, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'night'}, {u'dep': u'punct', u'dependent': 12, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'.'}], u'parse': u'(ROOT\n  (S\n    (NP (DT The) (JJ fat) (NN boy))\n    (VP (VBD went)\n      (PP (IN under)\n        (NP (DT the) (JJ big) (NN bridge)))\n      (PP\n        (ADVP (RB secretly))\n        (IN at)\n        (NP (NN night))))\n    (. .)))', u'tokens': [{u'index': 1, u'word': u'The', u'lemma': u'the', u'after': u' ', u'pos': u'DT', u'characterOffsetEnd': 3, u'characterOffsetBegin': 0, u'originalText': u'The', u'ner': u'O', u'before': u''}, {u'index': 2, u'word': u'fat', u'lemma': u'fat', u'after': u' ', u'pos': u'JJ', u'characterOffsetEnd': 7, u'characterOffsetBegin': 4, u'originalText': u'fat', u'ner': u'O', u'before': u' '}, {u'index': 3, u'word': u'boy', u'lemma': u'boy', u'after': u' ', u'pos': u'NN', u'characterOffsetEnd': 11, u'characterOffsetBegin': 8, u'originalText': u'boy', u'ner': u'O', u'before': u' '}, {u'index': 4, u'word': u'went', u'lemma': u'go', u'after': u' ', u'pos': u'VBD', u'characterOffsetEnd': 16, u'characterOffsetBegin': 12, u'originalText': u'went', u'ner': u'O', u'before': u' '}, {u'index': 5, u'word': u'under', u'lemma': u'under', u'after': u' ', u'pos': u'IN', u'characterOffsetEnd': 22, u'characterOffsetBegin': 17, u'originalText': u'under', u'ner': u'O', u'before': u' '}, {u'index': 6, u'word': u'the', u'lemma': u'the', u'after': u' ', u'pos': u'DT', u'characterOffsetEnd': 26, u'characterOffsetBegin': 23, u'originalText': u'the', u'ner': u'O', u'before': u' '}, {u'index': 7, u'word': u'big', u'lemma': u'big', u'after': u' ', u'pos': u'JJ', u'characterOffsetEnd': 30, u'characterOffsetBegin': 27, u'originalText': u'big', u'ner': u'O', u'before': u' '}, {u'index': 8, u'word': u'bridge', u'lemma': u'bridge', u'after': u' ', u'pos': u'NN', u'characterOffsetEnd': 37, u'characterOffsetBegin': 31, u'originalText': u'bridge', u'ner': u'O', u'before': u' '}, {u'index': 9, u'word': u'secretly', u'lemma': u'secretly', u'after': u' ', u'pos': u'RB', u'characterOffsetEnd': 46, u'characterOffsetBegin': 38, u'originalText': u'secretly', u'ner': u'O', u'before': u' '}, {u'index': 10, u'word': u'at', u'lemma': u'at', u'after': u' ', u'pos': u'IN', u'characterOffsetEnd': 49, u'characterOffsetBegin': 47, u'originalText': u'at', u'ner': u'O', u'before': u' '}, {u'index': 11, u'after': u'', u'word': u'night', u'lemma': u'night', u'normalizedNER': u'TNI', u'pos': u'NN', u'characterOffsetEnd': 55, u'timex': {u'tid': u't1', u'type': u'TIME', u'value': u'TNI'}, u'characterOffsetBegin': 50, u'originalText': u'night', u'ner': u'TIME', u'before': u' '}, {u'index': 12, u'word': u'.', u'lemma': u'.', u'after': u'', u'pos': u'.', u'characterOffsetEnd': 56, u'characterOffsetBegin': 55, u'originalText': u'.', u'ner': u'O', u'before': u''}], u'collapsed-dependencies': [{u'dep': u'ROOT', u'dependent': 4, u'governorGloss': u'ROOT', u'governor': 0, u'dependentGloss': u'went'}, {u'dep': u'det', u'dependent': 1, u'governorGloss': u'boy', u'governor': 3, u'dependentGloss': u'The'}, {u'dep': u'amod', u'dependent': 2, u'governorGloss': u'boy', u'governor': 3, u'dependentGloss': u'fat'}, {u'dep': u'nsubj', u'dependent': 3, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'boy'}, {u'dep': u'case', u'dependent': 5, u'governorGloss': u'bridge', u'governor': 8, u'dependentGloss': u'under'}, {u'dep': u'det', u'dependent': 6, u'governorGloss': u'bridge', u'governor': 8, u'dependentGloss': u'the'}, {u'dep': u'amod', u'dependent': 7, u'governorGloss': u'bridge', u'governor': 8, u'dependentGloss': u'big'}, {u'dep': u'nmod:under', u'dependent': 8, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'bridge'}, {u'dep': u'advmod', u'dependent': 9, u'governorGloss': u'night', u'governor': 11, u'dependentGloss': u'secretly'}, {u'dep': u'case', u'dependent': 10, u'governorGloss': u'night', u'governor': 11, u'dependentGloss': u'at'}, {u'dep': u'nmod:at', u'dependent': 11, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'night'}], u'collapsed-ccprocessed-dependencies': [{u'dep': u'ROOT', u'dependent': 4, u'governorGloss': u'ROOT', u'governor': 0, u'dependentGloss': u'went'}, {u'dep': u'det', u'dependent': 1, u'governorGloss': u'boy', u'governor': 3, u'dependentGloss': u'The'}, {u'dep': u'amod', u'dependent': 2, u'governorGloss': u'boy', u'governor': 3, u'dependentGloss': u'fat'}, {u'dep': u'nsubj', u'dependent': 3, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'boy'}, {u'dep': u'case', u'dependent': 5, u'governorGloss': u'bridge', u'governor': 8, u'dependentGloss': u'under'}, {u'dep': u'det', u'dependent': 6, u'governorGloss': u'bridge', u'governor': 8, u'dependentGloss': u'the'}, {u'dep': u'amod', u'dependent': 7, u'governorGloss': u'bridge', u'governor': 8, u'dependentGloss': u'big'}, {u'dep': u'nmod:under', u'dependent': 8, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'bridge'}, {u'dep': u'advmod', u'dependent': 9, u'governorGloss': u'night', u'governor': 11, u'dependentGloss': u'secretly'}, {u'dep': u'case', u'dependent': 10, u'governorGloss': u'night', u'governor': 11, u'dependentGloss': u'at'}, {u'dep': u'nmod:at', u'dependent': 11, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'night'}, {u'dep': u'punct', u'dependent': 12, u'governorGloss': u'went', u'governor': 4, u'dependentGloss': u'.'}]}]}

	print get_feats(output)



	